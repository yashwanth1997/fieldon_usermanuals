# Moderator
<!-- {docsify-ignore} -->

Moderators are those who approves and rejects the task submitted by field users. 

<p align="center"> <img width="1000" height="500" src="media/MA.jpg"> </p>

<div align="center"> 

**`Fig` Moderator activities**

</div>

<p>Enter valid Moderator username and password and click on "Sign in".</p>

Login as [Moderator ](https://demo.fieldon.com/#/login)

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod1.png"></p>

<div align="center">

**`Fig.1` Moderator login page**

</div>

<p>After login we can see the moderator page has</p>

 - Forms
 - Approvals
 - Downloads

<hr>


## Forms

1. When clicked on "Forms" tab, we can see the list of Forms available.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod2.png"></p>

<div align="center">

**`Fig.2` Forms page**

</div>

2.Search forms, shows the forms with the searched input if any.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod3.png"></p>

<div align="center">

**`Fig.3` Search forms**

</div>

3.Filter forms, click on "filter" button, select type All/public/private, shows forms based on the selection. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod4.png"></p>

<div align="center">

**`Fig.4` Filter forms**

</div>

4. GroupBy category, click on "group by category" shows categories and forms under each category. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod5.png"></p>

<div align="center">

**`Fig.5` Group By category-forms**
</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod6.png"></p>

<div align="center">

**`Fig.6` Group By categories-forms**
</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod7.png"></p>

<div align="center">

**`Fig.7` Forms under each category**

</div>

### Form preview

1.To Preview the Form, go to the form and hover on the form, we can see preview icon, Click on "preview" icon a popup opens with all the widgets in the form. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod8.png"></p>

<div align="center">

**`Fig.8` Preview of form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod9.png"></p>

<div align="center">

**`Fig.9` Preview of form**

</div>

### Form Export to Excel

1. We can Export the Form to Excel to get all the widgets of the form available in the form of excel. 
2. We can use fill this excel and upload in Work Assignments to Field user.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod10.png"></p>

<div align="center">

**`Fig.10` Export to Excel-form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod11.png"></p>


### Submitted records- Form

1. Click on "submitted records" icon which in on hover on the form. Shows records within 24 hrs. 
2. Click on "filter" button, opens a filter records popup. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod12.png"></p>

<div align="center">

**`Fig.11` Filter submitted records of forms**

</div>

3. Select from date, to date and select Field users, click on "get data" button. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod13.png"></p>

<div align="center">

**`Fig.12` Filter records**

</div>


5.Export to Excel, select records and click on "export to excel" button, opens columns selections page in the right side. Select the columns and click on ✅. Shows confirmation message. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod15.png"></p>

<div align="center">

**`Fig.14` Export to excel-Submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod16.png"></p>

<div align="center">

**`Fig.15` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod17.png"></p>

<div align="center">

</div>

6.Export to mail, select records and click on "export to mail" button, opens a popup with name email attachment with fields To, CC, select pdf or excel and click on send. Shows confirmation message. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod18.png"></p>

<div align="center">

**`Fig.16` Export to Email**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod19.png"></p>

<div align="center">

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod20.png"></p>

<div align="center">

**`Fig.17` Email attachment form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod21.png"></p>

<div align="center">

</div>

7.Export to pdf, select records and click on export to "pdf" button, opens columns selection page with list of columns. Select columns and click on ✅. Shows confirmation message. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod22.png"></p>

<div align="center">

**`Fig.18` Export to pdf**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod23.png"></p>

<div align="center">

**`Fig.19` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod24.png"></p>

<div align="center">

</div>

8.Inserted locations, select record and click on "inserted location" button, map opens and shows the inserted location of the selected record. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod25.png"></p>

<div align="center">

**`Fig.20` Inserted locations**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod26.png"></p>

<div align="center">

</div>

9.Add columns, click on "add columns" button, open columns selection page on the right side with list of columns. Select required columns and click on ✅. Selected columns are shown in the records table.  

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod27.png"></p>

<div align="center">

**`Fig.21` Add columns**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod28.png"></p>

<div align="center">

**`Fig.22` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod29.png"></p>

<div align="center">

**`Fig.23` Add columns**

</div>

**Note**

Can export 1000 records to Excel at a time, for more than 1000 records shows "alert message". 

Can export 10 records to mail/pdf at a time, for more than 10 records shows "alert message". 

### Preview of submitted records
1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by field user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.23` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.24` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done by field user in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.25` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.26`  Sketching**

</div>

Click on "geo tagged images", to see the images that are geo tagged by field user on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa185.jpg"></p>

<div align="center">

**`Fig.27`  Geo tagged images of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa186.jpg"></p>

<div align="center">

**`Fig.28` Geo tagged images**

</div>

Click on "attachments" to see the attached files. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa187.jpg"></p>

<div align="center">

**`Fig.29`  Attachments of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa188.jpg"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit skecthing function moderator can see sketch objects on the map. And edit sketching functionality is explained in the below steps.

1.Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by field user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.30` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.31` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.32` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.33`  Sketching**

</div>

4. Edit Skecthing 

o Click on "edit sketching" option on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.34`  Edit Sketching**

</div>

o The options will be displayed and click on "measuring tool" to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.35`  Edit Sketching**

</div>

o By clicking on "measuring tool", options get display the polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.36` Edit Sketching**

</div>

o If moderator select the polyline sketching option, application allows moderator to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.37` Draw sketching by polyline**

</div>

o Click on the "finish" option so that skecthing was end and the polyline is placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.38` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.39` Line sketching**

</div>

o Click on the "tick icon" to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.40` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to save skecthing

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.41` Save sketching**

</div>

o By clicking on "yes" in pop-up menu the sketching is saved successfully and displayed "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.42` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps mderator can sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures moderator can sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.43` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.44` Polygon sketching**

</div>

o Modearator can place a marker on the map by using marker option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.45` Marker sketching**

</div>

6. Delete Skecthing

o Moderator can delete the sketching, to delete the sketching user click on the "delete" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.46` Delete sketching**

</div>

o Click on the respective sketching to get delete

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.47` Delete sketching**

</div>

o Click on the "save" option, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.48` Delete sketching**

</div>

o Click on the "tick mark" option to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.49` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.50` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.51` Update sketching**

</div>

7. Edit Layers

o Moderator click on the "edit sketching" button on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.52`  Edit Sketching**

</div>

o Moderator click on the "measuring tool" option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.53`  Measuring tool**

</div>

o Moderator click on the "edit layers option". Moderator can see sketching data are enabled into edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.54`  Edit layers**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.55`  Sketching in edit mode**

</div>


o Moderator can edit by draging handles on the skecthing.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.56`  Drag Handles or marker**

</div>


o After changes are done moderator click on the "save" button, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.57` Save Option**

</div>

o Click on "tick mark" lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.58`  Update Button**

</div>

o Click on "yes" button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.59`  Update Button**

</div>

o Mofderator  gets a "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.60`  Update Button**

</div>

8. Sketching full screen view

o By clicking on "full screen view" option, the map opens in full screen


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.61` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.62` Full screen view sketching**

</div>


### Add/Edit sketching properties in the sketching

1. Moderator can add and edit  sketching properties information for sketching objects on the map

2. Moderator click on the "edit sketching" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.63`  Edit Sketching**

</div>

3. Moderator click on the "sketching object", pop-up menu open with readonly form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.64`  Sketching Properties Pop-Up menu**

</div>

4.Moderator by clicking on "edit" button in skecting properties, administartor can edit/add the fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.65`  Sketching Properties **

</div>

5 By clicking on "edit" option, form opens on the right side with all editable fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.66` Edit Sketching Properties**

</div>

6. Fill all the fields and click on "tick mark" on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.67` Edit Sketching Properties**

</div>

7. After successfully added the properties to skecthing,displayed "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.68` Edit Sketching Properties**

</div>

### Search on map 

o Click on "search" icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.69` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.70` Search on map**

</div>


### Measuring Tool

1.Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.png"></p>

<div align="center">

**`Fig.71` submitted records table**

</div>

2.Click On submitted record.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.png"></p>

<div align="center">

**`Fig.72` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.png"></p>

<div align="center">

**`Fig.73` submitted record preview**

</div>

3.Click on "ellipse" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.png"></p>

<div align="center">

**`Fig.74` Click on ellipse button**

</div>

4.click on "sketchings" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.png"></p>

<div align="center">

**`Fig.75` Click on sketching**

</div>

5.Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.png"></p>

<div align="center">

**`Fig.76` View Sketching on map**

</div>

6.Click on "draw tool" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.77` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.png"> </p>

<div align="center"> 

**`Fig.78` Draw tool**

</div>

7.Click on "Draw a polyline" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.79` click on draw a polyline**

</div>

8.By click on "Polyline" icon we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.80` Drawn a poly line**

</div>

9.Click on "Finish" option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.81` Click on Finish Button**

</div>

10.To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.82` Length of distance**

</div>

11.Click on "polygon" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.83` Click on polygon icon**

</div>

12. To draw a polygon then click on "finish" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.84` Drawn polygon on map**

</div>


13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.85` Area of polygon**

</div>

14. Click on draw a Rectangle icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.86` Click on draw a rectangle**

</div>
15.To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.87` Drawn rectangle on map**

</div>

16.To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.88` Area of rectangle**

</div>

17.Click on "Marker" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.89` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.90` Marker on map**

</div>


19.By click on "edit layers" icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.91` Click on Edit layers icon **

</div>

20. We can see drawn sketchings(polyline,polygon,Rectangle,Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.92` sketchings are in editable mode**

</div>

21.After edit sketchings click on save option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.93` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.94` saved sketchings**

</div>

21.Click on "delete" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.95` Click on delete icon**

</div>

22.Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.96` Click on clearAll**

</div>

23.Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.97` Clear All drawed sketchings on map**

</div>

24.Click on "Delete" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.98` Click on delete icon**

</div>

25.Click on drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.99` Click on drawn rectangle**

</div>

26. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.100` Deleted drawn rectangle**

</div>


### Info-Form

To see information of a Form hover on the Form, we can see "info" icon. Click on the "info" icon. 

1.Opens a popup with information of the form in a table. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod30.png"></p>

<div align="center">

**`Fig.101` Info-form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod31.png"></p>

<div align="center">

**`Fig.102` Info-form**

</div>

### Workflow Approval Cycles

We have two types of approval cycles for workflow in FieldOn.

 - Task level work flow,
 - Record level work flow


#### Task Level Workflow:

The “Task Level Workflow” is the one assigned to the “Tasks”, the administrators/moderators in the workflow are responsible to approve the whole task by checking the submissions on it.

Here we take an example of Task Level Workflow on a task. (Note: We have used same workflow as used in task level workflow). 

 - After field user has submitted the assigned records, the workflow gets triggered and the task goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on “Submitted Record” icon.
 
 - Basically, work flow starts from group admin to moderator. After group admin has approved the task after checking the submitted records of that task’s work assignment, moderator can approve or reject that task in approvals tab. 

 - Now when “Moderator” logs in, can find the task in approvals tab, showing as waiting for approval. 
 
 - After moderator approves the records of task’s work assignment, next approver in the work flow will get access to accept or reject records.

 - If the first person of the work flow rejects the task, the task was reassigned to the field user.

#### Record Level Workflow:

The “Record Level Workflow” is the one assigned on the “Tasks”, the administrators/moderators in the workflow are responsible to approve the record by checking the submissions on it.

.Here we take an example of Record Level Workflow on a task.

 - Whenever any record is submitted for this task’s work assignment, the workflow gets triggered and the record goes to the person at the first level of the workflow. Can find the records submitted for work assignment of the task by clicking on “Submitted Record” icon.

 - Basically, work flow starts from group admin to moderator. After group admin has approved the records of a task’s work assignment which is assigned to record level work flow, moderator can be approve or reject records of that task’s work assignment in approvals tab.

 - Now when “Moderator” logs in, can find the record in RED color in forms records, waiting for approval. 

 - After moderator approves the records of task’s work assignment, next approver in the work flow will get access to accept or reject records.

 - If the record is rejected by first approval in the work flow, record was reassigned to the field user.

## Approvals

Click on "approvals", opens approvals tab showing list of tasks (records level, task level) that are assigned to workflow with moderator waiting/ pending for the approval.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod32.png"></p>

<div align="center">

**`Fig.103` Approvals list**

</div>

### Approval flow – task level

- Moderator can accept task by clicking on "accept task" icon in approvals of the task, opens page with signature, comments fields.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod33.png"></p>

<div align="center">

**`Fig.104` Accept task**

</div>

- To accept task upload signature and comments and click on "update".

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod34.png"></p>

<div align="center">

**`Fig.105` Approve task**

</div>

## Rejection flow – task level

- Moderator can reject task, to reject task click on "reject task" icon opens a page with signature and comments.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod35.png"></p>

<div align="center">

**`Fig.106` Reject task**

</div>

- Upload signature, comments and click on "update" the task rejected successfully. When the task is rejected then task moves to previous approver.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod36.png"></p>

<div align="center">

**`Fig.107` Reject task**

</div>

- Click on "workflow history" icon to view work flow details, status, comments.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod37.png"></p>

<div align="center">

**`Fig.108` Workflow history of task with tasklevel workflow**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod38.png"></p>

<div align="center">

**`Fig.109` Workflow history**

</div>

- Click on "work assignments" to view work assignments of task/ project.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod39.png"></p>

<div align="center">

</div>

- Click on "submitted records" icon, to view submitted records of work assignments.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod40.png"></p>

<div align="center">

**`Fig.110` Submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod41.png"></p>

<div align="center">

</div>

### Export to Excel

- To download the data to Excel format, select the records individually or bulk and click on the ‘Export Excel’ button from tabular view.	 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod42.png"></p>

<div align="center">

**`Fig.111` Export to excel**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod43.png"></p>

<div align="center">

**`Fig.112` Columns selection page**

</div>


- Once after clicking on "excel" option, now all the list of fields are displayed select the green color option. The application shows a message saying, "Please check the downloaded records in downloads tab"."

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod44.png"></p>

<div align="center">

</div>

### Export to Email

- Select records and click on "Export to Email" option. Opens email attachment popup.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod45.png"></p>

<div align="center">

**`Fig.113` Export to email**

</div>

- Moderator can email the records to the Valid Email & can also send the same to another email by adding in the Email Id in CC which is optional. Select Excel/pdf and click on send.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod46.png"></p>

<div align="center">

</div>

- Opens columns selection page with list of all columns, selects the columns and click on ✅. it shows a successful pop-up.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod47.png"></p>

<div align="center">

**`Fig.114` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod48.png"></p>

<div align="center">

</div>

### Export to PDF
- Moderator can select single or multiple records and then click on "Pdf" option to generate the submitted data to PDF format.

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod49.png"></p>

<div align="center">

**`Fig.115` Export to pdf**

</div>

- Once after clicking on "pdf" option, columns selection page opens with the list of all columns, select the columns and click on ✅. The application shows a message saying, "Please check the downloaded records in downloads tab".

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod50.png"></p>

<div align="center">

**`Fig.116` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod51.png"></p>

<div align="center">

</div>

### Inserted locations

- Select the records and then click on "Inserted Locations". Locations from where the records were submitted will be displayed as a marker on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod52.png"></p>

<div align="center">

**`Fig.117` Inserted locations**

</div>

### Add columns

- Click on add columns, open columns selection page with list of all columns. Select the columns and click on ✅ 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod53.png"></p>

<div align="center">

**`Fig.118` Add columns**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod55.png"></p>

<div align="center">

**`Fig.119` Columns selection page**

</div>

- The selected columns information can be updated in the table of Submitted records.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod56.png"></p>

<div align="center">

</div>

### Preview of submitted records

- Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod57.png"></p>

<div align="center">

**`Fig.120` Submitted record of task with record level workflow**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod58.png"></p>

<div align="center">

**`Fig.121` Preview of record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod59.png"></p>

<div align="center">

</div>
Click on "actions", shows actions like accept task, reject task, work flow history, sketching, geo tagged images for the task that is assigned with record level work flow.

Shows actions like sketching, geo tagged images also.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod60.png"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit skecthing function moderator can sketch objects on the map. And edit sketching functionality is explained in the below steps.

1.Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by field user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.121` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.122` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.123` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.124`  Sketching**

</div>

4. Edit Skecthing 

o Click on "edit sketching" option on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.125`  Edit Sketching**

</div>

o The options are displayed and click on "measuring tool" to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.126`  Edit Sketching**

</div>

o By clicking on "measuring tool", displayed the options with polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.127` Edit Sketching**

</div>

o If moderator select the "polyline sketching" option, application allows moderator to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.128` Draw sketching by polyline**

</div>

o Click on the "finish" option so that skecthing was end and the polyline is placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.129` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.130` Line sketching**

</div>

o Click on the "tick" icon to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.131` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to save skecthing

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.132` Save sketching**

</div>

o By clicking on "yes" in pop-up menu the sketching saved successfully and displayed "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.133` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps moderator can be sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures moderator can be sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.134` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.135` Polygon sketching**

</div>

o Moderator can place a marker on the map by using "marker" option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.136` Marker sketching**

</div>

6. Delete Skecthing

o Moderator can delete the sketching, to delete the sketching user click on the "delete" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.137` Delete sketching**

</div>

o Click on the respective sketching to get delete

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.138` Delete sketching**

</div>

o Click on the "save" option, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.139` Delete sketching**

</div>

o Click on the "tick" mark option to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.140` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.141` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.142` Update sketching**

</div>

7. Edit Layers

o Moderator click on the "edit sketching" icon on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.143`  Edit Sketching**

</div>

o Moderator click on the "measuring tool" option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.144`  Measuring tool**

</div>

o Moderator click on the "edit layers" option. Moderator can see sketching data are enabled into edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.145`  Edit layers**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.146`  Sketching in edit mode**

</div>


o Moderator can edit by draging handles on the skecthing.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.147`  Drag Handles or marker**

</div>


o After changes are done moderator click on the "save" button, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.148` Save Option**

</div>

o Click on "tick mark" lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.149`  Update Button**

</div>

o Click on "yes" button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.150`  Update Button**

</div>

o Moderator gets "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.151`  Update Button**

</div>

8. Sketching full screen view

o By clicking on "full screen" view option, the map opens in full screen


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.152` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.153` Full screen view sketching**

</div>


### Add/Edit sketching properties in the sketching

1. Moderator can be add and edit  sketching properties information for sketching objects on the map

2. Moderator click on the "edit sketching" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.154`  Edit Sketching**

</div>

3. Moderator click on the sketching object, pop-up menu open with readonly form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.155`  Sketching Properties Pop-Up menu**

</div>

4.Moderator by clicking on "edit" icon in skecting properties, administartor able to edit/add the fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.156`  Sketching Properties **

</div>

5 By clicking on "edit" option, form opens on the right side with all editable fields as shown in fig.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.157` Edit Sketching Properties**

</div>

6. Fill all the fields and click on "tick mark" on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.158` Edit Sketching Properties**

</div>

7. After successfully added the properties to skecthing,gets displayed "toast message".  

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.159` Edit Sketching Properties**

</div>

### Search on map 

o Click on "search icon" on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.160` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.161` Search on map**

</div>


### Measuring Tool

1.Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.png"></p>

<div align="center">

**`Fig.162` submitted records table**

</div>

2.Click On submitted record by field user.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.png"></p>

<div align="center">

**`Fig.163` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.png"></p>

<div align="center">

**`Fig.164` submitted record preview**

</div>

3.Click on "ellipse" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.png"></p>

<div align="center">

**`Fig.165` Click on ellipse button**

</div>

4.click on "sketchings" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.png"></p>

<div align="center">

**`Fig.166` Click on sketching**

</div>

5.View sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.png"></p>

<div align="center">

**`Fig.167` View Sketching on map**

</div>

6.Click on "draw tool" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.168` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.png"> </p>

<div align="center"> 

**`Fig.169` Draw tool**

</div>

7.Click on "Draw a polyline" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.170` click on draw a polyline**

</div>

8.By click on "Polyline" icon we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.171` Drawn a poly line**

</div>

9.Click on "Finish" option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.172` Click on Finish Button**

</div>

10.To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.173` Length of distance**

</div>

11.Click on "polygon" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.174` Click on polygon icon**

</div>

12. To draw a polygon on map then click on "finish" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.175` Drawn polygon on map**

</div>


13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.176` Area of polygon**

</div>

14. Click on draw a "Rectangle" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.177` Click on draw a rectangle**

</div>
15.To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.178` Drawn rectangle on map**

</div>

16.To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.179` Area of rectangle**

</div>

17.Click on "Marker" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.180` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.181` Marker on map**

</div>


19.By click on "edit" layers icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.182` Click on Edit layers icon **

</div>

20. Moderator can see drawn sketchings(polyline,polygon,Rectangle,Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.183` sketchings are in editable mode**

</div>

21.After edit sketchings click on save option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.184` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.185` saved sketchings**

</div>

21.Click on "delete" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.186` Click on delete icon**

</div>

22.Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.187` Click on clearAll**

</div>

23.Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.188` Clear All drawed sketchings on map**

</div>

24.Click on "Delete" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.189` Click on delete icon**

</div>

25.Click on drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.190` Click on drawn rectangle**

</div>

26. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.191` Deleted drawn rectangle**

</div>

### Approval flow- record level
Click on "accept task", opens page with to upload approver signature and comments. After uploading signature, comments click on update. After clicking on update, shows a message as task accepted successfully.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod61.png"></p>

<div align="center">

**`Fig.192` Accept record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod62.png"></p>

<div align="center">

**`Fig.193` Approved record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod63.png"></p>

<div align="center">

</div>

### Rejection flow- record level

Click on "reject task", opens page with to upload approver signature and comments. After uploading signature, comments click on update. After clicking on update, shows a message as task rejected successfully.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod64.png"></p>

<div align="center">

**`Fig.194` Reject record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod65.png"></p>

<div align="center">

**`Fig.195` Reject record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod66.png"></p>

<div align="center">

</div>

Click on "work flow history", opens page click on view details to view the work flow details, comments, status.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod67.png"></p>

<div align="center">

**`Fig.196` Workflow history for record level**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod68.png"></p>

<div align="center">

</div>

Click on "sketching", to see the sketching done by field user in map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod69.png"></p>

<div align="center">

**`Fig.197` Sketching**

</div>

### Measuring Tool

10.Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.png"></p>

<div align="center">

**`Fig.198` Sketching on map**

</div>

11.Click on "draw tool" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.199` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91`.png"> </p>

<div align="center"> 

**`Fig.200` Draw tool**

</div>

10.Click on Draw a "polyline" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.201` click on draw a polyline**

</div>

11.By click on Polyline icon moderator can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.202` Drawn a poly line**

</div>

12.Click on "Finish" option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.203` Click on Finish Button**

</div>

12.To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.204` Length of distance**

</div>

13.Click on "polygon" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.205` Click on polygon icon**

</div>

14. To draw a polygon then click on "finish" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.206` Drawn polygon on map**

</div>


14. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.207` Area of polygon**

</div>

Click on draw a "Rectangle" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.208` Click on draw a rectangle**

</div>
To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.209` Drawn rectangle on map**

</div>

To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.210` Area of rectangle**

</div>

Click on Marker icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.211` click on marker icon**

</div>

To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.212` Marker on map**

</div>


19.By click on "edit layers" icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.213` Click on Edit layers icon **

</div>

20. We can see drawn sketchings(polyline,polygon,Rectangle,Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.214` sketchings are in editable mode**

</div>

After edit sketchings click on "save" option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.215` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.216` saved sketchings**

</div>

20.Click on delete icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.217` Click on "delete" icon**

</div>

21.Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.218` Click on clearAll**

</div>

22.Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.219` Clear All drawed sketchings on map**

</div>

22.Click on "Delete" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.220` Click on delete icon**

</div>

21.Click on drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.221` Click on drawn rectangle**

</div>

22. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.222` Deleted drawn rectangle**

</div>


<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod70.png"></p>

<div align="center">

</div>

Click on "geo tagged images", to see the images that are geo tagged by field user on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod71.png"></p>

<div align="center">

**`Fig.223` Geo taggged images**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod72.png"></p>

<div align="center">

</div>

To edit the submitted records, enable the toggle to edit and make changes and click on "update". Shows message updated successfully.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod73.png"></p>

<div align="center">

**`Fig.224` Edit submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod74.png"></p>

<div align="center">

**`Fig.225` Edit submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod75.png"></p>

<div align="center">

</div>

## Downloads 
The “Downloads” tab provides the feasibility to download the record reports, that is both excel and pdf reports.

It has the details like file name, generated on, size of file, downloaded from, type of record, from date, to date, task name, download option.

The files under “Downloads” tab gets deleted automatically after 24 hours of download.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod76.png"></p>

<div align="center">

**`Fig.226` Downloads**

</div>

## Dashboards:

The Dashboards provide the statistical information present under the particular login. Following are the different roles under the application and their respective dashboards.

Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.

### Moderator:

Under moderator type of entity:

- Approvals

Approvals page showing no of pending tasks with details end date, start date, task name.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod77.png"></p>

<div align="center">

**`Fig.227` Dashboard-Approvals**

</div>






