﻿
# Super User
<!-- {docsify-ignore} -->

Super User is single user who can view all accounts information. In Application we have Dashboard Icon on right side. It is common for all web users based on their roles all tasks, projects, licenses etc.  will be shown.

<p align="center"> <img width="1000" height="500" src="media/SUA.jpg"> </p>

<div align="center"> 

**`Fig` Super User activities**

</div>

<p>Enter valid super user username and password and click on Sign in.</p>

Login as [Super User](https://demo.fieldon.com/#/login)

<p align="center"> <img width="1000" height="450" src="media/SU/SU1.jpg"> </p>

<div align="center"> 

**`Fig.1` FieldOn login page**

</div>

- On successful Login, Super User lands on Accounts page with Welcome message, list of below options in the menu bar.

 - <code>Accounts</code>	
 - <code>Administrators</code>
 - <code>Users</code>
 - <code>Categories</code>
 - <code>Device Management</code>
 - <code>Templates</code>
 - <code>Forms</code>
 - <code>Downloads</code>
 - <code>Settings</code>


<p align="center"> <img width="1000" height="450" src="media/SU/SU2.jpg"> </p>

<div align="center"> 

**`Fig.2` Accounts page**

</div>

<hr>

## Accounts

1.	Super user can see the list of Accounts available.

2.	Super user can search for an Account.

3.	Super user can create an Accounts.

4.	Super user can update an Account.

5.	Super user can disassociate Administrators, Moderator, and Field Users for an Account.

6.	Super user can delete an account.

### View list of Accounts available


1.	Super user can view all the Accounts

<p align="center"> <img width="1000" height="450" src="media/SU/SU3.jpg"> </p>

<div align="center"> 

**`Fig.3` list of All Accounts**

</div>

### Account Search

1.	Super user can search an Account by providing Account name.

2.	In the right-hand top corner, we can see search box.

<p align="center"> <img width="1000" height="450" src="media/SU/SU4.png"> </p>

<div align="center"> 

**`Fig.4` Account search**

</div>

3.  Provide Account name in the search box and click on → icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU5.png"> </p>

<div align="center"> 

**`Fig.5` Search Account**

</div>

4.	Shows the result as per input provided in search box.

<p align="center"> <img width="1000" height="450" src="media/SU/SU6.jpg"> </p>

<div align="center"> 

**`Fig.6` Search Account**

</div>

### Account creation

1.	In the right-hand top corner, ellipse button in visible in accounts page. 

2.	On Click "ellipse" and then click on "create" button to create new ‘Account’.

<p align="center"> <img width="1000" height="450" src="media/SU/SU7.jpg"> </p>

<div align="center"> 

**`Fig.7` create account**

</div>

3.	A creation form gets opened with fields as follows - 

 - Account name – Mandatory,
 - Email – Mandatory, 
 - Description – Optional.

<p align="center"> <img width="1000" height="450" src="media/SU/SU8.jpg"> </p>

<div align="center"> 

**`Fig.8` create form of account**

</div>

4.	Fill the mandatory fields and click the create button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU9.jpg"> </p>

<div align="center"> 

**`Fig.9` Create account**

</div>

5.	After successful creation Super user can see a toasted message as “Account created successfully”

<p align="center"> <img width="1000" height="450" src="media/SU/SU10.jpg"> </p>

<div align="center"> 

**`Fig.10` Create account**

</div>

Note : Duplicate Account names are not allowed.

6.	In between Super user can cancel the creation process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU12.jpg"> </p>

<div align="center"> 

**`Fig.12` Cancel account creation**

</div>

7.	On click cancel, It shows the alert of data loss.

<p align="center"> <img width="1000" height="450" src="media/SU/SU13.jpg"> </p>


<div align="center"> 

**`Fig.13` Cancel account creation**

</div>

8.	Click on “Ok”, application redirects to Accounts page.

9. Click on “Cancel”, application remains in the same page.

### Account Edit/Update

1.	To edit ‘Account’, hover on the account,  following buttons are visible

 - Edit
 - Disassociation
 - Delete

<p align="center"> <img width="1000" height="450" src="media/SU/SU14.jpg"> </p>

<div align="center"> 

**`Fig.14` Edit account**

</div>

2.	Click on "Edit" button, view the existing data, and Super user can update Email and Description fields.

<p align="center"> <img width="1000" height="450" src="media/SU/SU15.jpg"> </p>

<div align="center"> 

**`Fig.15` Edit account form**

</div>

3.	Once the changes are done, Click on "Update" button to update the account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU16.jpg"> </p>

<div align="center"> 

**`Fig.16` Edit account**

</div>

4.	On successful Account update, toasted messages will be shown as “Account Admin updated successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU17.png"> </p>

<div align="center"> 

**`Fig.17` Edit account**

</div>

5.	To discard the edit process, Super user can click on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU18.jpg"> </p>

<div align="center"> 

**`Fig.18` Cancel edit account**

</div>

### Account Delete

1.	To delete ‘Account’ hover on the account, delete button is visible.

<p align="center"> <img width="1000" height="450" src="media/SU/SU19.jpg"> </p>

<div align="center"> 

**`Fig.19` Delete account**

</div>

2.	Once click on "Delete" button, application shows the conformation pop up, then click on "yes"

<p align="center"> <img width="1000" height="450" src="media/SU/SU20.jpg"> </p>

<div align="center"> 

**`Fig.20` Delete account**

</div>

3.	After click on Yes button, application shows the toasted message as “Account deleted successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU21.jpg"> </p>

<div align="center"> 

**`Fig.21` Account delete message**

</div>

4.	If click on ‘No’, Application shows accounts page and account will not be deleted.

5.	Account cannot be deleted if account is associated with any administrator. And shows the message as Account cannot be deleted, as it is associated with “Admin username”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU22.png"> </p>

<div align="center"> 

**`Fig.22` Alert message for accosiated account delete**

</div>

### Account Dissociation

1.	To remove any Administrators from a particular Account, Super user need to select the Account and click on Dissociation button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU23.jpg"> </p>

<div align="center"> 

**`Fig.23` Account dissociation**

</div>

2.	If admins are not associated, then it shows “No data available”.

3.	Once click Admins dropdown, we can see the list of Administrators Associated, Select the type as Admins in right-hand top dropdown.

<p align="center"> <img width="1000" height="450" src="media/SU/SU24.jpg"> </p>

<div align="center"> 

**`Fig.24` Account dissociation**

</div>
4.	If admins are available, then it shows the list of admins available.

5.	To disassociate Admin, Select the Username and click on "Remove" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU25.jpg"> </p>

<div align="center"> 

**`Fig.25` List of admin in account dissociate page**

</div>

6.	On clicking on Remove it shows the tosted message as “Admin removed successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU26.png"> </p>

<div align="center"> 

**`Fig.26` Admin removed message**

</div>

7.	In between if super user wants to cancel the disassociation of Admin, click on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU27.jpg"> </p>

<div align="center"> 

**`Fig.27` Cancel dissociation of account**

</div>

8.	To disassociate Group admin, Select Group admin in Admins dropdown.

<p align="center"> <img width="1000" height="450" src="media/SU/SU29.jpg"> </p>

<div align="center"> 

**`Fig.28` List of group admin in dissociate page**

</div>

9.	List of Group Admins are shown.

10.	Select the Username and click on "Remove" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU30.jpg"> </p>

<div align="center"> 

**`Fig.29` Dissociate account with group admin**

</div>

11.	On click on Remove, Toasted message will be shown as “Admin removed successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU26.png"> </p>

<div align="center"> 

**`Fig.30` Admin removed message**

</div>

12.	In Between, if Super user wants to cancel the disassociation of Group Admin, click on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU32.jpg"> </p>

<div align="center"> 

**`Fig.31` Cancel dissociation**

</div>

13.	To disassociate Moderator, Select Moderator from the Admins dropdown.

14.	List of Moderators are shown.

<p align="center"> <img width="1000" height="450" src="media/SU/SU34.jpg"> </p>

<div align="center"> 

**`Fig.32` List of moderators in dissociate page**

</div>

15.	Select the username and click on "Remove button".

<p align="center"> <img width="1000" height="450" src="media/SU/SU35.jpg"> </p>

<div align="center"> 

**`Fig.33` Dissociate account with moderator**

</div>

16.	On click on Remove, Toasted message will be shown as “Admin removed successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU26.png"> </p>

<div align="center"> 

**`Fig.34` Admin removed message**

</div>

17.	To cancel the disassociation of Moderator, click on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU37.jpg"> </p>

<div align="center"> 

**`Fig.35` cancel dissociation**

</div>

18.	To redirect back to accounts page, click on back icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU38.jpg"> </p>

<div align="center"> 

**`Fig.36` Navigate back to account page**

</div>

19.	To disassociate Field user, Select User from the Admins dropdown.

20.	On selecting Users, list of all the users are shown. 

<p align="center"> <img width="1000" height="450" src="media/SU/SU39.png"> </p>

<div align="center"> 

**`Fig.37` List of field users in dissociate page**

</div>

21.	Select the username and click on "Remove" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU40.jpg"> </p>

<div align="center"> 

**`Fig.38` Dissociate account with users**

</div>

22.	On click on Remove, Toasted message will be shown as “Admin removed successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU26.png"> </p>

<div align="center"> 

**`Fig.39` Admin removed message**

</div>

23.	In between super user can cancel the disassociation of field user, click on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU41.jpg"> </p>

<div align="center"> 

**`Fig.40` Cancel dissociation**

</div>

24.	To redirect back to accounts page, click on back icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU42.jpg"> </p>

<div align="center"> 

**`Fig.41` Navigate back to accounts page**

</div>

## Administrators

1.	On click of Administrators tab, Super user can see the list of Administrators available.

<p align="center"> <img width="1000" height="450" src="media/SU/SU43.jpg"> </p>

<div align="center"> 

**`Fig.42` list of administrators**

</div>

2.	Super user can search for Administrator, Moderator by Username.

3.	Super user can filter the Administrator by using filter in the right-hand top.

4.	Super user can see the following Administrators:

    - Account Administrators
	- Group Administrators
	- Moderators

5.	Super user can create Administrators.

6.	Super user can update Administrators.

7.	Super user can delete Administrators.

8.	Super user can reset the password.

### Administrator search

1.	Super user can search by providing username in the search box.

2.	Enter the username and click on → icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU44.jpg"> </p>

<div align="center"> 

**`Fig.43` Search administrators**

</div>

3.	 Successful result of search.

<p align="center"> <img width="1000" height="450" src="media/SU/SU45.jpg"> </p>

<div align="center"> 

**`Fig.44` Search administrators**

</div>

### Filter administrators

1.	Super user can filter the administrators with Administrator type.

2.	Click on filter and select “All”, Shows all the Administrators.

<p align="center"> <img width="1000" height="450" src="media/SU/SU46.jpg"> </p>

<div align="center"> 

**`Fig.45` Filter administrators**

</div>

3.	Click on filter and select “Account Administrator”, Shows all the Account Administrators.

<p align="center"> <img width="1000" height="450" src="media/SU/SU47.jpg"> </p>

<div align="center"> 

**`Fig.46` Filter account administrators**

</div>

4.	Click on filter and select “Group Administrator”, Shows all the Group Administrators.

<p align="center"> <img width="1000" height="450" src="media/SU/SU48.jpg"> </p>

<div align="center"> 

**`Fig.47` Filter group administrators**

</div>

5.	Click on filter and select “Moderator”, Shows all the Moderators.

<p align="center"> <img width="1000" height="450" src="media/SU/SU49.jpg"> </p>

<div align="center"> 

**`Fig.48` Filter moderators**

</div>

### Administrator creation

1.	Super User can create the Administrator by clicking on the "create" button in ellipse on the right-hand top.

<p align="center"> <img width="1000" height="450" src="media/SU/SU50.jpg"> </p>

<div align="center"> 

**`Fig.49` Create administrator**

</div>

2.	On click of create a form will open fill the details and click on "create".

<p align="center"> <img width="1000" height="450" src="media/SU/SU51.jpg"> </p>

<div align="center"> 

**`Fig.50` create form of administrator**

</div>

3.	We have Account Administrator, Group Administrator and Moderators.

4.	Based on the check box selection Administrators are created.

## Account Administrator

### Creation of Account Administrator

1. In creation page, the following fields are displayed

 - First name – Mandatory
 - Last name – Mandatory
 - Email – Mandatory
 - Phone – Mandatory
 - Role – Optional
 - Account – Optional
 - Avatar – Optional


<p align="center"> <img width="1000" height="450" src="media/SU/SU52.jpg"> </p>

<div align="center"> 

**`Fig.51` Account administrator creation**

</div>

2. Select Account administrator to create Account admin

<p align="center"> <img width="1000" height="450" src="media/SU/SU53.jpg"> </p>

<div align="center"> 

**`Fig.52` Account administrator creation**

</div>

3. Select account to associate administrator to accounts.

4. Select avatar and upload image for easy identification.

5. Click on create button to create account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU54.jpg"> </p>

<div align="center"> 

**`Fig.53` Account administrator creation**

</div>

6. On successful creation, Shows the toasted message as “Account Administrator created successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU55.jpg"> </p>

<div align="center"> 

**`Fig.54` Account administrator creation**

</div>

8. Super user can Cancel the creation process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU57.jpg"> </p>

<div align="center"> 

**`Fig.55` Cancel account administrator creation**

</div>

### Edit Account administrator

1. Mouse hover on admin card to see the edit button and click on 'Edit' icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU58.jpg"> </p>

<div align="center"> 

**`Fig.56` Edit account administrator**

</div>

2. On click 'Edit' icon, edit page will be opened with the following fields
 - Username – Read-only
 - First name – Editable
 - Last name – Editable
 - Email – Editable
 - Phone – Editable
 - Role – Read-only
 - Accounts – Editable (Note: User can only add the new accounts)


<p align="center"> <img width="1000" height="450" src="media/SU/SU59.jpg"> </p>

<div align="center"> 

**`Fig.57` Edit account administrator**

</div>

3. After modifications, Click on "update" button

<p align="center"> <img width="1000" height="450" src="media/SU/SU60.jpg"> </p>

<div align="center"> 

**`Fig.58` Update account administrator**

</div>

4. On click Update, Toasted message will be shown as “Admin updated successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU61.jpg"> </p>

<div align="center"> 

**`Fig.59` Admin updated message**

</div>

5. Super user can Cancel the update process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU63.jpg"> </p>

<div align="center"> 

**`Fig.60` Cancel edit account administrator**

</div>

### Delete account administrator

1. If Administrator is associated with account then Account admin cannot be deleted.

<p align="center"> <img width="1000" height="450" src="media/SU/SU64.jpg"> </p>

<div align="center"> 

**`Fig.61` Delete account administrator**

</div>

2. If account admin is not associated with any account, then Account admin can be deleted.

<p align="center"> <img width="1000" height="450" src="media/SU/SU65.jpg"> </p>

<div align="center"> 

**`Fig.62` Delete account administrator**

</div>

## Group administrators

### Create group administrator

1. In creation page, the following fields are displayed

 - First name – Mandatory
 - Last name – Mandatory
 - Email – Mandatory
 - Phone – Mandatory
 - Role – Optional
 - Account – Optional
 - Avatar – Optional

<p align="center"> <img width="1000" height="450" src="media/SU/SU66.jpg"> </p>

<div align="center"> 

**`Fig.63` Group administrator creation**

</div>
2. Select Group administrator to create group admin

<p align="center"> <img width="1000" height="450" src="media/SU/SU67.jpg"> </p>

<div align="center"> 

**`Fig.64` Group administrator creation**

</div>

3. Select account to associate administrator to accounts.

4. Select avatar and upload image for easy identification.

5. Click on "create" button to create account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU68.jpg"> </p>

<div align="center"> 

**`Fig.65` Group administrator creation**

</div>

6. On successful creation, Shows the toasted messages as “Group Administrator created successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU69.jpg"> </p>

<div align="center"> 

**`Fig.66` Group administrator creation**

</div>

7. Super user can Cancel the creation process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU71.jpg"> </p>

<div align="center"> 

**`Fig.67` Cancel group administrator creation**

</div>

### Edit Group administrator

1. Mouse hover on admin card to see the ‘Edit’ icon and click on Edit icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU72.jpg"> </p>

<div align="center"> 

**`Fig.68` Edit group administrator**

</div>

2. On click Edit icon, edit page will be opened with the following fields
 - Username – Read-only
 - First name – Editable
 - Last name – Editable
 - Email – Editable
 - Phone – Editable
 - Role – Read-only
 - Accounts – Editable (Note: User can only add the new accounts)

<p align="center"> <img width="1000" height="450" src="media/SU/SU73.jpg"> </p>

<div align="center"> 

**`Fig.69` Edit group administrator**

</div>

3. After modifications, Click on update icon

<p align="center"> <img width="1000" height="450" src="media/SU/SU74.jpg"> </p>

<div align="center"> 

**`Fig.70` Update group administrator**
</div>

4. On click "Update" button, Toasted message will be shown as “Admin updated successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU75.jpg"> </p>

<div align="center"> 

**`Fig.71` Admin updated message**

</div>

5. Super user can Cancel the update process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU77.jpg"> </p>

<div align="center"> 

**`Fig.72` Cancel edit group administrator**

</div>

### Delete group administrator

1. If Administrator is associated with account then group administrator cannot be deleted.

<p align="center"> <img width="1000" height="450" src="media/SU/SU78.jpg"> </p>

<div align="center"> 

**`Fig.73` Delete group administrator**

</div>

2. If group administrator is not associated with any account, then group administrator can be deleted.

<p align="center"> <img width="1000" height="450" src="media/SU/SU79.jpg"> </p>

<div align="center"> 

**`Fig.74` Delete group administrator**

</div>

## Moderator

### Create moderator

1. In creation page, the following fields are displayed

 - 	First name – Mandatory
 -	Last name – Mandatory
 -	Email – Mandatory
 -	Phone – Mandatory
 -	Role – Optional
 -	Account – Optional
 - 	Avatar – Optional


<p align="center"> <img width="1000" height="450" src="media/SU/SU80.jpg"> </p>

<div align="center"> 

**`Fig.75` Moderator creation**

</div>

2. If Role is not selected, then Moderator will be created.

<p align="center"> <img width="1000" height="450" src="media/SU/SU81.jpg"> </p>

<div align="center"> 

**`Fig.76` Moderator creation**

</div>

3. Select account to associate administrator to accounts.

4. Select avatar and upload image for easy identification.

5. Click on "create" button to create account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU82.jpg"> </p>

<div align="center"> 

**`Fig.77` Moderator creation**

</div>

6. On successful creation, Shows the toasted messages as “Moderator created successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU83.jpg"> </p>

<div align="center"> 

**`Fig.78` Moderator creation**

</div>

7. Super user can Cancel the creation process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU85.jpg"> </p>

<div align="center"> 

**`Fig.79` Cancel moderator creation**

</div>

### Edit Moderator

1. Mouse hover on admin card to see the ‘Edit’ icon and click on Edit icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU86.jpg"> </p>

<div align="center"> 

**`Fig.80` Edit moderator**

</div>

2. On click Edit icon, edit page will be opened with the following fields

 -	Username – Read-only
 -	First name – Editable
 -	Last name – Editable
 -	Email – Editable
 -	Phone – Editable
 - 	Role – Read-only
 -	Accounts – Editable (Note: User can only add the new accounts)

<p align="center"> <img width="1000" height="450" src="media/SU/SU87.jpg"> </p>

<div align="center"> 

**`Fig.81` Edit moderator**

</div>

3. After modifications, Click on "update" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU88.jpg"> </p>

<div align="center"> 

**`Fig.82` Update Moderator**

</div>

4. On click Update button, Toasted message will be shown as “Admin updated successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU89.jpg"> </p>

<div align="center"> 

**`Fig.83` Admin updated message**

</div>

5. Super user can Cancel the update process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU91.jpg"> </p>

<div align="center"> 

**`Fig.84` Cancel edit moderator**

</div>

### Delete moderator

1. If Administrator is associated with account then moderator cannot be deleted.

<p align="center"> <img width="1000" height="450" src="media/SU/SU92.jpg"> </p>

<div align="center"> 

**`Fig.85` Delete moderator**

</div>

2. If moderator is not associated with any account, then moderator can be deleted.

<p align="center"> <img width="1000" height="450" src="media/SU/SU93.jpg"> </p>

<div align="center"> 

**`Fig.86` Delete moderator**

</div>

<code>Note :</code> If Role is not selected, then Moderator will be created.

## Administrator password reset

1.	Super user can reset the password for Account Administrator, Group Administrators, and Moderator to default password.

2.	Mouse hover on Administrator card and click on password reset icon.

3.	Default password is ‘mm@1234’.

<p align="center"> <img width="1000" height="450" src="media/SU/SU95.jpg"> </p>

<div align="center"> 

**`Fig.87` Reset administrator password**

</div>

4.	Once click on ‘Reset’ icon, application shows the conformation pop up, then select the ‘yes’ option.

5.	 After click on ‘Yes’ button, application shows the toasted message as "Admin password reset successful".

<p align="center"> <img width="1000" height="450" src="media/SU/SU94.jpg"> </p>

<div align="center"> 

**`Fig.88` Password reset toast message**

</div>

## Users

1.	Super user can search for a Field user.

2.	Super user can update account for Field user.

3.	Super user can check the activities of Field user.

### Search user

1.	Super user can search for a Field user.

<p align="center"> <img width="1000" height="450" src="media/SU/SU96.jpg"> </p>

<div align="center"> 

**`Fig.89` Search users**

</div>

2.  Click on the search box and enter the username.

3.  Click on  → icon, then it shows the result.

<p align="center"> <img width="1000" height="450" src="media/SU/SU97.jpg"> </p>

<div align="center"> 

**`Fig.90` Search users**

</div>

### Edit user

Edit user function defines, super admin can edit/ update the account for field user.

1.	To edit the user profile, Mouse hover on user card.

2.	Edit icon will be appeared.

<p align="center"> <img width="1000" height="450" src="media/SU/SU98.jpg"> </p>

<div align="center"> 

**`Fig.91` Edit users**

</div>


3.	On Click 'Edit' icon, Opens the edit page with following fields.

 - Username – Read-only
 - First name – Read-only
 - Last name – Read-only
 - Email – Read-only
 - Phone – Read-only
 - Accounts – Editable (Note: can only add accounts)
 - Avatar – Read-only

4.	Super user has only Accounts field as editable and super user can add accounts.

<p align="center"> <img width="1000" height="450" src="media/SU/SU99.jpg"> </p>

<div align="center"> 

**`Fig.92` Edit users**

</div>

5.  After adding the account, super user can click on "update" button to update the field user to specific account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU100.jpg"> </p>

<div align="center"> 

**`Fig.93` Edit users**

</div>

6.  On successful update, toasted message will be shown as “User updated successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU101.jpg"> </p>

<div align="center"> 

**`Fig.94` User updated message**

</div>

7.  To cancel the process, Super user can click on "Cancel" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU102.jpg"> </p>

<div align="center"> 

**`Fig.95` Cancel edit users**

</div>


### Users Activity

1.  Click on Activity tab.

<p align="center"> <img width="1000" height="450" src="media/SU/SU104.jpg"> </p>

<div align="center"> 

**`Fig.96`  Users Activity tab**

</div>

2.  Select From date, To date and select Account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU105.jpg"> </p>

<div align="center"> 

**`Fig.97` Users activity**

</div>

3.  If Super user selects Accounts as “All” and click on 'Filter' icon,Shows all the activities with Activity, Username, Date, Device model and version.

<p align="center"> <img width="1000" height="450" src="media/SU/SU106.jpg"> </p>

<div align="center"> 

**`Fig.98` User activities of all accounts**

</div>
4.  If Super user selects Accounts as “a specific account” and click on Filter icon, Shows all the activities with Activity, Username, Date, Device model and version.

<p align="center"> <img width="1000" height="450" src="media/SU/SU107.jpg"> </p>

<div align="center"> 

**`Fig.99` User activity of an account**

</div>

## Categories

1.	When clicked on Categories tab, we can see the list of Categories available.

2.	Super Admin and Account Administrator can create Categories. 

<p align="center"> <img width="1000" height="450" src="media/SU/SU108.jpg"> </p>

<div align="center"> 

**`Fig.100` Categories tab**

</div>

### Search category

1.	Super user can search category by providing category name in search box.

2.	Enter category name in search box and then click on  → icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU109.jpg"> </p>

<div align="center"> 

**`Fig.101` Search category**

</div>

### Category creation

1.  On the right-hand top corner, ellipse button is displayed in category page. 

2.	On Click of "create" button in ellipse we can create new ‘Category’.

3.	A form will be opened with name and description.

<p align="center"> <img width="1000" height="450" src="media/SU/SU110.jpg"> </p>

<div align="center"> 

**`Fig.102` Create category**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU111.jpg"> </p>

<div align="center"> 

**`Fig.103` Create category**

</div>

4.	Fill the following field details 
  - Category Name – Mandatory 
  - Description – Optional  
and click the create button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU112.jpg"> </p>

<div align="center"> 

**`Fig.104` Create category**

</div>

5.	After successful creation, Super user can see a toasted message as "Category created successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU113.jpg"> </p>

<div align="center"> 

**`Fig.105` Category created message**

</div>

<code> Note:</code> Duplicate names not allowed.

6.	Click on "cancel" button to cancel the process.

<p align="center"> <img width="1000" height="450" src="media/SU/SU114.jpg"> </p>

<div align="center"> 

**`Fig.106` Cancel category creation**

</div>

### Edit category

1.	To edit ‘Category’, hover on the category, Super user can see the following buttons:

  - Edit
  - Delete

<p align="center"> <img width="1000" height="450" src="media/SU/SU116.jpg"> </p>

<div align="center"> 

**`Fig.107` Edit category**

</div>

2.	Click on Edit button, view the existing data with following fields:

 - Category name – Read-only
 - Description – Editable, 

<p align="center"> <img width="1000" height="450" src="media/SU/SU117.jpg"> </p>

<div align="center"> 

**`Fig.108` Edit category**

</div>

3.  After modifications, click on "update" button to update category.

<p align="center"> <img width="1000" height="450" src="media/SU/SU119.jpg"> </p>

<div align="center"> 

**`Fig.109` Edit category**

</div>

4.  On click "update" button, it shows the message as "category updated successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU120.jpg"> </p>

<div align="center"> 

**`Fig.110` Category updated message**

</div>

5.  In between If Super user wants to cancel the update process, then super user can cancel the update process by clicking "Cancel" button.
 
<p align="center"> <img width="1000" height="450" src="media/SU/SU121.jpg"> </p>

<div align="center"> 

**`Fig.111` Cancel edit category**

</div>

### Delete category

1.	To delete ‘Category’ hover on the category to see the buttons.

<p align="center"> <img width="1000" height="450" src="media/SU/SU123.jpg"> </p>

<div align="center"> 

**`Fig.112` Delete category**

</div>

2.	Once click on ‘Delete’ icon, application shows the conformation pop up, then select the ‘yes’ option.

<p align="center"> <img width="1000" height="450" src="media/SU/SU124.jpg"> </p>

<div align="center"> 

**`Fig.113` Delete category**

</div>

3.	After click on ‘Yes’ button, application shows the toasted message as "category deteled successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU125.jpg"> </p>

<div align="center"> 

**`Fig.114` Delete category**

</div>
4.	Cannot delete if category is associated with template.

<p align="center"> <img width="1000" height="450" src="media/SU/SU126.jpg"> </p>

<div align="center"> 

**`Fig.115` Delete category**

</div>

## Templates

1.	When clicked on Templates tab, displays list of Templates available.

<p align="center"> <img width="1000" height="450" src="media/SU/SU127.jpg"> </p>

<div align="center"> 

**`Fig.116` Templates tab**

</div>

2.	Super user can create Templates.

3.	Super user can preview Templates.

4.	Super user can update Templates.

5.	Super user can delete Templates.

### Search template

1.	Super user can search the template by Template name.

<p align="center"> <img width="1000" height="450" src="media/SU/SU128.jpg"> </p>

<div align="center"> 

**`Fig.117` Search templates**

</div>

2.	Enter the template name in the search box and click on  → icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU129.jpg"> </p>

<div align="center"> 

**`Fig.118` Search templates**

</div>

3.	Lists all the templates by given name in the search box.

4.	On success, result shows all the available templates with given name.

5.	If templates are not available then it shows the message as “No data available”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU130.jpg"> </p>

<div align="center"> 

**`Fig.119` Search templates**

</div>

### Create template

1.  On the right-hand top corner, 'ellipse' button is visible in templates page. 

2.	On Click of "create" button in ellipse, Super user can create new ‘Template’.

<p align="center"> <img width="1000" height="450" src="media/SU/SU131.jpg"> </p>

<div align="center"> 

**`Fig.120` Create templates**

</div>

3.	A form will be opened with following fields 
 - name, 
 - category, 
 - Template Type.

<p align="center"> <img width="1000" height="450" src="media/SU/SU132.jpg"> </p>

<div align="center"> 

**`Fig.121` Create templates**

</div>

Template types are

 - Private
 - Public

Private: If private selected, then the template will be visible to only selected Accounts.

<p align="center"> <img width="1000" height="450" src="media/SU/SU133.jpg"> </p>

<div align="center"> 

**`Fig.122` Create templates**

</div>

Public: If public selected, then the template will be visible to all Accounts.

<p align="center"> <img width="1000" height="450" src="media/SU/SU134.jpg"> </p>

<div align="center"> 

**`Fig.123` Create templates**

</div>

1.	Creation of Public and Private template

2.	After clicking of ‘Go’ button for public template

<p align="center"> <img width="1000" height="450" src="media/SU/SU135.jpg"> </p>

<div align="center"> 

**`Fig.124` Create templates**

</div>

3.	It will open ‘Form Builder’ where sper user can drag and drop different widgets.

<p align="center"> <img width="1000" height="450" src="media/SU/SU136.jpg"> </p>

<div align="center"> 

**`Fig.125` Create templates**

</div>

4. Drag and drop the widgets to the pallate to create Template.

•	Following are the types of widgets and fields:

<code>Form Widgets</code>

 - Input
 - Text area
 - Dropdown
 - Checkbox
 - Radio
 - Number
 - Rating
 - Calendar

<code>Media Widgets</code>
 - Camera
 - Video
 - Signature
 - Barcode

<code>Add on Widgets</code>
 - Calculator
 - Header
 - Map
 - Table
 - Time
 - Properties
 - Reference List

<p align="center"> <img width="1000" height="450" src="media/SU/SU137.jpg"> </p>

<div align="center"> 

**`Fig.126` Create templates**

</div>

5.	After drag and drop of widget, we need to fill the properties of the widget and need to save the property.

<p align="center"> <img width="1000" height="450" src="media/SU/SU138.jpg"> </p>

<div align="center"> 

**`Fig.127` Create templates**

</div>

6.	After saving of the widget Super user need to save the Template by clicking the 'save' icon in the top right corner.

<p align="center"> <img width="1000" height="450" src="media/SU/SU139.jpg"> </p>

<div align="center"> 

**`Fig.128` Create templates**

</div>

7.	After clicking of the save icon a popup opens with a drop down, select the Display fields and save the Template.

<p align="center"> <img width="1000" height="450" src="media/SU/SU140.jpg"> </p>

<div align="center"> 

**`Fig.129` Create templates**

</div>

8.	Select the display fields

<p align="center"> <img width="1000" height="450" src="media/SU/SU141.jpg"> </p>

<div align="center"> 

**`Fig.130` Create templates**

</div>

9.	Then click on green tick mark to create template.

10.	On click on green tick icon, toasted message will be displays as “Template created successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU142.jpg"> </p>

<div align="center"> 

**`Fig.131` Template created message**

</div>

<code>Note:</code>  Duplicate names not allowed.

11.	While creating process of template, Super user can navigate back to templates page by clicking on back on

<p align="center"> <img width="1000" height="450" src="media/SU/SU143.jpg"> </p>

<div align="center"> 

**`Fig.132` Navigate back to templates page**

</div>

12.	While creating process of template Super user can cancel the process and can navigates back to the templates page by Clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU144.jpg"> </p>

<div align="center"> 

**`Fig.133` Cancel template creation**

</div>

### Template preview

1.	Mouse hover on template card, the 'preview' icon is visible.

2.	On click 'preview' icon, template will be shown in read-only format.

<p align="center"> <img width="1000" height="450" src="media/SU/SU145.jpg"> </p>

<div align="center"> 

**`Fig.134` Preview of template**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU146.jpg"> </p>

<div align="center"> 

**`Fig.135` Preview of template**

</div>

### Edit template

1. To edit a Template, mouse hover on the Template, 'edit' icon is visible.

<p align="center"> <img width="1000" height="450" src="media/SU/SU147.jpg"> </p>

<div align="center"> 

**`Fig.136` Edit template**

</div>

2. On Click of edit button on the template card super user can the see existing basic details.

<p align="center"> <img width="1000" height="450" src="media/SU/SU148.jpg"> </p>

<div align="center"> 

**`Fig.137` Edit template**

</div>

3.	After clicking of "Go" button. It will open ‘Form Builder’ with the existing widgets and can add new widgets to the Template.

4.	We need to drag and drop the widgets to the palate to add new widgets to Template.

5.	After drag and drop of widget, fill the properties of the widget and save the properties.

<p align="center"> <img width="1000" height="450" src="media/SU/SU149.jpg"> </p>

<div align="center"> 

**`Fig.138` Edit template**

</div>

6.	After saving of the widget, save the Template by clicking the 'save' icon in the top right corner.

<p align="center"> <img width="1000" height="450" src="media/SU/SU150.jpg"> </p>

<div align="center"> 

**`Fig.139` Edit template**

</div>

7.	After clicking of the save button, displays a popup with a drop down, select the Display fields and update the Template.

<p align="center"> <img width="1000" height="450" src="media/SU/SU151.jpg"> </p>

<div align="center"> 

**`Fig.140` Edit template**

</div>
8.	On click update, Template gets updated.

<p align="center"> <img width="1000" height="450" src="media/SU/SU152.jpg"> </p>

<div align="center"> 

**`Fig.141` Edit template**

</div>

9.	On click cancel, super user can navigate back to templates page without applying changes.

<p align="center"> <img width="1000" height="450" src="media/SU/SU154.jpg"> </p>

<div align="center"> 

**`Fig.142` Cancel edit template**

</div>

10.	Super user can copy the existing template by Clicking on "Copy" button with same widget with different form name.

Hover on template and click on edit option
 - Click on Go button
 - Click on save template icon
 - Click on Copy button

<code>Note:</code> Super user can change private template to public and vice-versa Super user can add or remove or modify the widgets.

<p align="center"> <img width="1000" height="450" src="media/SU/SU155.jpg"> </p>

<div align="center"> 

**`Fig.143` Copy template**

</div>

11.	On click copy button, opens copy template with following fields:
 - New form name
 - display fields

<p align="center"> <img width="1000" height="450" src="media/SU/SU156.jpg"> </p>

<div align="center"> 

**`Fig.144` Copy template**

</div>

12.	Enter new form name and select display fields.

13.	Click Copy button, Template will be copied successfully, and toasted message will be shown as “Template copied successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU157.jpg"> </p>

<div align="center"> 

**`Fig.145` Copy template**

</div>

14.	Super user can cancel the copy process by Clicking on cancel button, and Redirects back to the template update page.

<p align="center"> <img width="1000" height="450" src="media/SU/SU158.jpg"> </p>

<div align="center"> 

**`Fig.146` Cancel copy template**

</div>

### Template Delete

1.	To delete ‘Template’ hover on the template, to see the icons.

<p align="center"> <img width="1000" height="450" src="media/SU/SU159.jpg"> </p>

<div align="center"> 

**`Fig.147` Delete template**

</div>

2.	Once click on ‘Delete’ icon, application shows the confirmation pop up, then select the ‘yes’ option.

3.	After click on ‘Yes’ button, application shows the toast message as "template deleted successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU160.jpg"> </p>

<div align="center"> 

**`Fig.148` Delete template**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU161.jpg"> </p>

<div align="center"> 

**`Fig.149` Template deleted message**

</div>

If super user clicks on “No”, Then template delete process gets cancelled.

## Forms

1.	When clicked on Forms tab, Super user can see the list of Forms available.

<p align="center"> <img width="1000" height="450" src="media/SU/SU162.jpg"> </p>

<div align="center"> 

**`Fig.150` Forms page**

</div>

2.	Super user can search for form.

3.	Super user can preview the Form.

4.	Super user can view submitted records of a form.

5.	Super user can share the form.

6.	Super user can view form information.

7.	Super user can import form as template.

### Form Search

1.	Super user can search the form by providing the input as form name in the search box.

<p align="center"> <img width="1000" height="450" src="media/SU/SU163.jpg"> </p>

<div align="center"> 

**`Fig.151` Search Forms**

</div>

2.	Result for the input provided by super user.

<p align="center"> <img width="1000" height="450" src="media/SU/SU164.jpg"> </p>

<div align="center"> 

**`Fig.152` Search Forms**

</div>

### Filter Forms

1.	 Click on filter icon and select “All”, Shows both public and private forms.

<p align="center"> <img width="1000" height="450" src="media/SU/SU165.jpg"> </p>

<div align="center"> 

**`Fig.153` Filter Forms**

</div>

2.	Click on filter icon and select Public, shows all the public forms.

<p align="center"> <img width="1000" height="450" src="media/SU/SU166.jpg"> </p>

<div align="center"> 

**`Fig.154` Filter Forms**

</div>

3.	Click on filter icon and select Private, shows all the private forms.

<p align="center"> <img width="1000" height="450" src="media/SU/SU167.jpg"> </p>

<div align="center"> 

**`Fig.155` Filter Forms**

</div>

4.	Forms can be categorized by groups  by clicking on Group by category icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU168.jpg"> </p>

<div align="center"> 

**`Fig.156` Group by category of Forms**

</div>

### Form Preview

1.	Super user can preview the Form.

2.	To Preview the Form, go to the form and hover on the form card.

3.	Super user can see preview button.

4.	Click on preview button a popup will be open with all the widgets in the form.

<p align="center"> <img width="1000" height="450" src="media/SU/SU170.jpg"> </p>

<div align="center"> 

**`Fig.157` Form preview**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU172.jpg"> </p>

<div align="center"> 

**`Fig.158` Form preview**

</div>

#### Form Import as Template

1.	Go to the Form tab and mouse hover on the Form, Import button is visible. 

<p align="center"> <img width="1000" height="450" src="media/SU/SU173.jpg"> </p>

<div align="center"> 

**`Fig.159` Import Form as template**

</div>

2.	On click import icon, import as template popup will opened.

<p align="center"> <img width="1000" height="450" src="media/SU/SU174.jpg"> </p>

<div align="center"> 

**`Fig.160` Import Form as template**

</div>

3.	Fill the following fields:

 •	Template name

 •	Categories

<code>Note:</code> Categories can be selected multiple

<p align="center"> <img width="1000" height="450" src="media/SU/SU175.jpg"> </p>

<div align="center"> 

**`Fig.161` Import Form as template**

</div>

4.	After filling the fields click on "Create" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU176.jpg"> </p>

<div align="center"> 

**`Fig.162` Import Form as template**

</div>

5. 	On successful import, a toasted message will be shown as “Form imported as template successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU177.jpg"> </p>

<div align="center"> 

**`Fig.163` Form imported message**

</div>

6.	Super user can cancel the import process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU178.jpg"> </p>

<div align="center"> 

**`Fig.164` Cancel Import Form as template**

</div>


### Form Submitted Records

1.	Click on Forms tab.

2.	Mouse hover on Forms, options are displayed.

3.	Click on 'Submitted records' icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU179.jpg"> </p>

<div align="center"> 

**`Fig.165` Submitted records of form**

</div>

4.	Opens the submitted records page

 a.	Records will be displayed on daily bases.

 b.	Super user can view all the records submitted by Field user.

 c.	If records are not submitted, then toasted message will be shown as “No records found”.


<p align="center"> <img width="1000" height="450" src="media/SU/SU180.jpg"> </p>

 <div align="center"> 

 **`Fig.166` Submitted records of form**

 </div>

 d.	Super user can get the records by filter option.

 e.	Super user has to click on 'filter' icon and provide from date and to date and click on "get records" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU181.jpg"> </p>

 <div align="center"> 

 **`Fig.167` Get data of submitted records**

 </div>

 f.	If records are available, then records are shown in table format with the following headers:

  •	Record status

  •	Inserted place

  •	Inserted date

  •	Workflow Assigned to

  •	Selection

  •	Name and 

  •	Form fields

<p align="center"> <img width="1000" height="450" src="media/SU/SU182.jpg"> </p>

<div align="center"> 

**`Fig.168` Submitted records of form**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU183.jpg"> </p>

<div align="center"> 

**`Fig.169` Submitted records of form**

</div>

g.	Super user can add columns to headers.

h.	click on add header icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU184.jpg"> </p>

<div align="center"> 

**`Fig.170` Add columns of submitted records**

</div>

i.	opens the columns selection window on right side.

j.	select required columns and click on tick icon

<p align="center"> <img width="1000" height="450" src="media/SU/SU185.jpg"> </p>

<div align="center"> 

**`Fig.171` Add columns of submitted records**

</div>

k.	on success, Columns are seen in table.

<p align="center"> <img width="1000" height="450" src="media/SU/SU186.jpg"> </p>

<div align="center"> 

**`Fig.172` Add columns of submitted records**

</div>

5.	Super user can export submitted records to excel

<p align="center"> <img width="1000" height="450" src="media/SU/SU187.jpg"> </p>

<div align="center"> 

**`Fig.173` Export submitted records to excel**

</div>

6.	If super user would not select records then toasted message will be shown as “Please select the records to generate".

7.	Super user need to select records and then click on export to excel icon.

8.	Opens the column selection page.

<p align="center"> <img width="1000" height="450" src="media/SU/SU188.jpg"> </p>

<div align="center"> 

**`Fig.174` Export submitted records to excel**

</div>

9.	Select the columns and click on tick icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU189.jpg"> </p>

<div align="center"> 

**`Fig.175` Export submitted records to excel**

</div>

10. On success, excel will be generated and shows the toasted message as "Note: You can only export 1000 records and information as Please check the form-name_with_unique_id.excel in downloads tab".

<p align="center"> <img width="1000" height="450" src="media/SU/SU190.jpg"> </p>

<div align="center"> 

**`Fig.176` Export submitted records to excel**

</div>

11.	Super user can export records to mail.

<p align="center"> <img width="1000" height="450" src="media/SU/SU191.jpg"> </p>

<div align="center"> 

**`Fig.177` Export submitted records to mail**

</div>

12.	If super user tries to export without selecting records, then toasted message will be shown as “Please select the records to export.

<p align="center"> <img width="1000" height="450" src="media/SU/SU192.jpg"> </p>

<div align="center"> 

**`Fig.178` Export submitted records to mail**

</div>

13.	On selecting records, an Email attachment pop up shown with the following fields:

 •	To

 •	From

 •	Excel or pdf

If super user selects excel, then receiver receives in excel format.

If super user selects pdf, then receiver receives in pdf format.

<p align="center"> <img width="1000" height="450" src="media/SU/SU193.jpg"> </p>

<div align="center"> 

**`Fig.179` Export submitted records to mail**

</div>

14.	On click on "send" button, the column selection page opens on right side.

<p align="center"> <img width="1000" height="450" src="media/SU/SU194.jpg"> </p>

<div align="center"> 

**`Fig.180` Export submitted records to mail**

</div>

15.	Super user can select fields and click on tick icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU195.jpg"> </p>

<div align="center"> 

**`Fig.181` Export submitted records to mail**

</div>

16.	On success, toasted message will be shown as “mail sent successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU196.jpg"> </p>

<div align="center"> 

**`Fig.182` Export submitted records to mail**

</div>

17.	Super user can export records to pdf by clicking to export to pdf icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU197.jpg"> </p>

<div align="center"> 

**`Fig.183` Export submitted records to pdf**

</div>

18.	Without selecting records, if super user tries to export then toasted message shown as “Please select records to export”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU198.jpg"> </p>

<div align="center"> 

**`Fig.184` Export submitted records to pdf**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU199.jpg"> </p>

<div align="center"> 

**`Fig.185` Export submitted records to pdf**

</div>

19.	Super user need to select records and click on export to pdf icon, then column selection with fields pop up will be shown on right side.

20.	Select required fields and click on tick icon.

21.	On success, toast message will be shown as Please check the form-name_with_unique_id.pdf in downloads tab.

<p align="center"> <img width="1000" height="450" src="media/SU/SU200.jpg"> </p>

<div align="center"> 

**`Fig.186` Export submitted records to pdf**

</div>

22.	Super user can trace inserted location by clicking on 'Inserted location' icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU201.jpg"> </p>

<div align="center"> 

**`Fig.187` Inserted location of submitted records**

</div>

23.	On click inserted location without selecting record, it show the toasted message as please select record to view on map.

<p align="center"> <img width="1000" height="450" src="media/SU/SU202.jpg"> </p>

<div align="center"> 

**`Fig.188` Inserted location of submitted records**

</div>

24.	Select the records and click on 'Inserted location' icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU203.jpg"> </p>

<div align="center"> 

**`Fig.189` Inserted location of submitted records**

</div>

25.	Shows the record inserted location on map.

<p align="center"> <img width="1000" height="450" src="media/SU/SU204.jpg"> </p>

<div align="center"> 

**`Fig.190` Inserted location of submitted records**

</div>

26.	Super user can view record details by click on Record status.

<p align="center"> <img width="1000" height="450" src="media/SU/SU205.jpg"> </p>

<div align="center"> 

**`Fig.191` Preview of submitted records**

</div>

27.	Record status will be maintained based on submission of record.

28.	If the record is submitted for the first time by the field user, then the status will be original record.

29.	Super user can edit, and change submitted date for original record.

<p align="center"> <img width="1000" height="450" src="media/SU/SU206.jpg"> </p>

<div align="center"> 

**`Fig.192` Update submitted record data**

</div>

30.	On click update, displays toasted message as “Record updated successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU207.jpg"> </p>

<div align="center"> 

**`Fig.193` Record updated message**

</div>

31.	If the record is rejected, then the record status will be rejected and rejected records cannot be update by Super user.

32.	If the record is re-submitted, then status will be pending for review and record cannot be updated by super user.

<p align="center"> <img width="1000" height="450" src="media/SU/SU208.jpg"> </p>

<div align="center"> 

**`Fig.194` Preview of submitted records**

</div>

33.	Super user can see all the attachments of the record if any attachments are available.

34.	If attachments are not available, then “No data available” message will be seen.

<p align="center"> <img width="1000" height="450" src="media/SU/SU209.jpg"> </p>

<div align="center"> 

**`Fig.195` Attachments of submitted records**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU210.jpg"> </p>

<div align="center"> 

**`Fig.196` Attachments of submitted records**

</div>

35.	Super user can see the following options for a record on clicking ellipse:

 •	History

 •	Geo tagged images

 •	Sketching

36.	On click History, Super user can see approved and rejected status for every record.

37.	If form is not in workflow management then super user can see “No data available” message

<p align="center"> <img width="1000" height="450" src="media/SU/SU211.jpg"> </p>

<div align="center"> 

**`Fig.197` Workflow history of submitted records**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU212.jpg"> </p>

<div align="center"> 

**`Fig.198` Workflow history of submitted records**

</div>

38.	Super user can see all the geo tagged images, if geo tagged images are there then super user can see all then images and if not, then super user will see the toasted message as “No geo tagged images”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU215.jpg"> </p>

<div align="center"> 

**`Fig.199` Geo tagged images of submitted records**

</div>

<p align="center"> <img width="1000" height="450" src="media/AA/aa186.jpg"> </p>

<div align="center"> 

**`Fig.200` Geo tagged images of submitted records**

</div>


39.	Super user can see all the sketching available for the record and if sketching is not done, then it shows “No sketching is available”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU213.jpg"> </p>

<div align="center"> 

**`Fig.201` Sketchings of submitted records**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU214.jpg"> </p>

<div align="center"> 

**`Fig.202` Sketchings of submitted records**

</div>

### Measuring Tool

1. Click on 'draw tool' icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.203` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.png"> </p>

<div align="center"> 

**`Fig.204` Draw tool**

</div>

2. Click on Draw a 'polyline' icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.205` click on draw a polyline**

</div>

3. Draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.206` Drawn a poly line**

</div>

4. Click on "Finish" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.207` Click on Finish Button**

</div>

5. To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.208` Length of distance**

</div>

6. Click on 'polygon' icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.209` Click on polygon icon**

</div>

7. Draw a polygon then click on "finish" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.210` Drawn polygon on map**

</div>


8. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.211` Area of polygon**

</div>

9. Click "Rectangle" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.212` Click on draw a rectangle**

</div>

10. Draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.213` Drawn rectangle on map**

</div>

11. To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.214` Area of rectangle**

</div>

12. Click on "Marker" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.215` click on marker icon**

</div>

13. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.216` Marker on map**

</div>

14. Click on 'edit layers' icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.217` Click on Edit layers icon **

</div>

15. We can see the existing sketchings(polyline,polygon,Rectangle,Marker) in editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.218` sketchings are in editable mode**

</div>

16. After edit sketchings click on 'save' icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.219` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.220` saved sketchings**

</div>

17. Click on 'delete' icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.221` Click on delete icon**

</div>

18. Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.222` Click on clearAll**

</div>

19. Clears all sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.223` Clear All drawed sketchings on map**

</div>

20. Click on 'Delete' icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.224` Click on delete icon**

</div>

20. Click on skecthings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.225` Click on drawn rectangle**

</div>

21. Delete sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.226` Deleted drawn rectangle**

</div>

### Form information

1.	Mouse hover on form, application displays 'info' icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU217.jpg"> </p>

<div align="center"> 

**`Fig.227` Form info**

</div>

2.	Super user can view the form information by clicking on info icon on the form.

3.	The following information is shown in a table:

 •	Form Name

 •	Created by

 •	Creation date

 •	No of assignments

 •	No of cases collected

 •	Last form modified date

 •	Last downloaded date

 •	Last downloaded by

 •	No of users downloaded


<p align="center"> <img width="1000" height="450" src="media/SU/SU218.jpg"> </p>

<div align="center"> 

**`Fig.228` Form info**

</div>

## Device Management

1.	Once the Super user logs in, displays “Device Management” icon in left side menu.

 •	All the devices belonging to all the account will be visible on clicking on “Device Management” tab.

 •	Any user trying to login form any device for the first time, the device has to be approved by the super user.

 •	When the request for a new device comes, its status is “Pending”. Super user needs to check for the credibility and take necessary action.

<p align="center"> <img width="1000" height="450" src="media/SU/SU219.jpg"> </p>

<div align="center"> 

**`Fig.229` Device management page**

</div>

 •	We get the list of “Pending” devices from the device icon in the header.

 •	Super user can Approve/Suspend/Revoke/Unauthorize a particular device request based on the credibility.

2.	Super user can apply filter bases on status

The following are the status:

 1)	All

 2)	Approved

 3)	Pending

 4)	Rejected

 5)	Suspended

 6)	Unauthorized

<p align="center"> <img width="1000" height="450" src="media/SU/SU220.jpg"> </p>

<div align="center"> 

**`Fig.230` Device management page**

</div>

•	If super user selects “All” option from filter

<p align="center"> <img width="1000" height="450" src="media/SU/SU221.jpg"> </p>

<div align="center"> 

**`Fig.231` Filter device details**

</div>

•	Super user can see all the devices with its status as Pending/Rejected/Approved/Suspended/Unauthorized.

•	If super user selects “Approved”, then super user can see all the approved devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU222.jpg"> </p>

<div align="center"> 

**`Fig.232` Filter device details**

</div>

•	If super user selects “Pending”, then super user can see all the pending devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU223.jpg"> </p>

<div align="center"> 

**`Fig.233` Filter device details**

</div>

•	If super user selects “Rejected”, then super user can see all the rejected devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU224.jpg"> </p>

<div align="center"> 

**`Fig.234` Filter device details**

</div>

•	If super user selects “suspended”, then super user can see all the suspended devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU225.jpg"> </p>

<div align="center"> 

**`Fig.235` Filter device details**

</div>

•	If super user selects “unauthorized”, then super user can see all the unauthorized devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU226.jpg"> </p>

<div align="center"> 

**`Fig.236` Filter device details**

</div>

3.	Super user can approve the device

•	If super user, click on status of device, it shows the list as approve, Reject, Unauthorize.

<p align="center"> <img width="1000" height="450" src="media/SU/SU227.jpg"> </p>

<div align="center"> 

**`Fig.237` Device management page**

</div>

•	If super user clicks on Approve, it shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU228.jpg"> </p>

<div align="center"> 

**`Fig.238` Approve Device**

</div>

•	If super user clicks on “Yes” then the device will get approved and toasted message will be shown as “Device is approved” and send auto-generate approval mail to field user.

<p align="center"> <img width="1000" height="450" src="media/SU/SU229.jpg"> </p>

<div align="center"> 

**`Fig.239` Approve device**

</div>

•	The status will be changed from Pending to Approved.

•	If super user clicks on “No” then the device will not get approved and shows a toasted message as “Device approval action is cancelled”

<p align="center"> <img width="1000" height="450" src="media/SU/SU230.jpg"> </p>

<div align="center"> 

**`Fig.240` Cancel approve device**

</div>

5.	Super user can reject the device details

 •	On click status dropdown, super user can see Approve, Reject, unauthorized.

 •	On selection of Reject, Super user can reject the device details. 

<p align="center"> <img width="1000" height="450" src="media/SU/SU231.jpg"> </p>

<div align="center"> 

**`Fig.241` Reject device**

</div>

•	On click reject, super user can see the rejection alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU232.jpg"> </p>

<div align="center"> 

**`Fig.242` Reject device**

</div>

•	Shows the alert to confirm the device details

•	If super user clicks on “Yes”

<p align="center"> <img width="1000" height="450" src="media/SU/SU233.jpg"> </p>

<div align="center"> 

**`Fig.243` Reject device**

</div>

•	Device Rejected Successfully

•	If device status is rejected then Super user can delete the device details.

•	Click on delete icon, then shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU234.jpg"> </p>

<div align="center"> 

**`Fig.244` Delete device details**

</div>

•	If click on “Yes”, then device details gets deleted, and toasted message shown as “Device deleted successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU235.jpg"> </p>

<div align="center"> 

**`Fig.245` Delete device details**

</div>

•	If click on “No”, then device details gets rejected status and toasted message shown as “Device deletion is cancelled” and all the device details page will be displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/SU236.jpg"> </p>

<div align="center"> 

**`Fig.246` Cancel delete device details**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU237.jpg"> </p>

<div align="center"> 

**`Fig.247` Device management page**

</div>

•	Device Reject process will be cancelled.

5.	Super user can Unauthorized the device

 •	Super user need to click on status dropdown

 •	Super user can unauthorize the device details by selecting unauthorize option


<p align="center"> <img width="1000" height="450" src="media/SU/SU238.jpg"> </p>

<div align="center"> 

**`Fig.248` Unauthorize Device**

</div>

•	On click Unauthorized, Device Unauthorize process shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU239.jpg"> </p>

<div align="center"> 

**`Fig.249` Unauthorize Device**

</div>

•	If super user clicks on “No”, Device Unauthorize process gets cancelled

<p align="center"> <img width="1000" height="450" src="media/SU/SU240.jpg"> </p>

<div align="center"> 

**`Fig.250` Cancel Unauthorize Device**
</div>

•	If super user clicks on “Yes”, Device gets unauthorized, and device can be deleted by clicking on delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU241.jpg"> </p>

<div align="center"> 

**`Fig.251` Device management page**

</div>

•	Super user can delete the unauthorized devices by clicking in delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU242.jpg"> </p>

<div align="center"> 

**`Fig.252` Delete device details**

</div>

•	On click delete icon, Deletion confirmation alert gets displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/SU243.jpg"> </p>

<div align="center"> 

**`Fig.253` Delete device details**

</div>

•	If click on “Yes”, then device gets deleted successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/SU244.jpg"> </p>

<div align="center"> 

**`Fig.254` Delete device details**

</div>

•	If click on “No”, deletion process gets cancelled.

6.	Super user can suspend the device details.

If the device status is approved and Super user wants to Suspend then Super user clicks on Approve and it shows the suspend option as follows

<p align="center"> <img width="1000" height="450" src="media/SU/SU245.jpg"> </p>

<div align="center"> 

**`Fig.255` Device management page**

</div>

• If super user clicks on Suspend.

<p align="center"> <img width="1000" height="450" src="media/SU/SU246.jpg"> </p>

<div align="center"> 

**`Fig.256` Suspend Device**

</div>

• If Super user clicks on “No”, process gets cancelled.

<p align="center"> <img width="1000" height="450" src="media/SU/SU247.jpg"> </p>

<div align="center"> 

**`Fig.257` Cancel suspend Device**

</div>

• If super user clicks on “Yes”, device gets suspended

<p align="center"> <img width="1000" height="450" src="media/SU/SU248.jpg"> </p>

<div align="center"> 

**`Fig.258` Suspend Device**

</div>

• Changes the device status gets changed from Approved to suspended.

<p align="center"> <img width="1000" height="450" src="media/SU/SU249.jpg"> </p>

<div align="center"> 

**`Fig.259` Device management page**

</div>


• Super user can delete the suspended devices by clicking in delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/suspended devices.jpg"> </p>

<div align="center"> 

**`Fig.260` Delete device details**

</div>

•	On click delete icon, Deletion confirmation alert gets displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/delete device.jpg"> </p>

<div align="center"> 

**`Fig.261` Delete device details**

</div>

•	If click on “Yes”, then device gets deleted successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/deleted toast.jpg"> </p>

<div align="center"> 

**`Fig.262` Delete device details**

</div>

•	If click on “No”, deletion process gets cancelled.

7.	Super user can update the status from suspend to revoke back to pending.

<p align="center"> <img width="1000" height="450" src="media/SU/SU250.jpg"> </p>

<div align="center"> 

**`Fig.263` Revoke device status**

</div>

• Click on revoke, shows the conformation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU251.jpg"> </p>

<div align="center"> 

**`Fig.264` Revoke device status**

</div>

• If click on “No”, revoke process gets cancelled.

<p align="center"> <img width="1000" height="450" src="media/SU/SU252.jpg"> </p>

<div align="center"> 

**`Fig.265` Cancel revoke device**

</div>

• If click on “Yes”, device details gets revoked successfully, and toasted message displayed as “your device is pending”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU253.jpg"> </p>

<div align="center"> 

**`Fig.266` Device management page**

</div>

• when a suspended device is revoke, status changes to pending. 

## Device Management Activity

1. Click on activity tab in device management page.

2. Application will open activity page of device management with from date, to date and filter icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/SU/device management.jpg"></p>

<div align="center">

</div>

3. Choose From date and To date, account dropdown and click on filter icon.

4. Device Activity details in between the selected dates gets displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/SU/device management activity filter.jpg"></p>

<div align="center">

</div>

## Downloads 

The Downloads tab provides the feasibility to download the record reports, that is both excel and pdf reports.

It has the other details like place of download, time of download etc.

The files under Downloads tab gets deleted automatically after 24 hours of download.

<p align="center"> <img width="1000" height="450" src="media/SU/SU254.jpg"> </p>

<div align="center"> 

**`Fig.267` Downloads page**
</div>

## Settings

Super user will be able to perform some of the actions from the system. The below is the list:

 •	Profile

 •	Privileges

 •	Application Settings

 •	Accounts Settings

 •  Map configuration

 •	Layer configuration

<p align="center"> <img width="1000" height="450" src="media/SU/SU255.jpg"> </p>

<div align="center"> 

**`Fig.268` Settings page**

</div>

###	Profile

•	Super user will be able to change the user details like ‘First name, Last name, Email, Phone Number, Image’.

•	First, click on the profile and then edit the required details and then click on "save" button. The changes gets updated with new data.

<p align="center"> <img width="1000" height="450" src="media/SU/SU256.jpg"> </p>

<div align="center"> 

**`Fig.269` profile settings**

</div>

•	The application displays a message saying, "Profile Updated Successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU257.jpg"> </p>

<div align="center"> 

**`Fig.270` Update profile**

</div>

### Privileges

•	Click on Privileges to update Admin privileges.

<p align="center"> <img width="1000" height="450" src="media/SU/SU258.jpg"> </p>

<div align="center"> 

**`Fig.271` View privileges**

</div>
•	Select admin by clicking on admins dropdown.

<p align="center"> <img width="1000" height="450" src="media/SU/SU259.jpg"> </p>

<div align="center"> 

**`Fig.272` Update privileges**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU260.jpg"> </p>

<div align="center"> 

**`Fig.273` Update privileges**

</div>

•	On selecting admin and type, the list of menu options with toggle off and on are shown as follows:

 ◦	Downloads

 ◦	Forms

 ◦	Templates

 ◦	Category

 ◦	Projects

 ◦	Tasks

 ◦	Users

 ◦	Administrators

 ◦	Workflow management

 ◦	Device management

 ◦	settings

<p align="center"> <img width="1000" height="450" src="media/SU/SU261.jpg"> </p>

<div align="center"> 

**`Fig.274` Update privileges**

</div>

•	On click update, privileges gets updated and toasted message shown as “Privileges updated successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU262.jpg"> </p>

<div align="center"> 

**`Fig.275` Update privileges**

</div>

### Map

•	Super user can update Maps for specific account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU263.jpg"> </p>

<div align="center"> 

**`Fig.276` View map settings**

</div>

•	On click Maps option, Maps page gets open.

•	The following fields and options are displayed:

 ◦	Account – Dropdown

 ◦	Map with different options as follows

  ▪	Open street maps

  ▪	Map Box

  ▪ Google maps

<p align="center"> <img width="1000" height="450" src="media/SU/SU264.jpg"> </p>

<div align="center"> 

**`Fig.277` View map settings**

</div>

•	Super user must select account, then must select map component.

•	After selection super user must click on update to apply the changes.

<p align="center"> <img width="1000" height="450" src="media/SU/SU265.jpg"> </p>

<div align="center"> 

**`Fig.278` Update map settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU266.jpg"> </p>

<div align="center"> 

**`Fig.279` Update map settings**

</div>

•	On success, Super user can see the toasted messages as "map updated successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU267.jpg"> </p>

<div align="center"> 

**`Fig.280` Update map settings**

</div>
 
#### Application Settings

1.	Super user can update Application settings.

2.	To update Account settings, click on Application setting icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU268.jpg"> </p>

<div align="center"> 

**`Fig.281` Application settings**

</div>

3.	On click application settings, super user can see the application settings page with following options:

 a.	Application security toggle

 b.	A table with following headers

  i.	Parameter

  ii.	Value

  iii.	Description

  iv.	Action

<p align="center"> <img width="1000" height="450" src="media/SU/SU269.jpg"> </p>

<div align="center"> 

**`Fig.282` Account settings**

</div>

•	On click application security toggle on, super user can on the security, and it shows the message as “Settings updated successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU270.jpg"> </p>

<div align="center"> 

**`Fig.283` Application settings**

</div>

•	Super user can update reset or cancel the application settings process

 o	Click on update in actions for Account lock settings

 o	Value will show as editable field

 o	Super user can change the value and click on update

 o	On successful update, toasted message will be shown as “settings updated successfully”

<p align="center"> <img width="1000" height="450" src="media/SU/SU271.jpg"> </p>

<div align="center"> 

**`Fig.284` Account lock settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU272.jpg"> </p>

<div align="center"> 

**`Fig.285` Account lock settings**

</div>

o	To rest with default value, super user has to click on update and then click on Reset.

<p align="center"> <img width="1000" height="450" src="media/SU/SU273.jpg"> </p>

<div align="center"> 

**`Fig.286` Account lock settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU274.jpg"> </p>

<div align="center"> 

**`Fig.287` Account lock settings**

</div>
o	Click on update in actions for Lock interval settings

o	Value will show as editable field

o	Super user can change the value and click on update

o	On successful update, toasted message shown as “settings updated successfully”

<p align="center"> <img width="1000" height="450" src="media/SU/SU275.jpg"> </p>

<div align="center"> 

**`Fig.288` Lock interval settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU276.jpg"> </p>

<div align="center"> 

**`Fig.289` Lock interval settings**

</div>

o	To rest with default value, super user must click on update and then click on "Reset".

<p align="center"> <img width="1000" height="450" src="media/SU/SU277.jpg"> </p>

<div align="center"> 

**`Fig.290` Lock interval settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU278.jpg"> </p>

<div align="center"> 

**`Fig.291` Lock interval settings**

</div>

o	Click on update in actions for files per record

o	Value will show as editable field

o	Super user can change the value and click on "update"

o	On successful update, toasted message shown as “settings updated successfully”

<p align="center"> <img width="1000" height="450" src="media/SU/SU279.jpg"> </p>

<div align="center"> 

**`Fig.292` Application settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU280.jpg"> </p>

<div align="center"> 

**`Fig.293` Application settings**

</div>

o	To rest with default value, super user must click on update and then click on "Reset".

<p align="center"> <img width="1000" height="450" src="media/SU/SU281.jpg"> </p>

<div align="center"> 

**`Fig.294` Application settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU282.jpg"> </p>

<div align="center"> 

**`Fig.295` Application settings**

</div>

o	Click on update in actions for size of file

o	Value will show as editable field

o	Super user can change the value and click on update

o	On successful update, toasted message shown as “settings updated successfully”

<p align="center"> <img width="1000" height="450" src="media/SU/SU283.jpg"> </p>

<div align="center"> 

**`Fig.296` Application settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU284.jpg"> </p>

<div align="center"> 

**`Fig.297` Application settings**

</div>

o	To rest with default value, super user must click on update and then click on Reset.

<p align="center"> <img width="1000" height="450" src="media/SU/SU285.jpg"> </p>

<div align="center"> 

**`Fig.298` Account settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU286.jpg"> </p>

<div align="center"> 

**`Fig.299` Application settings**

</div>


### Account settings

1.	Super user can change the account settings by clicking on Account settings icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU287.jpg"> </p>

<div align="center"> 

**`Fig.300` Account settings**

</div>

2.	On click Account settings icon, Account setting page gets opened with Accounts dropdown and Account lock toggle.

<p align="center"> <img width="1000" height="450" src="media/SU/SU288.jpg"> </p>

<div align="center"> 

**`Fig.301` Account settings**

</div>

3.	From dropdown list, select account and click on update.

<p align="center"> <img width="1000" height="450" src="media/SU/SU289.jpg"> </p>

<div align="center"> 

**`Fig.302` Update account settings**

</div>

4.	On successful update, toasted message shown as setting updated successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/SU290.jpg"> </p>

<div align="center"> 

**`Fig.303` Account settings**

</div>

### layer configuration
 
o Super user can perform layer configuration settings by clicking on layer configuration.

<p align="center"> <img width="1000" height="450" src="media/SU/settings page.jpg"> </p>

<div align="center"> 

**`Fig.304` Settings page**

</div>

1. Create Layer

o Application opens layer configuration page with create layer and Accounts dropdown.

o Click on +/create layer button.

<p align="center"> <img width="1000" height="450" src="media/SU/layer configuration page.jpg"> </p>

<div align="center"> 

**`Fig.305` Layer configuration settings page**

</div>

o Add new layer form page opens with Accounts, external name, internal name, layer url, layer type and create, cancel buttons.

<p align="center"> <img width="1000" height="450" src="media/SU/add layer.jpg"> </p>

<div align="center"> 

**`Fig.306` Add new layer page**

</div>

o Select account from the dropdown list, enter details, select layer type wms/wmts and click on create button.

<p align="center"> <img width="1000" height="450" src="media/SU/create layer.jpg"> </p>

<div align="center"> 

**`Fig.307` Add new layer page**

</div>

o Application displays toast message for layer creation.

<p align="center"> <img width="1000" height="450" src="media/SU/create layer toast.jpg"> </p>

<div align="center"> 

**`Fig.308` Toast for layer creation**

</div>

2. Edit Layer

o In layer configuration page select account from the dropdown list, shows the existing layers of the selected account.

<p align="center"> <img width="1000" height="450" src="media/SU/layers list.jpg"> </p>

<div align="center"> 

**`Fig.309` Layers list**

</div>

o Click on edit layer option, opens edit layer form with layer url, layer type in editable mode and update, cancel button.

o Update the required changes and click on update button.

<p align="center"> <img width="1000" height="450" src="media/SU/add layer.jpg"> </p>

<div align="center"> 

**`Fig.310` Update layer**

</div>

o Application displays toast message for layer update.

<p align="center"> <img width="1000" height="450" src="media/SU/layer update toast.jpg"> </p>

<div align="center"> 

**`Fig.311` Update layer toast**

</div>

3. Preview layer

o Click on preview layer option.

o Opens map showing layers data on map. 

<p align="center"> <img width="1000" height="450" src="media/SU/layer preview.jpg"> </p>

<div align="center"> 

**`Fig.312` Layer preview**

</div>

4.  Search on map 

o Click on search icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.313` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.314` Search on map**

</div>

5. Delete layer

o Click on delete layer icon.

<p align="center"> <img width="1000" height="450" src="media/SU/delete layer option.jpg"> </p>

<div align="center"> 

**`Fig.315` Delete layer**

</div>

o Application displays confirm action message, click on yes.

<p align="center"> <img width="1000" height="450" src="media/SU/delete action.jpg"> </p>

<div align="center"> 

**`Fig.316` Delete action message**

</div>

o Application displays toast message for layer delete.

<p align="center"> <img width="1000" height="450" src="media/SU/delete layer toast.jpg"> </p>

<div align="center"> 

**`Fig.317` Toast for delete layer**

</div>

### Profile information

1.	Super user can view profile information

2.	Super user must click on profile picture on the top right corner and click on profile

<p align="center"> <img width="1000" height="450" src="media/SU/SU291.jpg"> </p>

<div align="center"> 

**`Fig.318` Profile details**

</div>

## Dashboards

The Dashboards provide the statistical information present under the login. Following are the different roles under the application and their respective dashboards.
Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.

### Root

Root has two type of entities:
 •	Usage of System

 •	Account Specific

<p align="center"> <img width="1000" height="450" src="media/SU/SU292.jpg"> </p>

<div align="center"> 

**`Fig.319` Dashboard**

</div>

•	Under “Usage of System”, the following statistics available. The grey boxes with ellipse opens a table with records explaining more about the numeric data. For example, “Consumed Licenses” in the below screenshot.

<p align="center"> <img width="1000" height="450" src="media/SU/SU293.jpg"> </p>

<div align="center"> 

**`Fig.320` Dashboard**

</div>

•	Under “Account Specific” entity we get account specific details. The account can be selected from the dropdown on clicking the ellipse.

<p align="center"> <img width="1000" height="450" src="media/SU/SU294.jpg"> </p>

<div align="center"> 

**`Fig.321` Dashboard**

</div>

•	The grey boxes with ellipse provide data in tabular form. Below are the details available under Account Specific entity. 

<p align="center"> <img width="1000" height="450" src="media/SU/SU295.jpg"> </p>

<div align="center"> 

**`Fig.322` Dashboard**

</div>




