# About FieldOn

<p>FieldOn is a product of Magikminds which make digital data collection and analysis simple & streamlined with built-in integration services that will automate processes and push data to other enterprise systems. Build smart forms that enhance work allocation, supervision, and efficiency. Advanced data fields & GIS support systems make FieldOn accessible to varied industries. From your field workers to top-level managers, keep your workforce connected with seamless data collection & integration.</p>

# Why FieldOn?

GO DIGITAL FOR DYNAMIC DATA COLLECTION For more info visit https://fieldon.com/

<code>Possibilities for FieldOn are limitless</code>

This possibilities for FieldOn are limitless. Below are just a few examples of how customers
are using FieldOn to pull more productivity out of the field.

FEATURES THAT WILL GIVE YOU A COMPETITIVE EDGE:

- Deployable on Mobile & Web.

- Advanced Data Fields : Digital signatures, photo capture, GPS, Barcode/Qr Code Reader and Time stamps.

- Supported GIS features like Asset visualization & update, crew tracking & geofencing, geotagging.

- Built-in calculations, validation, troubleshooting, navigation and hands-free interaction.

- Reports and consolidated data available in XL/CSV format.

- Documentation & Reviewal of completed work.




<!-- [filename](media/example.md ':include :type=code :fragment=demo') -->
<!-- [filename](_media/example.js ':include :type=code :fragment=demo') -->
<!-- ![logo](media/Capture.PNG) -->

# Features

FieldOn comprises of following features:

 - Easily capture emplicate image data with any smart device.Real time accessing and storing of data is a cake walk with photo capture.

<p align="center"> <img width="200" height="200" src="media/camera.png"> </p>

<div align="center"> 

**`Fig` Photo capture**

</div>

 - Supervisors can control the field from multiple locations with user tracking and real time data. Scan QR and barcodes from your device into the forms.

<p align="center"> <img width="200" height="200" src="media/GPS.png"> </p>

<div align="center"> 

**`Fig` GPS & Barcode**

</div>

 - Expidite client approvals, sign contracts and agreements on time. Notify your team once work is completed and send work order copy on-the-fly.

<p align="center"> <img width="200" height="200" src="media/signature.png"> </p>

<div align="center"> 

**`Fig` Digital Signature**

</div>

 - Use time stamps feature to show when work was performed and how long it took and when it was completed. Get all the history you need to manage assets and report time and work accurately.

<p align="center"> <img width="200" height="200" src="media/Time-stamp.png"> </p>

<div align="center"> 

**`Fig` Time stamps**

</div>

 - Customise your forms to suit your business type using our administrative dashboard. Export, evaluate and review collected data and make timely and informed decisions.

<p align="center"> <img width="200" height="200" src="media/dashboard.png"> </p>

<div align="center"> 

**`Fig` Flexible dashboards**

</div>

 - Enable your user to access data across admins and sub-admins. Empower users to create and customise their forms and improve functionality with users-groups for different accounts.

<p align="center"> <img width="200" height="200" src="media/users.png"> </p>

<div align="center"> 

**`Fig` Multiple user facility**

</div>

 - Create forms from scratch according to your need or use standard templates designed for diverse business types. Launch and assign tasks using our forms, in real time.

<p align="center"> <img width="200" height="200" src="media/form.png"> </p>

<div align="center"> 

**`Fig` Design custom forms**

</div>

 - Enhance, data storage, security, authorisation  by integrating your forms with any existing system, like SAP, Oracle, CIS etc. Automate processes and integrate collected information with any system.

<p align="center"> <img width="200" height="200" src="media/lock.png"> </p>

<div align="center"> 

**`Fig` Buit-in service integration**

</div>

<!-- [filename](media/example.md ':include :type=code :fragment=demo') -->

## Administrative Web portal (AP)

An administrative portal provides all necessary configurations for defining and maintaining the application flow. It is primarily supporting functionalities associated with following user roles.

In administrative portal we will provide Four types of administrative users.

 1.	Super User

 2.	Account Administrator

 3.	Group Administrator

 4.	Moderator

