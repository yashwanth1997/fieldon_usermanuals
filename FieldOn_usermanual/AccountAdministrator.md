﻿
# Account Administrator
<!-- {docsify-ignore} -->
Account administrator is created and mapped to specific Account by Super User, the activities which are created in a specific account are seen by the respective Account Administrator and Super Administrator.

<p align="center"> <img width="1000" height="500" src="media/AAA.jpg"> </p>

<div align="center"> 

**`Fig` Account administrator activities**

</div>

<p>Enter valid Account administrator username and password and click on Sign in.</p>

Login as [Account Administrator ](https://demo.fieldon.com/#/login)

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa1.jpg"></p>

<div align="center">

**`Fig.1` Account Administrator login page**

</div>

- After login we can see the page where we have the 

 - <code>Administrators</code>	
 - <code>Users</code>
 - <code>Categories</code>	
 - <code>Devicemanagement</code>
 - <code>Templates</code>
 - <code>Forms</code>
 - <code>Projects</code>
 - <code>Tasks</code>
 - <code>Workflowmanagement</code>
 - <code>Downloads</code>
 - <code>Settings</code>

## Administrators

1. On click of Administrators tab displays the list of Administrators available.
2. We can see all types of Administrators.

 i.	Account Administrator

 ii. Group Administrator

 iii. Moderators

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa2.jpg"></p>

<div align="center">

**`Fig.2` Administrators page**

</div>

3. We can filter the Administrators by using filter in the top right-hand side. By default, it shows all administrators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa3.jpg"></p>

<div align="center">

**`Fig.3` Filter Administrators**

</div>

4. Select Moderator, shows all available moderators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa4.jpg"></p>

<div align="center">

**`Fig.4` Filter moderators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa5.jpg"></p>

<div align="center">

</div>

5. Select Group Administrators, shows all available group administrators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa6.jpg"></p>

<div align="center">

**`Fig.5` Filter Group administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa7.jpg"></p>

<div align="center">

</div>

6.  Search administrators in the search bar which is on top right-hand, shows the available administrators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa8.jpg"></p>

<div align="center">

**`Fig.6` Search administrators**

</div>

### Administrator Create

1.	Account administrator can create the Administrator by clicking on the create button in ellipse on the top right-hand side.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa9.jpg"></p>

<div align="center">

</div>

2.	On click of "create" button, a form  open with fields first name, last name, email, phone, role, select avatar. Fill the details and click on create.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa10.jpg"></p>

<div align="center">

**`Fig.7` Administrator creation form**

</div>

3.	We have Group Administrator and Moderators.

4.	Based on the check box selection, Administrators are created.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa11.jpg"></p>

<div align="center">

**`Fig.8` Group administrator creation**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa12.jpg"></p>

<div align="center">

</div>

5.	If role is not selected, then Moderator is created.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa13.jpg"></p>

<div align="center">

**`Fig.9` Moderator creation**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa14.jpg"></p>

<div align="center">

</div>

<code>Note:</code> No duplicate Emails allowed for same type of Administrator.

### Edit/Update Administrator

1.	To edit Administrator, select the Administrator and hover on the card we can see Edit icon.

2.	Select the Edit icon, edit administrator form opened with existing details, update the details, and click on "Update" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa15.jpg"></p>

<div align="center">

**`Fig.10` Edit administrators**

</div>

3.	After successful update, toast message is shown as "Administartor updated successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa16.jpg"></p>

<div align="center">

**`Fig.11` Edit form of administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa17.jpg"></p>

<div align="center">

</div>

### Delete Administrator

1.	To delete Administrator, select the Administrator and click on delete button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa18.jpg"></p>

<div align="center">

</div>

2.	Once click on Delete button, application shows the conformation pop up, then select the yes option.

3.	After click on Yes button, application shows the toast message as "Administartor deleted successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa19.jpg"></p>

<div align="center">

**`Fig.12` Delete administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa20.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
Can delete Administrators, only if it is not associated to accounts

### Administrator Password Reset

1. Account administrator can reset the password of Administrator to default password.

2.	Select the Administrator and click on password reset button.

3.	Default password is "mm@1234".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa21.jpg"></p>

<div align="center">

**`Fig.13` Reset password-administrators**

</div>

4.	Once click on Reset button, application shows the conformation pop up, then select the yes option.

5.	After click on Yes button, application shows the toast message as "password reset successful".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa22.jpg"></p>

<div align="center">

**`Fig.14` Reset password-administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa23.jpg"></p>

<div align="center">

</div>

## Users

1.	On click of Users tab, displays the list of Users available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa24.jpg"></p>

<div align="center">

**`Fig.15` Users list**

</div>

2.	Account administrator can create the users individually or can import users from excel sheet by clicking Import users option.

3.	For creating the user, click on actions/ellipse.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa25.jpg"></p>

<div align="center">

</div>

### Individual User Creation

- Click on Create User option.
- Fill the required details and click on "Create" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa26.jpg"></p>

<div align="center">

**`Fig.16` Create Users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa27.jpg"></p>

<div align="center">

**`Fig.16` User creation form**

</div>

- Once click on Create button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa28.jpg"></p>

<div align="center">

</div>

<code>Note:</code> 
 Duplicate emails are not allowed.

### User Edit/Update

Account administrator can edit/ update the user profiles.

  1. To edit the user profile, first select the user profile and click on Edit button.

  2. Make the changes and click on Update button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa29.jpg"></p>

<div align="center">

**`Fig.18` Edit users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa30.jpg"></p>


<div align="center">

**`Fig.19` Edit users form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa31.jpg"></p>

<div align="center">

</div>

### User Delete

1.	To delete User, select the User and click on "delete" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa32.jpg"></p>

<div align="center">

**`Fig.20` Delete user**

</div>

2.	Once click on Delete button, application shows the conformation pop up, then select the "yes" option.

3.	After click on Yes button, application shows the toast message as "user deleted successfully"

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa33.jpg"></p>

<div align="center">

**`Fig.21` Delete users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa34.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
 Can delete users, only if user is not assigned to tasks.

### User Password Reset

1.	Account administrator can reset the password of User to default password.

2.	Select the User and click on "password reset" icon.

3.	Default password  be mm@1234.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa35.jpg"></p>

<div align="center">

**`Fig.22` Reset password**

</div>

4.	Once click on Reset button, application shows the conformation pop up, then select the "yes" button.

5.	After click on Yes button, application shows toast message "password reset successful". 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa36.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa37.jpg"></p>

<div align="center">

</div>

### Import Users

Import Users function defines, Account administrator can create bulk user profiles within a single moment by uploading the excel sheet. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa38.jpg"></p>

<div align="center">

**`Fig.23` Import Users**

</div>

1.	To import Users from excel sheet, first download User creation template from Import Users page and fill the user details (valid email ids). 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa39.jpg"></p>

<div align="center">

**`Fig.24` Import Users**

</div>

2.	Save the file with extension .xlsx (Only XLSX files are supported)

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa40.jpg"></p>

<div align="center">

</div>

3.	Select the excel in choose file and click on show table to check the details and edit the details.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa41.jpg"></p>

<div align="center">

**`Fig.25` Import Users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa42.jpg"></p>

<div align="center">

</div>

4.	We can edit and delete the data by selecting Edit and Delete buttons.

5.	Click on the Upload button to upload the users.

6.	After successful upload, displays a toast messsage.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa43.jpg"></p>

<div align="center">

</div>

## Users activity
1. Click on activity tab in users page.

2. Application will open activity page of users with from date, to date and filter icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/user activity page.jpg"></p>

<div align="center">

</div>

3. Choose From date and To date and click on filter icon.

4. Users Activity details in between the selected dates gets displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/user activity.jpg"></p>

<div align="center">

</div>


## Categories

1.	When clicked on Categories tab, displays the list of Categories available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa44.jpg"></p>

<div align="center">

**`Fig.26` List of categories**

</div>

2.	Search categories, shows available categories with searched name on search bar and click on enter.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa45.jpg"></p>

<div align="center">

**`Fig.27` Search categories**

</div>

### Category Creation

1.	 On the right-hand top corner, we can see ellipse. 

2.	On Click of "create button" in ellipse, can create new Category.

3.	A form  opened with name and description.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa46.jpg"></p>

<div align="center">

**`Fig.28` Create category**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa47.jpg"></p>

<div align="center">

**`Fig.29` Create category**

</div>

4.	Fill the details and click the "create" button.

5.	After successful creation, displays a toast message.  

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa48.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
Duplicate names not allowed.

### Category Edit/Update

1.	To edit Category hover on the category, buttons are displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa49.jpg"></p>

<div align="center">

**`Fig.30` Edit categories**

</div>

2.	Click on "Edit" icon, view the existing data, and make the changes In description and update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa50.jpg"></p>

<div align="center">

**`Fig.31` Edit category**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa51.jpg"></p>

<div align="center">

</div>

### Category Delete

1.	To delete Category hover on the category, icons are displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa52.jpg"></p>

<div align="center">

**`Fig.32` Delete category**

</div>

2.	Once click on Delete button, application shows the conformation pop up, then select the "yes" button.

3.	After click on Yes button, application displays toast message as "category deleted successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa53.jpg"></p>

<div align="center">

**`Fig.33` Delete category**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa54.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
 Can delete category, only if it is not assigned to forms/tasks.

## Device Management

1.	Once the Account administrator logs in, “Device Management” icon is visible in left side menu.

 •	All the devices belonging to all the account  visible on clicking on “Device Management” tab.

 •	Any user trying to login form any device for the first time, needs his device to be approved by the Account administrator.

 •	When the request for a new device comes, its status is “Pending”. Account administrator needs to check for the credibility and take necessary action.

<p align="center"> <img width="1000" height="450" src="media/SU/SU219.jpg"> </p>

<div align="center"> 

**`Fig.34` Device management page**

</div>

 •	We get the list of “Pending” devices from the device icon in the header.

 •	Account administrator can Approve/Suspend/Revoke/Unauthorize a particular device request based on the credibility.

2.	Account administrator can apply filter bases on status

The following are the status:

 1)	All

 2)	Approved

 3)	Pending

 4)	Rejected

 5)	Suspended

 6)	Unauthorized

<p align="center"> <img width="1000" height="450" src="media/SU/SU220.jpg"> </p>

<div align="center"> 

**`Fig.35` Device management page**

</div>

•	If Account administrator selects “All” option from filter

<p align="center"> <img width="1000" height="450" src="media/SU/SU221.jpg"> </p>

<div align="center"> 

**`Fig.36` Filter device details**

</div>

•	Account administrator can see all the devices with its status as Pending/Rejected/Approved/Suspended/Unauthorized.

•	If Account administrator selects “Approved”, then Account administrator can see all the approved devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU222.jpg"> </p>

<div align="center"> 

**`Fig.37` Filter device details**

</div>

•	If Account administrator selects “Pending”, then Account administrator can see all the pending devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU223.jpg"> </p>

<div align="center"> 

**`Fig.38` Filter device details**

</div>

•	If Account administrator selects “Rejected”, then Account administrator can see all the rejected devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU224.jpg"> </p>

<div align="center"> 

**`Fig.39` Filter device details**

</div>

•	If Account administrator selects “suspended”, then Account administrator can see all the suspended devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU225.jpg"> </p>

<div align="center"> 

**`Fig.40` Filter device details**

</div>

•	If Account administrator selects “unauthorized”, then Account administrator can see all the unauthorized devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU226.jpg"> </p>

<div align="center"> 

**`Fig.41` Filter device details**

</div>

3.	Account administrator can approve the device

•	If Account administrator, click on status of device, it shows the list as approve, Reject, Unauthorize.

<p align="center"> <img width="1000" height="450" src="media/SU/SU227.jpg"> </p>

<div align="center"> 

**`Fig.42` Device management page**

</div>

•	If Account administrator clicks on Approve, it shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU228.jpg"> </p>

<div align="center"> 

**`Fig.43` Approve Device**

</div>

•	If Account administrator clicks on “Yes” then the device  get approved and toasted message  shown as “Device is approved” and send auto-generate approval mail to field user.

<p align="center"> <img width="1000" height="450" src="media/SU/SU229.jpg"> </p>

<div align="center"> 

**`Fig.44` Approve device**

</div>

•	The status  changed from Pending to Approved.

•	If Account administrator clicks on “No” then the device  not get approved and shows a toasted message as “Device approval action is cancelled”

<p align="center"> <img width="1000" height="450" src="media/SU/SU230.jpg"> </p>

<div align="center"> 

**`Fig.45` Cancel approve device**

</div>

5.	Account administrator can reject the device details

 •	On click status dropdown, Account administrator can see Approve, Reject, unauthorized

 •	On selection of Reject, Account administrator can reject the device details 

<p align="center"> <img width="1000" height="450" src="media/SU/SU231.jpg"> </p>

<div align="center"> 

**`Fig.46` Reject device**

</div>

•	On click reject, Account administrator can see the rejection alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU232.jpg"> </p>

<div align="center"> 

**`Fig.47` Reject device**

</div>

•	Shows the alert to confirm the device details

•	If Account administrator clicks on “Yes”

<p align="center"> <img width="1000" height="450" src="media/SU/SU233.jpg"> </p>

<div align="center"> 

**`Fig.48` Reject device**

</div>

•	Device Rejected Successfully

•	If device status is rejected then Account administrator can delete the device details.

•	Click on delete icon, then shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU234.jpg"> </p>

<div align="center"> 

**`Fig.49` Delete device details**

</div>

•	If click on “Yes”, then device details  be deleted, and toasted message shown as “Device deleted successfully”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU235.jpg"> </p>

<div align="center"> 

**`Fig.50` Delete device details**

</div>

•	If click on “No”, then device details  be in rejected status and toasted message  be shown as “Device deletion is cancelled” and all the device details page  be displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/SU236.jpg"> </p>

<div align="center"> 

**`Fig.51` Cancel delete device details**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU237.jpg"> </p>

<div align="center"> 

**`Fig.52` Device management page**

</div>

•	Device Reject process gets cancelled.

5.	Account administrator can Unauthorized the device

 •	Account administrator need to click on status dropdown

 •	Account administrator can unauthorize the device details by selecting unauthorize option


<p align="center"> <img width="1000" height="450" src="media/SU/SU238.jpg"> </p>

<div align="center"> 

**`Fig.53` Unauthorize Device**

</div>

•	On click Unauthorized, Device Unauthorize process shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU239.jpg"> </p>

<div align="center"> 

**`Fig.54` Unauthorize Device**

</div>

•	If Account administrator clicks on “No”, Device Unauthorize process  cancelled

<p align="center"> <img width="1000" height="450" src="media/SU/SU240.jpg"> </p>

<div align="center"> 

**`Fig.55` Cancel Unauthorize Device**

</div>

•	If Account administrator clicks on “Yes”, Device  be unauthorized, and device can be deleted by clicking on delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU241.jpg"> </p>

<div align="center"> 

**`Fig.56` Device management page**

</div>

•	Account administrator can delete the unauthorized devices by clicking in delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU242.jpg"> </p>

<div align="center"> 

**`Fig.57` Delete device details**

</div>

•	On click delete icon, Deletion confirmation alert displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/SU243.jpg"> </p>

<div align="center"> 

**`Fig.58` Delete device details**

</div>

•	If click on “Yes”, then device  deleted successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/SU244.jpg"> </p>

<div align="center"> 

**`Fig.59` Delete device details**

</div>

•	If click on “No”, deletion process be cancelled.

6.	Account administrator can suspend the device details.
If the device status is approved and Administrator wants to Suspend then Account administrator clicks on Approve and it shows the suspend option as follows

<p align="center"> <img width="1000" height="450" src="media/SU/SU245.jpg"> </p>

<div align="center"> 

**`Fig.60` Device management page**

</div>

If Account administrator clicks on Suspend.

<p align="center"> <img width="1000" height="450" src="media/SU/SU246.jpg"> </p>

<div align="center"> 

**`Fig.61` Suspend Device**

</div>

If Account administrator clicks on “No”, process  cancelled.

<p align="center"> <img width="1000" height="450" src="media/SU/SU247.jpg"> </p>

<div align="center"> 

**`Fig.62` Cancel suspend Device**

</div>

If Account administrator clicks on “Yes”, device  be suspended

<p align="center"> <img width="1000" height="450" src="media/SU/SU248.jpg"> </p>

<div align="center"> 

**`Fig.63` Suspend Device**

</div>

Changes the device status  be changed from Approved to suspended.

<p align="center"> <img width="1000" height="450" src="media/SU/SU249.jpg"> </p>

<div align="center"> 

**`Fig.64` Device management page**

</div>


Account administrator can delete the suspended devices by clicking in delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/suspended devices.jpg"> </p>

<div align="center"> 

**`Fig.65` Delete device details**

</div>

•	On click delete icon, Deletion confirmation alert  be displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/delete device.jpg"> </p>

<div align="center"> 

**`Fig.66` Delete device details**

</div>

•	If click on “Yes”, then device  be deleted successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/deleted toast.jpg"> </p>

<div align="center"> 

**`Fig.67` Delete device details**

</div>

•	If click on “No”, deletion process  be cancelled.

7.	Account administrator can update the status from suspend to revoke back to pending.

<p align="center"> <img width="1000" height="450" src="media/SU/SU250.jpg"> </p>

<div align="center"> 

**`Fig.68` Revoke device status**

</div>

Click on revoke, shows the conformation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU251.jpg"> </p>

<div align="center"> 

**`Fig.69` Revoke device status**

</div>

If click on “No”, revoke process is cancelled.

<p align="center"> <img width="1000" height="450" src="media/SU/SU252.jpg"> </p>

<div align="center"> 

**`Fig.70` Cancel revoke device**

</div>

If click on “Yes”, device details is revoked successfully, and toasted message  shown as “your device is pending”.

<p align="center"> <img width="1000" height="450" src="media/SU/SU253.jpg"> </p>

<div align="center"> 

**`Fig.71` Device management page**

</div>

when a suspended device is revoke, status changes to pending. 

## Device Management Activity

1. Click on activity tab in device management page.

2. Application will open activity page of device management with from date, to date and filter icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/device activity.jpg"></p>

<div align="center">

</div>

3. Choose From date and To date and click on filter icon.

4. Device Activity details in between the selected dates gets displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/device activity filter.jpg"></p>

<div align="center">

</div>

## Templates

1.	When clicked on Templates tab, we can see the list of Templates available.

2.	Account administrator can search templates, shows the available templates with searched name.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa66.jpg"></p>

<div align="center">

**`Fig.72` Search Template**

</div> 

3.	Filter templates, click on filter icon, select the type (All/public/private) shows the templates based on the selection, by default shows all templates.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa67.jpg"></p>

<div align="center">

**`Fig.73` Filter Template**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa68.jpg"></p>

<div align="center">

**`Fig.74` Filter Templates by public type**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa69.jpg"></p>

<div align="center">

</div> 

4.	Group by category, click on group by category icon, shows categories and templates under each category.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa70.jpg"></p>

<div align="center">

**`Fig.75` Group by category of Templates**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa71.jpg"></p>

<div align="center">

**`Fig.76` View Templates under each category**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa72.jpg"></p>

<div align="center">

</div> 

### Preview Template

5.	Click on preview icon, opens preview template page

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa73.jpg"></p>

<div align="center">

**`Fig.77` Preview of template**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa74.jpg"></p>

<div align="center">

**`Fig.78` Preview of template**

</div> 

### Import Template as Form

6.	 To import a template as form, go to the template and hover on the template we can see Import button. 

7.	Click on it to import the Template.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa75.jpg"></p>

<div align="center">

**`Fig.79` Click on import**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa76.jpg"></p>

<div align="center">

**`Fig.80` Import template as form**

</div> 

8.	We can see the existing data of the template where we need to change name, select form type (public/private), When we select form type as private, we need to select users),select the category in that Account and click on Go button. Shows imported successfully message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa77.jpg"></p>

<div align="center">

</div> 

9.	Template  be imported into Forms tabs. Shows forms tabs after imported successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa78.jpg"></p>

<div align="center">

</div> 

## Forms

1.	When clicked on Forms tab, we can see the list of Forms available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa79.jpg"></p>

<div align="center">

**`Fig.81` List of form**

</div> 

2.	Search forms, shows the forms with the searched input if any. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa80.jpg"></p>

<div align="center">

**`Fig.82` Search forms**

</div> 

3.	Filter forms, click on filter icon, select type All/public/private, shows forms based on the selection.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa81.jpg"></p>

<div align="center">

**`Fig.83` Filter forms**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa82.jpg"></p>

<div align="center">

**`Fig.84` Private forms**

</div>

4.	GroupBy category, click on group by category shows categories and forms under each category.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa83.jpg"></p>

<div align="center">

**`Fig.85` Group by category of forms**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa84.jpg"></p>

<div align="center">

**`Fig.86` Group by category of forms**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa85.jpg"></p>

<div align="center">

**`Fig.87` Shows forms under a category**

</div>

5.	Account Administrator can create Forms. 

### Form Creation

1.	 On the right-hand top corner, we can see ellipse. 

2.	Click on ellipse, click on "create" button

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa86.jpg"></p>

<div align="center">

**`Fig.88` Create forms**

</div>

3.	Create form opened with name, category, Form Type, Description and Design from Excel

 a.	<code>Private:</code> If private selected, then the form  visible to only selected Users

 b.	<code>Public:</code> If public selected, then the form visible to all Users of that Account.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa87.jpg"></p>

<div align="center">

**`Fig.89` Create form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa88.jpg"></p>

<div align="center">

**`Fig.90` Create form**

</div>

4.	After clicking of "Go" button. It  open ‘Form Builder’ where we can select the widgets and create Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa89.jpg"></p>

<div align="center">

**`Fig.91` Form widgets**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa90.jpg"></p>

<div align="center">

**`Fig.92` Media widgets and Addon widgets**

</div>

5.	Drag and drop the widgets to the pallate to create Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa91.jpg"></p>

<div align="center">

</div>

6.	After drag and drop of widget, we need to fill the properties of the widget and need to save the properties.

7.	After saving of the widget, save the Form by clicking the "save" button in the top right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa92.jpg"></p>

<div align="center">

</div>

8.	After clicking of the save button we  get a popup with a drop down where we have to select the Display fields and save the Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa93.jpg"></p>

<div align="center">

**`Fig.93` Create form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa94.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
Duplicate names not allowed.

### Form creation from excel

1.	Create form widgets from excel by uploading excel in Form creation page.

2.	Download ‘creation Template’ from the creation page 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa95.jpg"></p>

<div align="center">

**`Fig.94` Form creation using excel**

</div>

3.	After filling the Excel, we need to upload the excel by clicking Choose File button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa96.jpg"></p>

<div align="center">

</div>

4.	After uploading click on "Go" button, we can see the Form Builder with widgets.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa97.jpg"></p>

<div align="center">

</div>

5.	Update the details and save the form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa98.jpg"></p>

<div align="center">

</div>

### Form Preview

1.	To Preview the Form, go to the form and hover on the form, we can see preview button, Click on preview button a popup  be open with all the widgets in the form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa99.jpg"></p>

<div align="center">

**`Fig.95` Form preview**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa100.jpg"></p>

<div align="center">

**`Fig.96` Form preview**

</div>

### Form Edit/Update

1.	 To edit a Form hover on the Form, we can see Edit button.

2.	On Click of edit button on the form we can see the existing details.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa101.jpg"></p>

<div align="center">

**`Fig.97` Edit form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa102.jpg"></p>

<div align="center">

**`Fig.98` Edit form**

</div>

3.	After clicking of 'Go' button. It open Form Builder with the existing widgets and can add new widgets to the Form.

4.	We need to drag and drop the widgets to the pallate to add new widgets to Form.

5.	After drag and drop of widget, we need to fill the properties of the widget and need to save the properties.

6.	After saving of the widget we need to save the Form by clicking the "save" button in the top right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa103.jpg"></p>

<div align="center">

</div>

7.	After clicking of the save button a popup with a drop down opens, select the Display fields and click on update, updates the Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa104.jpg"></p>

<div align="center">

**`Fig.99` Form Update**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa105.jpg"></p>

<div align="center">

</div>

###  Form Derived Conditions

1.	To add derived conditions to a form, after clicking on edit instead of Save click on  "Save" and "Continue".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa106.jpg"></p>

<div align="center">

</div>

2.	After clicking on, we can see a side bar opened with Save, Add and Back buttons.

3.	Once click on Add we can see the dropdown to select and set a condition.

4.	Click on Set to set the condition and Apply.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa107.jpg"></p>

<div align="center">

**`Fig.100` Derived conditions of form**

</div>

5.	After that we can see the conditions in a list where we can delete and save the complete list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa108.jpg"></p>

<div align="center">

</div>

6.	Click on Save to save the form.

### Form Export to Excel

1.	To export form to excel hover on the form, click on export to excel icon, downloads the form in excel.

2.	We can Export the Form to Excel to get all the widgets of the form available in the form in excel.

3.	We can use fill this excel and upload in Work Assignments to user.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa109.jpg"></p>

<div align="center">

**`Fig.101` Export to excel-Form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa110.jpg"></p>

<div align="center">

**`Fig.102` Excel file of form**

</div>

### Submitted records of form

1.	Click on submitted records icon which in on hover on the form. Shows records within 24 hrs.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa111.jpg"></p>

<div align="center">

**`Fig.103` Submitted records of form**

</div>

2.	Click on "filter" icon, opens a filter records popup.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa112.jpg"></p>

<div align="center">

**`Fig.104` Filter records**

</div>

3.	Select from date, to date and select users, click on get data.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa113.jpg"></p>

<div align="center">

**`Fig.105` Filter submitted records of form**

</div>

4.	Shows submitted records table which are in between selected dates.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa114.jpg"></p>

<div align="center">

</div>

5.	Export to Excel, select records and click on export to excel, opens columns selections page in the right side. Select the columns and click on ✅. Shows confirmation message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa115.jpg"></p>

<div align="center">

**`Fig.106` Export to excel-Submitted records of form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa116.jpg"></p>

<div align="center">

**`Fig.107` Columns selection page**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa117.jpg"></p>

<div align="center">

</div>

6.	Export to mail, select records and click on "export to mail button", opens a popup with name email attachment with fields To, CC, select pdf or excel and click on send. Shows confirmation message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa118.jpg"></p>

<div align="center">

**`Fig.108` Export to Mail**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa119.jpg"></p>

<div align="center">

**`Fig.109` Email attachment form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa120.jpg"></p>

<div align="center">

</div>

7.	Export to pdf, select records and click on export to pdf button, opens columns selection page with list of columns. Select columns and click on✅. Shows confirmation message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa121.jpg"></p>

<div align="center">

**`Fig.110` Export to excel-Submitted records form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa122.jpg"></p>

<div align="center">

</div>

8.	Inserted locations, select record and click on inserted location button, map opens and shows the inserted location of the selected record.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa123.jpg"></p>

<div align="center">

**`Fig.111` Inserted locations-Submitted records form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa124.jpg"></p>

<div align="center">

</div>

- Click on disable map view icon, to disable the map.

9.	Add columns, click on add columns buttons, open columns selection page on the right side with list of columns. Select required columns and click on ✅. Selected columns are shown in the records table. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa125.jpg"></p>

<div align="center">

**`Fig.112` Add columns-Submitted records form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa126.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa127.jpg"></p>

<div align="center">

**`Fig.113` Add columns**

</div>

<code>Note:</code>
 - Can export 1000 records to Excel at a time, for more than 1000 records shows alert message.
 - Can export 10 records to mail/pdf at a time, for more than 10 records shows alert message.


### Share Link - form

1.	To share a form link, we need to select the Form and hover on the form, we can see the ‘Share Link’ button. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa128.jpg"></p>

<div align="center">

**`Fig.114` Share link of form**

</div>

2.	After clicking share link button, we can see a popup with validity, we need to select the expiry date and click on Generate button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa129.jpg"></p>

<div align="center">

</div>

3.	After clicking of generate we can see the Generated ‘Link’ and expiry date. we can copy the link by clicking on copy button

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa130.jpg"></p>

<div align="center">

**`Fig.115` Copy link of form**

</div>

4.	Paste the copied link in the new tab, opens the form

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa131.jpg"></p>

<div align="center">

</div>

### Info - Form

1.	To see information of a Form hover on the Form, we can see info button. Click on the info button.

2.	Opens a popup with information of the form in a table.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa132.jpg"></p>

<div align="center">

**`Fig.116` Info-form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa133.jpg"></p>

<div align="center">

**`Fig.117` Info-form**

</div>

### Form Version History

1.	We can view list of changes done on the form after creation as Version History.

2.	To view the version history of a Form, hover on the form and click Version History button

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa134.jpg"></p>

<div align="center">

**`Fig.118` Version history-form**

</div>

3.	After clicking on that we can view the list of versions of that form.

4.	Click on the ‘Info’ icon to view the changes done on that form in that version.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa135.jpg"></p>

<div align="center">

</div>

5.	A popup open with Added, Updated and Deleted categories.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa136.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa137.jpg"></p>

<div align="center">

**`Fig.119` Version history details**

</div>

### Business rules

1. On Clicking on Forms on the left-hand side menu bar, we can see list of forms available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa307.jpg"></p>

<div align="center">

**`Fig.120` List of forms**

</div>

2. To business rules, select the form and hover on it we can see business rules option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa308.jpg"></p>

<div align="center">

**`Fig.121`Set rules forms**

</div>

3. Select the business rules option, red flag rules  be opened with already created rule if any or show No rules defined message and Add new rule option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa309.jpg"></p>

<div align="center">

**`Fig.122` Add new rule**

</div>

4. Clik on Add new rule option, blank row  be added with 'Filed', 'Rule' and dropdowns, 'Value' field and save rule, delete rule option in Action.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa310.jpg"></p>

<div align="center">

**`Fig.123`Add new rule**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa311.jpg"></p>

<div align="center">

**`Fig.124` Add new rule**

</div>

#### Set rule for Text Box

1. On selection Field of type Text Box from field dropdown, in rule dropdown Equalto, Notequal to, Blank options are visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa312.jpg"></p>

<div align="center">

**`Fig.125` Set rules for Text box field**

</div>

2. On selection of rule as Equaltp/NotEqual to, Account administrator  be  enter text in valid field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa313.jpg"></p>

<div align="center">

**`Fig.126` Set rules for text box field**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa314.jpg"></p>

<div align="center">

**`Fig.127` Set rules for Text box field**

</div>

3. On selection Rule as Blank, Value field is disabled

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa315.jpg"</p>

<div align="center">

**`Fig.128` Set rules for Text box field**

</div>

4. After choosing field, Rule and Value click on save rule option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa316.jpg"></p>

<div align="center">

**`Fig.129` Set rules for Text box field**

</div>

5. Application shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa317.jpg"></p>

<div align="center">

**`Fig.130` Saved rules for Text box field**

</div>

6. In redflag rules page, Rule  be shown in the list.

#### Set rule for Text Area field

1.	On Selecting Field of type Text Area from Field dropdown, in Rule Dropdown Equal to, Not Equal to, Blank options are visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa318.jpg"></p>

<div align="center">

**`Fig.131` Set rule for Text area field**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa319.jpg"></p>

<div align="center">

**`Fig.132`   Set rule for Text area field**                         

</div>

2.	On selecting Rule as Equal to/ Not Equal to, Account administrator  enter text in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa320.jpg"></p>

<div align="center">

**`Fig.133`  Save rule for area field**

</div>

3.	On selecting Rule as Blank, value field is disabled.

4.	After choosing field, rule and value click on save rule option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa321.jpg"></p>

<div align="center">

**`Fig.134` Save rule for area field**

</div>

5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa322.jpg"></p>

<div align="center">

**`Fig.135` Saved rule for area field**

</div>

6.In red flag rules page, Rule  be shown in the list.

#### Set rule for Number field

1.	On Selecting Field of type Number from Field dropdown, in Rule dropdown less than, greater than, Equal to, Blank options are visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa323.jpg"></p>

<div align="center">

**`Fig.136` Set rule for Number field**

</div>

2.	On selecting Rule as Greater than/less than/ Equal to, enter text in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa324.jpg"></p>

<div align="center">

**`Fig.137` Set rule for Number field**

</div>

3.	On selecting Rule as Blank, value field is disabled.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa325.jpg"></p>

<div align="center">

**`Fig.138` Save rule for number field**

</div>

4.	After choosing field, rule and value click on save rule option.

5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa326.jpg"></p>

<div align="center">

**`Fig.139` Saved rule for number field**

</div>

6.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Dropdown field

1.	On Selecting Field of type Dropdown from Field dropdown, in Rule dropdown contains, not contains, Blank options is visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa327.jpg"></p>

<div align="center">

**`Fig.140` Set rule for Dropdown field**

</div>

2.	On selecting Rule as contains/not contains, Account administrator view and select options of the selected dropdown in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa328.jpg"></p>

<div align="center">

**`Fig.141` Set rule for Dropdown field**

</div>

3.	On selecting Rule as Blank, value field is disabled.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa329.jpg"></p>

<div align="center">

**`Fig.142` Save rule for Dropdown field**

</div>

4.	After choosing field, rule and value click on save rule option.

5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa330.jpg"></p>

<div align="center">

**`Fig.143` Saved rule for Dropdown field**

</div>

6.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Radio field

1.	On Selecting Field of type Radio from Field dropdown, in Rule dropdown contains, not contains, Blank options is visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa331.jpg"></p>

<div align="center">

**`Fig.144` Set rule for Radio field**

</div>

2.	On selecting Rule as contains/not contains, Account administrator can view and select options of the selected radio field in value field.

3.	On selecting Rule as Blank, value field is disabled.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa332.jpg"></p>
<div align="center">

**`Fig.145` Set rule for Radio field**

</div>

4.	After choosing field, rule and value click on save rule option.

5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa333.jpg"></p>

<div align="center">

**`Fig.146` Saved rule for Radio field**

</div>

6.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Check box field

1.	On Selecting Field of type checkbox from Field dropdown, in Rule dropdown contains, Not contains, Blank options is visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa334.jpg"></p>

<div align="center">

**`Fig.147` Set rule for Check box field**

</div>

2.	On selecting Rule as contains/not contains, Account administrator can view and select options of the selected radio in value field.

3.	On selecting Rule as Blank, value field is disabled. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa335.jpg"></p>

<div align="center">

**`Fig.148` Set rule for Check box field**

</div>

4.	After choosing field, rule and value click on save rule option. 

5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa336.jpg"></p>

<div align="center">

**`Fig.149` Saved rule for check box field**

</div>

6.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Calendar field

1.	On Selecting Field of type calendar from Field dropdown, in Rule dropdown greater than, less than options  be visible.

2.	On selecting Rule as greater than/less than, Account administrator  can view and select date in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa337.jpg"></p>

<div align="center">

**`Fig.150` Set rule for calendar field**

</div>

3.	After choosing field, rule and value click on save rule option. 

4.	Application shows toast message as “Rule saved successfully”.

5.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Calculator field

1.	On Selecting Field of type checkbox from Field dropdown, in Rule dropdown Greater than, less than, equal to, Blank options visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa338.jpg"></p>

<div align="center">

**`Fig.151` Set rule for calculator field**

</div>

2.	On selecting Rule as greater than/less than/equal to, Account administrator can enter the number in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa339.jpg"></p>

<div align="center">

**`Fig.152` Save rule for calculator field**

</div>

3.	On selecting Rule as Blank, value field  be disabled. After choosing field, rule and value, click on save rule option.

4.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa340.jpg"></p>

<div align="center">

**`Fig.153` Saved rule for calculator field**

</div>

5.	In red flag rules page, Rule us shown in the list.

#### Edit rule

1.	All the rules in editable mode, make changes by selecting the field, rule and value and click on save rule icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa341.jpg"></p>

<div align="center">

**`Fig.154` Edit rule**

</div>

2.	New changes are saved successfully and shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa342.jpg"</p>

<div align="center">

**`Fig.155` Edit rule**

</div>

#### Delete rule

1.	Click on delete icon, application show confirmation action message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa343.jpg"></p>

<div align="center">

**`Fig.156` Delete rule**

</div>

2.	On clicking on yes, rule deleted successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa344.jpg"></p>

<div align="center">

**`Fig.157` Delete rule**

</div>

3.	Shows toast message as “Rule deleted successfully "and rule is removed from the list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa345.jpg"></p>

<div align="center">

**`Fig.158` rule deleted**

</div>

### Form Delete

1.	To delete ‘Form’ hover on the form, we can see the buttons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa138.jpg"></p>

<div align="center">

**`Fig.159`  Delete form**

</div>

2.	Once click on ‘Delete’ button, application shows the confirmation pop up, then select the ‘yes’ option.

3.	After click on ‘Yes’ button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa139.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa140.jpg"></p>

<div align="center">

</div>

### Preview of submitted records

1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.160` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.161` Preview of submitted record**

</div>

2. Click on actions, shows actions like sketching, geo tagged images.

3. Click on sketching, to see the sketching done by user in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.162` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.163`  Sketching**

</div>

Click on geo tagged images, to see the images that are geo tagged by user on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa185.jpg"></p>

<div align="center">

**`Fig.164`  Geo tagged images of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa186.jpg"></p>

<div align="center">

**`Fig.165` Geo tagged images**

</div>

Click on attachments to see the attached files. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa187.jpg"></p>

<div align="center">

**`Fig.166`  Attachments of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa188.jpg"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit skecthing function Account administrator  can sketch objects on the map. And edit sketching functionality is explained in the below steps.

1.Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.167` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.168` Preview of submitted record**

</div>

2. Click on actions, shows actions like sketching, geo tagged images.

3. Click on sketching, to see the sketching done in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.169` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.170`  Sketching**

</div>

4. Edit Skecthing 

o Click on edit sketching option on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.171`  Edit Sketching**

</div>

o The options  be displayed and click on measuring tool to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.172`  Edit Sketching**

</div>

o By clicking on measuring tool, options  display the polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.173` Edit Sketching**

</div>

o If Account administrator  select the polyline sketching option, application allows to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.174` Draw sketching by polyline**

</div>

o Click on the finish button so that it end sketching and the polyline  placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.175` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.176` Line sketching**

</div>

o Click on the tick icon to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.177` Save sketching**

</div>

o Click on yes option in the pop-up menu to save skecthing

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.178` Save sketching**

</div>

o By clicking on yes in pop-up menu the sketching  save successfully and toast message  displayed"Sketching Updated Successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.179` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps Account administrator can sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures Account administrator can also sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.180` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.181` Polygon sketching**

</div>

o  Place a marker on the map by using marker option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.182` Marker sketching**

</div>

6. Delete Skecthing

o Account administrator can delete the sketching, to delete the sketching, click on the delete icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.183` Delete sketching**

</div>

o Click on the respective sketching to delete it

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.184` Delete sketching**

</div>

o Click on the save option, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.185` Delete sketching**

</div>

o Click on the tick mark option to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.186` Save sketching**

</div>

o Click on yes option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.187` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.188` Update sketching**

</div>

7. Edit Layers

o Click on the edit sketching button on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.189`  Edit Sketching**

</div>

o Tap on the measuring tool option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.190`  Measuring tool**

</div>

o Tap on the edit layers option. Account administrator  can see sketching data are enabled in to edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.191`  Edit layers**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.192`  Sketching in edit mode**

</div>


o Account administrator  can edit by draging handles on the skecthing.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.193`  Drag Handles or marker**

</div>


o After changes are done click on the save button, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.193` Save Option**

</div>

o Click on tick mark lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.194`  Update Button**

</div>

o Click on yes button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.195`  Update Button**

</div>

o Application displays a toast message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.196`  Update Button**

</div>

8. Sketching full screen view

o By clicking on full screen view option, the map opens in full screen


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.197` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.198` Full screen view sketching**

</div>


### Add/Edit sketching properties in the sketching

1. Account administrator  can add and edit  sketching properties information for sketching objects on the map

2. Account administartor click on the edit sketching button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.199`  Edit Sketching**

</div>

3. Click on the sketching object, pop-up menu open with readonly form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.200`  Sketching Properties Pop-Up menu**

</div>

4. By clicking on edit button in skecting properties, Account administrator can edit/add the fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.201`  Sketching Properties **

</div>

5 By clicking on edit option, form opens on the right side with all editable fields as shown in fig.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.202` Edit Sketching Properties**

</div>

6. Fill all the fields and click on tick mark on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.203` Edit Sketching Properties**

</div>

7. After adding the properties to skecthing successfully, toast message displays as "Properties added successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.204` Edit Sketching Properties**

</div>

### Measuring Tool

1. Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.png"></p>

<div align="center">

**`Fig.205` submitted records table**

</div>

2. Click On submitted record.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.png"></p>

<div align="center">

**`Fig.206` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.png"></p>

<div align="center">

**`Fig.207` submitted record preview**

</div>

3. Click on ellipse button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.png"></p>

<div align="center">

**`Fig.208` Click on ellipse button**

</div>

4. click on sketchings button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.png"></p>

<div align="center">

**`Fig.209` Click on sketching**

</div>

5. Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.png"></p>

<div align="center">

**`Fig.210` View Sketching on map**

</div>

6. Click on draw tool icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.211` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.png"> </p>

<div align="center"> 

**`Fig.212` Draw tool**

</div>

7. Click on Draw a polyline icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.213` click on draw a polyline**

</div>

8. By click on Polyline icon we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.214` Drawn a poly line**

</div>

9. Click on Finish option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.215` Click on Finish Button**

</div>

10. To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.216` Length of distance**

</div>

11. Click on polygon icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.217` Click on polygon icon**

</div>

12. To draw a polygon then click on finish button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.218` Drawn polygon on map**

</div>

13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.219` Area of polygon**

</div>

14. Click on draw a Rectangle icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.220` Click on draw a rectangle**

</div>
15. To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.221` Drawn rectangle on map**

</div>

16. To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.222` Area of rectangle**

</div>

17. Click on Marker icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.223` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.224` Marker on map**

</div>


19. By click on edit layers icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.225` Click on Edit layers icon **

</div>

20. We can see drawn sketchings(polyline,polygon,Rectangle,Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.226` sketchings are in editable mode**

</div>

21. After editing sketchings, click on save option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.227` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.228` saved sketchings**

</div>

22. Click on delete icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.229` Click on delete icon**

</div>

23. Click on clearAll button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.230` Click on clearAll**

</div>

24. Clear all sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.231` Clear All drawed sketchings on map**

</div>

25. Click on Delete icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.232` Click on delete icon**

</div>

26. Click on drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.233` Click on drawn rectangle**

</div>

27. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.234` Deleted drawn rectangle**

</div>

### Search on map 

o Click on search icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.235` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.236` Search on map**

</div>


## Projects

1.	Click on projects icon, opens projects page showing list of projects.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa141.jpg"></p>

<div align="center">

**`Fig.237`  List of projects**

</div>

2.	Search projects, Shows projects available with the searched input.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa142.jpg"></p>

<div align="center">

**`Fig.239`  Search projects**

</div>

3.	Grid view, to view projects as grids click on the icon in the right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa143.jpg"></p>

<div align="center">

**`Fig.240` Grid view of projects**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa144.jpg"></p>

<div align="center">

</div>

### Project Creation

Projects are defined as the Account administrator can be  create Projects and assign to Group administrator, where Group administrator creates certain project tasks and assign work assignment to group of users along with some predefined data.

1.	For creating Projects first go to “Projects” tab and click on ellipse, click on ‘Create’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa145.jpg"></p>

<div align="center">

**`Fig.241` Create projects**

</div>

2.	After clicking on ‘Create’ button, now enter the project details, like ‘Name, Description, Category, Group Admin, start date & End date’ and then click on ‘Create’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa146.jpg"></p>

<div align="center">

</div>

3.	The application  display a message saying, ‘Project created successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa147.jpg"></p>

<div align="center">

</div>

### Edit Project
Account administrator can edit/update the created Project (if required) by clicking on ‘Edit’ option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa148.jpg"></p>

<div align="center">

**`Fig.242`  Edit project**

</div>

- The details like ‘Description, End date’  available for updating.

- After updating the details click on ‘Update button’ to save the changes.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa149.jpg"></p>

<div align="center">

**`Fig.243` Create projects**

</div>

- Once after clicking on the update button, the application displays a toast message as "Project updated successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa150.jpg"></p>

<div align="center">

</div>

### Delete Project

To delete the Project, select the Project and click on ‘Delete’ button. Account administrator can delete the project only if no project task is assigned.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa151.jpg"></p>

<div align="center">

**`Fig.244`  Delete projects**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa152.jpg"></p>

<div align="center">

</div>

- If any task is associated with the Project, then Account administrator cannot delete.

- Toast message gets displayed as "project deleted successfully"

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa153.jpg"></p>

<div align="center">

</div>

### Project Task

 - The project task within the project created by Group administrator assigned to the project. Workflow of this functionality be same as Task.
 - Click on project tasks, shows list of projects tasks under the project which are created by assigned Group administrator.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa154.jpg"></p>

<div align="center">

**`Fig.245`  Project tasks **

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa155.jpg"></p>

<div align="center">

**`Fig.246` List of project tasks **

</div>

- Search project tasks, shows projects tasks with searched input.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa156.jpg"></p>

<div align="center">

**`Fig.247` Search project tasks **

</div>

- Preview of project tasks, shows existing details of project tasks as read only fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa157.jpg"></p>

<div align="center">

**`Fig.248` Preview of project tasks **

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa158.jpg"></p>

<div align="center">

</div>

- Work assignments, click on work assignments icon in project task page, shows list of work assignments under the project task.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa159.jpg"></p>

<div align="center">

**`Fig.249` Work assignment of project tasks **

</div>

- Submitted records, click on submitted records icon shows records of work assignment.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa160.jpg"></p>

<div align="center">

**`Fig.250`  Submitted records of work assignment **

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa161.jpg"></p>

<div align="center">

**`Fig.251`  Submitted records of work assignment **

</div>

### Submitted Records

- Once all the records are displayed, Account administrator can export the data to Excel, mail, PDF, inserted location and add columns.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa162.jpg"></p>

<div align="center">

</div>

### Export to Excel

- To download the data to Excel format, select the records individually or bulk and click on the ‘Export Excel’ button from tabular view.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa163.jpg"></p>

<div align="center">

**`Fig.252` Export to excel-Submitted records**

</div>

- Once after clicking on excel option, now all the list of fields  be displayed select the green color option. The application display a message saying, ‘Please check the downloaded records in downloads tab’

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa164.jpg"></p>

<div align="center">

**`Fig.253`  Column selections page to export records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa165.jpg"></p>

<div align="center">

</div>

- The downloaded Excel appears in this format.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa166.jpg"></p>

<div align="center">

**`Fig.254` Downloaded excel doc of submitted records**

</div>

### Export to Email

- Select records and click on Export to Email option. Opens email attachment popup.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa167.jpg"></p>

<div align="center">

**`Fig.255` Export to email-Submitted records**

</div>

- Account administrator can email the records to the Valid Email & can also send the same to another email by adding in the Email Id in CC which is optional. Select Excel/pdf and click on send

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa168.jpg"></p>

<div align="center">

**`Fig.256`  Email attachment form**

</div>

- Opens columns selection page with list of all columns, selects the columns and click on ✅. Successful pop-up be shown.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa169.jpg"></p>

<div align="center">

**`Fig.257`  Columns selection page-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa170.jpg"></p>

<div align="center">

</div>

### Export to PDF

- Administrator  be can  select single or multiple records and then click on Pdf option to generate the submitted data to PDF format.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa171.jpg"></p>

<div align="center">

**`Fig.258`  Export to pdf-Submitted records**

</div>

- Once after clicking on pdf option, columns selection page opens with the list of all columns, select the columns and click on ✅ 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa172.jpg"></p>

<div align="center">

**`Fig.259` Column selection page-Submitted records**

</div>

- The application  display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa173.jpg"></p>

<div align="center">

</div>

- Downloaded pdf appears in the format shown below.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa174.jpg"></p>

<div align="center">

</div>

### Inserted locations

- Select the records and then click on ‘Inserted Locations’. Locations from where the records were submitted  displayed as a marker on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa175.jpg"></p>

<div align="center">

**`Fig.260` Inserted locations-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa176.jpg"></p>

<div align="center">

</div>

### Add columns

Click on add columns, open columns selection page with list of all columns. Select the columns and click on ✅

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa177.jpg"></p>

<div align="center">

**`Fig.261`  Add columns-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa178.jpg"></p>

<div align="center">

**`Fig.262` Columns selection page**

</div>

The selected columns information  be updated in the table of Submitted records.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa179.jpg"></p>

<div align="center">

</div>

### Preview of submitted records

Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa180.jpg"></p>

<div align="center">

**`Fig.263` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa181.jpg"></p>

<div align="center">

**`Fig.264` Preview of submitted record **

</div>

Click on actions, shows actions like sketching, geo tagged images.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa182.jpg"></p>

<div align="center">

**`Fig.265` Preview of submitted record **

</div>

Click on sketching, to see the sketching done by user in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa183.jpg"></p>

<div align="center">

**`Fig.266` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa184.jpg"></p>

<div align="center">

**`Fig.267`  Sketching**

</div>

### Measuring Tool

1. Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.png"></p>

<div align="center">

**`Fig.268` submitted records table**

</div>

2. Click On submitted record.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.png"></p>

<div align="center">

**`Fig.269` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.png"></p>

<div align="center">

**`Fig.270` submitted record preview**

</div>

3. Click on ellipse button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.png"></p>

<div align="center">

**`Fig.271` Click on ellipse button**

</div>

4. click on sketchings button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.png"></p>

<div align="center">

**`Fig.272` Click on sketching**

</div>

5. Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.png"></p>

<div align="center">

**`Fig.273` View Sketching on map**

</div>

6. Click on draw tool icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.274` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.png"> </p>

<div align="center"> 

**`Fig.275` Draw tool**

</div>

7. Click on Draw a polyline icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.276` click on draw a polyline**

</div>

8. By click on Polyline icon we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.277` Drawn a poly line**

</div>

9. Click on Finish option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.278` Click on Finish Button**

</div>

10. To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.279` Length of distance**

</div>

11. Click on polygon icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.280` Click on polygon icon**

</div>

12. To draw a polygon then click on finish button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.281` Drawn polygon on map**

</div>


13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.282` Area of polygon**

</div>

14. Click on draw a Rectangle icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.283` Click on draw a rectangle**

</div>
15. To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.284` Drawn rectangle on map**

</div>

16. To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.285` Area of rectangle**

</div>

17. Click on Marker icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.286` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.287` Marker on map**

</div>


19. By click on edit layers icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.288` Click on Edit layers icon **

</div>

20. We can see drawn sketchings(polyline,polygon,Rectangle,Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.289` sketchings are in editable mode**

</div>

21. After editing the sketchings, click on save option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.290` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.291` saved sketchings**

</div>

22. Click on delete icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.292` Click on delete icon**

</div>

23. Click on clearAll button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.293` Click on clearAll**

</div>

24. Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.294` Clear All drawed sketchings on map**

</div>

25. Click on Delete icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.295` Click on delete icon**

</div>

26. Click on drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.296` Click on drawn rectangle**

</div>

27. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.297` Deleted drawn rectangle**

</div>

Click on geo tagged images, to see the images that are geo tagged by user on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa185.jpg"></p>

<div align="center">

**`Fig.298`  Geo tagged images of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa186.jpg"></p>

<div align="center">

**`Fig.299` Geo tagged images**

</div>

Click on attachments to see the attached files. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa187.jpg"></p>

<div align="center">

**`Fig.300`  Attachments of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa188.jpg"></p>

<div align="center">

</div>

### Search on map 

o Click on search icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.301` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.302` Search on map**

</div>

### Work flow history

For the project tasks that are associated with workflow, there are two types of work flow levels:
  1. Task level
  2. Record level 

<code>1.Task level:</code>

To view Work flow history for project tasks that are associated with work flow in task level, click on the work flow history icon on the project task hover.

Opens work flow history page with work flow details, status, comments. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa189.jpg"></p>

<div align="center">

**`Fig.303` Workflow history**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa190.jpg"></p>

<div align="center">

**`Fig.304` Workflow history**

</div>

To view work flow history of project tasks that are assigned to record level work flow, in submitted records preview, click on actions and click on work flow history icon. 

Opens work flow history page with work flow details, status, comments.

<code>2. Record level:</code>

- In record level each record have work flow.
- Click on work assignments of project task with record level work flow.
- Click on submitted records icon, shows submitted records of work assignments.
- Click on record status of a record, shows preview of the submitted record.
- Click on actions icon.
- On clicking on actions icon, shows options work flow history, sketching, geo tagged images.
- Click on work flow history icon.
- Shows work flow history page showing work flow details, status and comments.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa191.jpg"></p>

<div align="center">

**`Fig.305` Workflow history**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa192.jpg"></p>

<div align="center">

**`Fig.306` Workflow history**

</div>

### Re-assign the data

Re-assign function defines that, if any record contains invalid or incomplete data then Account administrator re-assign the data to any user (if required).

To re-assign the record, first select the required record, and click on ‘Re-assign’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa193.jpg"></p>

<div align="center">

**`Fig.307` Reassign records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa194.jpg"></p>

<div align="center">

**`Fig.308`  Reassign records**

</div>

Once the record is re-assigned, then the record status  be changed to Reassigned record.

## Tasks

Tasks are defined as the Account administrator can be  assign the work to group of users along with some predefined data (if required).
 - For assigning the task, first go to ‘Tasks tab’ and click on ellipse, click on create

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa195.jpg"></p>

<div align="center">

**`Fig.309` Create task**

</div>

- After clicking on ‘Create’ button, now enter the task details, like ‘Name, Description, Category, Group administrator, Workflow, Workflow Level, Start date, End date’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa196.jpg"></p>

<div align="center">

**`Fig.310` Create task form**

</div>

- Once the details are entered now click on ‘Create’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa197.jpg"></p>

<div align="center">

**`Fig.311` Create tasks**

</div>

- Once the task is created application shows a pop-up message saying, “Task created successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa198.jpg"></p>

<div align="center">

</div>

### Work assignment creation

- After creating the task click on ‘Work assignment option’ to assign the users.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa199.jpg"></p>

<div align="center">

**`Fig.312` Work assignment creation**

</div>

- Now click on ‘+ button’ which is to create a new work assignment.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa200.jpg"></p>

<div align="center">

**`Fig.313` Work assignment creation**

</div>

- Once after clicking on new work assignment creation, the application ask whether to upload pre-populated data ‘Yes or No’.

### Work assignment creation- without pre-populated data

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa201.jpg"></p>

<div align="center">

</div>

- Now click on No and continue.
- Opens work assignment creation form, now enter the details like ‘Assignment name, Form, Frequency, Users, Start date, End date’.
- Once after entering all the details now click on create button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa202.jpg"></p>

<div align="center">

**`Fig.314` Work assignment without pre-papulated data**

</div>

- Once after clicking on create button the application  display a toast message as "Work assignment created successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa203.jpg"></p>

<div align="center">

</div>

### Work assignment creation – with pre-populated data

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa204.jpg"></p>

<div align="center">

**`Fig.315` Work assignment creation with pre-papulated data**

</div>

- Click on yes, and continue.
- Opens work assignment creation form, now enter the details like ‘Assignment name, Form, Frequency, Users, Start date, End date’.
- Select the form and upload the selected form Excel file with pre-populated data.
- Once after entering all the details now click on next button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa205.jpg"></p>

<div align="center">

**`Fig.316` Work assignment creation with pre-papulated data**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa206.jpg"></p>

<div align="center">

**`Fig.317` Assign pre-papulated data to users**

</div>

- Once after clicking on create button the application display a message saying, Work assignment created successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa207.jpg"></p>

<div align="center">

</div>

### Edit Task

- Account administrator can edit/ update the created task (if required) by clicking on ‘Edit’ option. Account administrator can update few details like “Description, Category, Group Admin, End date”. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa208.jpg"></p>

<div align="center">

**`Fig.318` Edit task**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa209.jpg"></p>

<div align="center">

</div>

<code>Note:</code>Account administrator cannot update the assigned Users, assigned Forms, assigned data.

- After editing the details for task click on ‘Update button’ to save the changes. The application  display a message saying, ‘Task updated successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa210.jpg"></p>

<div align="center">

</div>

### Task Deletion

- Account administrator can delete the task only if the start date of the task is not current date, or if no users are assigned to the Task.

- If the task is in-progress, then Account administrator unable to delete the task & application display a message saying, ‘Task is in progress cannot be deleted’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa211.jpg"></p>

<div align="center">

</div>

### Submitted Records

1.	Submitted records define to the Account administrator, that all the user submitted records  displayed. Account administrator can export the data to PDF, Excel.

2.	To get the submitted data click on the required task and then ‘Work assignment’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa212.jpg"></p>

<div align="center">

**`Fig.319` List of tasks**

</div>

3.	Now within the work assignment click on ‘Submitted records.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa213.jpg"></p>

<div align="center">

**`Fig.320`  List of work assignments**

</div>

4.	All the submitted data by the users for the work assignment  be displayed as a list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa214.jpg"></p>

<div align="center">

**`Fig.321` Submitted records of work assignments**

</div>

5.	Once all the records are displayed, Admin can be  export the records to excel, PDF, Attach to Email, Re-assign data, Inserted locations, add columns.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa215.jpg"></p>

<div align="center">

</div>

### Export to PDF
1.	Account administrator can select single or multiple records and then click on export to Pdf option to generate the submitted data to PDF format.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa216.jpg"></p>

<div align="center">

**`Fig.322` Export to pdf-Submitted records**

</div>

2.	Once after clicking on pdf option, now all the list of fields  be displayed select the green color option. 

3.	The application display a message saying, "Please check the downloaded records in downloads tab".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa217.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa218.jpg"></p>

<div align="center">

</div>

### Export to Excel

To download the data to Excel format, select the records individually or bulk and click on the ‘Export to Excel’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa219.jpg"></p>

<div align="center">

**`Fig.323` Export to excel-Submitted records**

</div>

Once after clicking on excel option, columns selection page with list of all columns. Select the columns and click on ✅. 

The application  display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa220.jpg"></p>

<div align="center">

**`Fig.324` Columns selection page**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa221.jpg"></p>

<div align="center">

</div>

### Export to Email

Account administrator can email the records to the Valid Email & can also send the same to another email by adding in Cc which is optional. Select the records and click on export to email

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa222.jpg"></p>

<div align="center">

**`Fig.325`  Export to email-Submitted records**

</div>

1.	After clicking on export to Email option, opens email attachment popup. Enter valid Email, select Excel/pdf and click on send.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa223.jpg"></p>

<div align="center">

</div>

2.	Opens columns selection page with list of columns, select columns and click on ✅.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa224.jpg"></p>

<div align="center">

**`Fig.326`  Columns selection page**

</div>

3.	Successful pop-up  shown on performing email records as" Mail sent successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa225.jpg"></p>

<div align="center">

</div>

### Re-assign the records
- Re-assign function defines that, if any record contains invalid or incomplete data then Account administrator  be  re-assign the data to any user (if required).
- To re-assign the record, first select the required record, and click on ‘Re-assign’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa226.jpg"></p>

<div align="center">

**`Fig.327` Reassign records**

</div>

- Now enter comments and select users and click on submit button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa227.jpg"></p>

<div align="center">

**`Fig.328` Reassign records**

</div>

- Application shows message as “Records reassigned successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa228.jpg"></p>

<div align="center">

</div>

<code>Note:</code>Once the record is re-assigned, then the record status  changed to Reassigned record.

### Inserted locations

First, select the records individually or multiple records and then click on ‘Inserted Locations’. Shows the locations from where the selected records are submitted.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa229.jpg"></p>

<div align="center">

**`Fig.329` Inserted location-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa230.jpg"></p>

<div align="center">

**`Fig.330`  Inserted location-Submitted records**

</div>

### Add columns

- Click on add columns icon in submitted records page of work assignments, opens columns selection page with list of all columns.
- Select the columns and click on ✅.
- The selected columns and information of the columns are updated to the table of submitted records.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa231.jpg"></p>

<div align="center">

**`Fig.331` Add columns-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa232.jpg"></p>

<div align="center">

**`Fig.332`  Columns selection page**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa233.jpg"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit skecthing function Account administrator can sketch objects on the map. And edit sketching functionality is explained in the below steps.

1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa365.jpg"></p>

<div align="center">

**`Fig.333` Preview of submitted record**

</div>

2. Click on actions, shows actions like sketching, geo tagged images.

<p align = "center"><img width = "10000 "height ="366" src = "media/AA/aa182.jpg"></p>

<div align="center">

**`Fig.334` Preview of submitted record **

</div>

3. Click on sketching, to see the sketching done by user in map

<p align = "center"><img width = "10000 "height ="367" src = "media/AA/aa183.jpg"></p>

<div align="center">

**`Fig.335` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.336`  Sketching**

</div>

4. Edit Skecthing 

o Click on edit sketching option on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.337`  Edit Sketching**

</div>

o The options  be displayed and click on measuring tool to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.338`  Edit Sketching**

</div>

o By clicking on measuring tool, options  display the polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.339` Edit Sketching**

</div>

o If Account administrator select the polyline sketching option, application allows Account administrator to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.340` Draw sketching by polyline**

</div>

o Click on the finish button so that it end skecthing and the polyline placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.341` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.342` Line sketching**

</div>

o Click on ✅ to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.343` Save sketching**

</div>

o Click on yes option in the pop-up menu to save skecthing

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.344` Save sketching**

</div>

o By clicking on yes in pop-up menu the sketching  save successfully and toast message displayed "sketching update successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.345` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps Account administrator  sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures Account administrator can sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.346` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.347` Polygon sketching**

</div>

o Account administrator  place a marker on the map by using marker option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.348` Marker sketching**

</div>

6. Delete Skecthing

o Account administrator can delete the sketching, to delete the sketching user click on the delete icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.349` Delete sketching**

</div>

o Click on the respective sketching to get delete

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.350` Delete sketching**

</div>

o Click on the save option, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.351` Delete sketching**

</div>

o Click on ✅ option to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.352` Save sketching**

</div>

o Click on yes option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.353` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.354` Update sketching**

</div>

7. Edit Layers

o Account administrator click on the edit sketching button on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.355`  Edit Sketching**

</div>

o Account administrator tap on the measuring tool option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.356`  Measuring tool**

</div>

o Account administrator tap on the edit layers option. Account administrator can see sketching data are enabled in to edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.357`  Edit layers**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.358`  Sketching in edit mode**

</div>


o Account administrator can edit by draging handles on the skecthing.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.359`  Drag Handles or marker**

</div>


o After changes are done click on the save button, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.360` Save Option**

</div>

o Click on tick mark lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.361`  Update Button**

</div>

o Click on yes button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.362`  Update Button**

</div>

o Account administrator  get a toast message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.363`  Update Button**

</div>

8. Sketching full screen view

o By clicking on full screen view option, the map opens in full screen


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.364` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.365` Full screen view sketching**

</div>


### Add/Edit sketching properties in the sketching

1. Account administrator  add and edit  sketching properties information for sketching objects on the map

2. Account administartor click on the edit sketching button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.366`  Edit Sketching**

</div>

3. Account administrator click on the sketching object, pop-up menu open with readonly form fields

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.367`  Sketching Properties Pop-Up menu**

</div>

4. Account administrator by clicking on edit button in skecting properties, Account administartor can edit/add the form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.368`  Sketching Properties Pop-Up menu**

</div>

5. By clicking on edit option, form opens on the right side with all editable fields as shwon in fig.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.369` Edit Sketching Properties**

</div>

6. Fill all the fields and click on tick mark on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.370` Edit Sketching Properties**

</div>

7. After successfully added the properties to skecthing, toast message displayed "Properties added successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.371` Edit Sketching Properties**

</div>

### Measuring Tool

1. Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.png"></p>

<div align="center">

**`Fig.372` submitted records table**

</div>

2. Click On submitted record.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.png"></p>

<div align="center">

**`Fig.373` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.png"></p>

<div align="center">

**`Fig.374` submitted record preview**

</div>

3. Click on ellipse button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.png"></p>

<div align="center">

**`Fig.375` Click on ellipse button**

</div>

4. Click on sketchings button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.png"></p>

<div align="center">

**`Fig.376` Click on sketching**

</div>

5. Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.png"></p>

<div align="center">

**`Fig.377` View Sketching on map**

</div>

6. Click on draw tool icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.378` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.png"> </p>

<div align="center"> 

**`Fig.379` Draw tool**

</div>

7. Click on Draw a polyline icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.380` click on draw a polyline**

</div>

8. By click on Polyline icon we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.381` Drawn a poly line**

</div>

9. Click on Finish option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.382` Click on Finish Button**

</div>

10. To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.383` Length of distance**

</div>

11. Click on polygon icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.384` Click on polygon icon**

</div>

12. To draw a polygon then click on finish button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.385` Drawn polygon on map**

</div>


13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.386` Area of polygon**

</div>

14. Click on draw a Rectangle icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.387` Click on draw a rectangle**

</div>
15.To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.388` Drawn rectangle on map**

</div>

16. To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.389` Area of rectangle**

</div>

17. Click on Marker icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.390` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.391` Marker on map**

</div>


19. By click on edit layers icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.392` Click on Edit layers icon **

</div>

20. We can see drawn sketchings(polyline,polygon,Rectangle,Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.393` sketchings are in editable mode**

</div>

21. After edit sketchings click on save option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.394` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.395` saved sketchings**

</div>

22. Click on delete icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.396` Click on delete icon**

</div>

23. Click on clearAll button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.397` Click on clearAll**

</div>

24. Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.398` Clear All drawed sketchings on map**

</div>

25. Click on Delete icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.399` Click on delete icon**

</div>

26. Click on drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.400` Click on drawn rectangle**

</div>

27. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.401` Deleted drawn rectangle**

</div>

### Search on map 

o Click on search icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.402` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.403` Search on map**

</div>

### Pending Records

- Click on pending records button which is on the work assignments hover.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa234.jpg"></p>

<div align="center">

**`Fig.404` Pending records**

</div>

- After clicking on pending records, shows pending records with the pre-populated data. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa235.jpg"></p>

<div align="center">

</div>

### Edit assigned records

- Account administrator can add other user to the pending records by selecting the records and select users in the dropdown and click on add user icon. Click on save records. Shows updated successfully message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa236.jpg"></p>

<div align="center">

**`Fig.405` Assign Pending records to user**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa237.jpg"></p>

<div align="center">

</div>

### Delete assigned records

- By selecting records and clicking on delete icon, assigned records can be deleted by Account administrator.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa238.jpg"></p>

<div align="center">

**`Fig.406` Delete records**

</div>

- Open popup to confirm, click on yes.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa239.jpg"></p>

<div align="center">

</div>

- After clicking on yes, the record deleted successfully. Shows Updated successfully message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa240.jpg"></p>

<div align="center">

</div>

### Add records

- Account administrator can add records, to add click on +icon, shows choose file option, upload file with pre-populated data of the same form. 
- Select users, click on add user icon. 
- Shows updated successfully message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa241.jpg"></p>

<div align="center">

**`Fig.407` Add records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa242.jpg"></p>

<div align="center">

**`Fig.408` Add records**

</div>

### Assign records

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa243.jpg"></p>

<div align="center">

**`Fig.409` Assign added records to user**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa244.jpg"></p>

<div align="center">

**`Fig.410` Save records to assign**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa245.jpg"></p>

<div align="center">

</div>

The assigned records for a task  be  Export to excel, pdf.

### Export to excel
-  Account administrator can export the pending records to excel.
-  First, select the records and then click on Excel option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa246.jpg"></p>

<div align="center">

**`Fig.411` Export to excel-Pending records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa247.jpg"></p>

<div align="center">

</div>

- Once after clicking on export to excel option, columns selection page open showing list of all columns. Select the columns and click on✅. 
- Shows message to check the Excel file in downloads tab.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa248.jpg"></p>

<div align="center">

</div>

- The exported records in excel  be downloaded in downloads tab.

### Export to PDF

- Account administrator can export the pending records to PDF.
- First, select the records and then click on export to PDF option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa249.jpg"></p>

<div align="center">

**`Fig.412` Export to pdf**

</div>

- Once after clicking on Export to PDF option, columns selection page open showing list of all columns. Select the columns and click on✅. 
- Shows message to check the pdf in downloads tab.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa250.jpg"></p>

<div align="center">

</div>

- The exported data in PDF  be downloaded in downloads tab.

### Export to Email

- Account administrator can export the pending records to Email option.
- First, select the records and then click on Export to Email option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa251.jpg"></p>

<div align="center">

**`Fig.413` Export to email**

</div>

- Once after clicking on Email option, email attachment popup opens, Enter the to & CC valid email Id’s. Now select the required format ‘Pdf, Excel’ and then click on submit button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa252.jpg"></p>

<div align="center">

**`Fig.414` Email attachment form**

</div>

- Columns selection page open showing list of all columns. Select the columns and click on✅.
- Shows message Email sent successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa253.jpg"></p>

<div align="center">

</div>

## Workflow Management

Workflow Management is the feature provided by FieldOn which we can coordinate and sequence the tasks.

### Workflow creation

- On clicking the “Workflow Management” tab, we can see the list of workflows created previously.
- Click on the “Create” button which would open a page to enter details for workflow creation.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa254.jpg"></p>

<div align="center">

**`Fig.415` Create workflow**

</div>

- Fill the required details, you can add the levels, and then create the workflow.

- After successful creation you can see the created workflow in the list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa255.jpg"></p>

<div align="center">

**`Fig.416` Create workflow**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa256.jpg"></p>

<div align="center">

**`Fig.417`  Workflow creation**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa257.jpg"></p>

<div align="center">

</div>

### Workflow Edit and level deletion

- You can edit a workflow if the workflow is not yet assigned to any task or form.

- Click on “Edit” button and you can change the “From” and “To” values and click on save. Updates the workflow.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa258.jpg"></p>

<div align="center">

**`Fig.418` Edit workflow**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa259.jpg"></p>

<div align="center">

</div>

- If the workflow is not yet assigned to any task or form, the level of the workflow can also be deleted.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa260.jpg"></p>

<div align="center">

</div>

### Workflow deletion

- If the workflow is not assigned to any task or form, it can be deleted.
- Click on the “Delete” button and, on confirmation message click “Yes”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa261.jpg"></p>

<div align="center">

**`Fig.419` Delete workflow**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa262.jpg"></p>

<div align="center">

**`Fig.420` Delete workflow**

</div>

- The workflow  be deleted. The application shows message as “workflow deleted successfully”.

### Workflow Approval Cycles

 We have two types of approval cycles for workflow in FieldOn.
 - Task level workflow
 - Record level workflow

<code>Task level Workflow:</code>

The “Task Level Workflow” is the one assigned on the “Tasks”, the administrators/moderators in the workflow are responsible to approve the whole task by checking the submissions on it.
- While creating a task, we assign workflow to it as follows,

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa263.jpg"></p>

<div align="center">

</div>

- Select the type of workflow.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa264.jpg"></p>

<div align="center">

**`Fig.421` Workflow selection**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa265.jpg"></p>

<div align="center">

**`Fig.210`  Workflow level selection**

</div>

- After you have created the task, and assigned the workflow to it, we can now create assignments for this task.
- For creation of assignment please check the topic “Work Assignments”.
- The data is submitted on these assignments by the field user.
- After the data is submitted, the Account Administrator, needs to change the status of the task, to “Workflow Cycle Started”, then the workflow would be triggered and the task would be pending for approval of the first person in the workflow.
- Example: The following is the workflow used for depicting the task level workflow.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa266.jpg"></p>

<div align="center">

**`Fig.422  Edit status**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa267.jpg"></p>

<div align="center">

**`Fig.423`  No.of days waiting for approval**

</div>

<code>Workflow Cycle Started:</code>
- As in the above workflow, “Chanda GA” is the first person, he find this task in the tasks pending for his approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa268.jpg"></p>

<div align="center">

**`Fig.424`  Workflow approval**

</div>

- On clicking on the above highlighted icon, you can find the recent task “Ballarpur Area” pending for his approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa269.jpg"></p>

<div align="center">

**`Fig.425`  Approval process**

</div>

- Now if the “Chanda GA” rejects the task, all the work assignments under it and corresponding records in it are reassigned to the users.
- When users, submit all the reassigned records for all work assignment under the particular task, the workflow starts again, and the task is again pending for approval of “Chanda GA”
- Now the “Chanda GA” approves and the task goes to next level in the workflow and as you can see in the below screenshot, on approval it would go to “Moderator Chandrapur” (who is a moderator).

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa270.jpg"></p>

<div align="center">

**`Fig.426` Approval process**

</div>

- Now the task comes to the next level, which is at “Moderator Chandrapur”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa271.jpg"></p>

<div align="center">

**`Fig.427` Approval process**

</div>

- “Moderator Chandrapur” can accept or reject the task, if moderator accepts, it goes to the next level, else back to the earlier level.
- Now on accepting, it goes to “Chanda GA” (Note: Please see the from and to for all levels in the workflow)

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa272.jpg"></p>

<div align="center">

</div>

- Again, the “Chanda GA”  be responsible to approve or reject the task.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa273.jpg"></p>

<div align="center">

</div>

- On approving the workflow cycle completes.

<code>Record level Workflow:</code>

The “Record Level Workflow” is the one assigned on the “Tasks/Forms”, the administrators/moderators in the workflow are responsible to approve the record by checking the submissions on it.
Here we take an example of Record Level Workflow on a form. (Note: We have used same workflow as used in task level workflow)
- We assign workflow to a form by editing the form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa274.jpg"></p>

<div align="center">

</div>

- Whenever any record is submitted for this form, the workflow gets triggered and the record goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on “Submitted Record” icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa275.jpg"></p>

<div align="center">

</div>

- Now login as “Chanda GA” and go to the forms records,  find the record status in RED color. The RED color indicates that the record is pending for the logged in user’s approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa276.jpg"></p>

<div align="center">

</div>

- On clicking on the status, the record details open in the side panel. There on clicking on the ellipse,  find accept or reject icons.

### Approval flow – record level

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa277.jpg"></p>

<div align="center">

**`Fig.428` Record level approval**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa278.jpg"></p>

<div align="center">

**`Fig.429`  Record level approval**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa279.jpg"></p>

<div align="center">

</div>

- On rejecting the record, it goes back to the field user for resubmission.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa280.jpg"></p>

<div align="center">

</div>

- When field user submits the data, again the record comes to the “Chanda GA” for approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa281.jpg"></p>

<div align="center">

</div>

- Now when “Moderator Chandrapur” logs in, he can find the record in RED color in forms records, waiting for his approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa282.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa283.jpg"></p>

<div align="center">

</div>

- The next level person, “Chanda GA”, now has to accept or reject the record.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa284.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa285.jpg"></p>

<div align="center">

</div>

## Downloads
- The “Downloads” tab provides the feasibility to download the record reports, that is both excel and pdf reports.
- It has the other details like File name, generated on, size of file, downloaded from, type of record, from date, to date, task name, download option.
- The files under “Downloads” tab gets deleted automatically after 24 hours of download.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa286.jpg"></p>

<div align="center">

**`Fig.430` Downloads page**

</div>

## Settings

Account administrator  be can perform some of the actions from the system. The below is the list
- Profile
- Privileges
- Configurations
- Reference List
- Application Settings
- Layers Configuration.

### Profile
- Account administrator can change the user details like ‘First name, Last name, Email, Phone Number, Image’.
- First, click on the profile and then edit the required details and then click on save option. The changes updated with new ones.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa287.jpg"></p>

<div align="center">

**`Fig.431`  Profile settings**

</div>

- The application  display a message saying, ‘Profile Updated Successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa288.jpg"></p>

<div align="center">

</div>

### Privileges

The Account administrator can update the privileges for ‘Group administrator & Moderators’.

### Group administrator Privileges

- First, select the Group administrator and then select the administrator name all the list of functionalities for the group administrator displayed.
- For the functionalities which are in view state, account administrator can change the state to Edit. Whereas for the functionalities which are in Edit state, administrator  be  change the state to View.
- Toggle On & Off to change the states.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa289.jpg"></p>

<div align="center">

**`Fig.432` Previllege settings**

</div>

### Moderator Privileges

- First, select the Moderator and then select the administrator name all the list of functionalities for the Moderator  displayed.
- For the functionalities which are in view state, account administrator can change the state to Edit. Whereas for the functionalities which are in Edit state, administrator  be  change the state to View.
- Toggle On & Off to change the states.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa290.jpg"></p>

<div align="center">

**`Fig.433` Previllege settings**

</div>

### Configurations

- The Account administrator auto approve the devices at a single stretch by clicking on auto approve option.
- First, select the configurations and then click on auto approve devices. All the devices be auto approved for the account.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa291.jpg"></p>

<div align="center">

**`Fig.434` Configuration-settings**

</div>

### Reference List
- First click on the reference list and then application  ask to Upload excel data.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa292.jpg"></p>

<div align="center">

**`Fig.435` Reference list creation**

</div>

- Select the required excel sheet having the drop-down fields and its values.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa293.jpg"></p>

<div align="center">

**`Fig.436` Reference list creation**

</div>

- Once after selecting the valid file now click on Submit option so that the uploaded sheet updated in the system. The application display a message saying ‘Reference list created successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa294.jpg"></p>

<div align="center">

</div>

- Account administrator can add or Edit the uploaded values in the system. 

### Application Settings

-  Account administrator can  update the account lock or lock interval for the users who are accessing the application by entering the invalid details for few attempts.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa295.jpg"></p>

<div align="center">

**`Fig.437` Application Settings**

</div>

- First, select the account lock, lock interval, and then click on update button. For account block enter the value and for account interval enter the value and then save the values.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa296.jpg"></p>

<div align="center">

**`Fig.438` Application Settings**

</div>

### layer configuration
 
o Account administrator can perform layer configuration settings by clicking on layer configuration.

<p align="center"> <img width="1000" height="450" src="media/AA/settings page view.jpg"> </p>

<div align="center"> 

**`Fig.439` Settings page**

</div>

1. Create Layer

o Application opens layer configuration page with create layer and existing layers list.

o Click on +/create layer button.

<p align="center"> <img width="1000" height="450" src="media/AA/layers settings page.jpg"> </p>

<div align="center"> 

**`Fig.440` Layer configuration settings page**

</div>

o Add new layer form page opens with external name, internal name, layer url, layer type and create, cancel buttons.

<p align="center"> <img width="1000" height="450" src="media/AA/create layer page.jpg"> </p>

<div align="center"> 

**`Fig.441` Add new layer page**

</div>

o Enter details, select layer type wms/wmts and click on create button.

<p align="center"> <img width="1000" height="450" src="media/AA/create layer form.jpg"> </p>

<div align="center"> 

**`Fig.442` Add new layer page**

</div>

o Application displays toast message for layer creation.

<p align="center"> <img width="1000" height="450" src="media/AA/create layer msg.jpg"> </p>

<div align="center"> 

**`Fig.443` Toast for layer creation**

</div>

2. Edit Layer

o In layer configuration page select account from the dropdown list, shows the existing layers of the selected account.

<p align="center"> <img width="1000" height="450" src="media/AA/layer edit icon.jpg"> </p>

<div align="center"> 

**`Fig.444` Layers list**

</div>

o Click on edit layer option, opens edit layer form with layer url, layer type in editable mode and update, cancel button.

o Update the required changes and click on update button.

<p align="center"> <img width="1000" height="450" src="media/AA/layer edit.jpg"> </p>

<div align="center"> 

**`Fig.445` Update layer**

</div>

o Application displays toast message for layer update.

<p align="center"> <img width="1000" height="450" src="media/AA/layer update.jpg"> </p>

<div align="center"> 

**`Fig.446` Update layer toast**

</div>

3. Preview layer

o Click on preview layer option.

o Opens map showing layers data on map. 

<p align="center"> <img width="1000" height="450" src="media/SU/layer preview.jpg"> </p>

<div align="center"> 

**`Fig.447` Layer preview**

</div>

4.  Search on map 

o Click on search icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.448` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.449` Search on map**

</div>

5. Delete layer

o Click on delete layer icon.

<p align="center"> <img width="1000" height="450" src="media/AA/layer delete icon.jpg"> </p>

<div align="center"> 

**`Fig.450` Delete layer**

</div>

o Application displays confirm action message, click on yes.

<p align="center"> <img width="1000" height="450" src="media/AA/confirm delete.jpg"> </p>

<div align="center"> 

**`Fig.451` Delete action message**

</div>

o Application displays toast message for layer delete.

<p align="center"> <img width="1000" height="450" src="media/AA/layer delete msg.jpg"> </p>

<div align="center"> 

**`Fig.452` Toast for delete layer**

</div>

## Dashboards

The Dashboards provide the statistical information present under the particular login. Following are the different roles under the application and their respective dashboards.
Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.

### Account Administrator

Under Account Administrator we have following entities:
- Account Specific
- Projects
- Tasks
- Users
- Device Management

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa297.jpg"></p>

<div align="center">

**`Fig.453`  Dashboards**

</div>

- Under the “Account Specific” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa298.jpg"></p>

<div align="center">

**`Fig.454` Account specific-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa299.jpg"></p>

<div align="center">

**`Fig.455` Account specific-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa300.jpg"></p>

<div align="center">

</div>

- Under the “Projects” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa301.jpg"></p>

<div align="center">

**`Fig.456`  Projects-Dashboards**

</div>

- Under the “Tasks” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa302.jpg"></p>

<div align="center">

**`Fig.457` Tasks-Dashboards**

</div>

- Under the “Users” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa303.jpg"></p>

<div align="center">

**`Fig.458` Users-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa304.jpg"></p>

<div align="center">

</div>

- Under the “Device Management” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa305.jpg"></p>

<div align="center">

**`Fig.459` Device management-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa306.jpg"></p>

<div align="center">

</div>















