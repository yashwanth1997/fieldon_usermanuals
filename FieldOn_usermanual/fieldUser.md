
# Field User
<!-- {docsify-ignore} -->

The Field User Mobile application is a key interface for field operation team. It enhances the performance of the field crew and provides all the information required. Field User will use it for data capturing.

<p align="center"> <img width="1000" height="500" src="media/FUA.jpg"> </p>

<div align="center"> 

**`Fig` Field user activities**

</div>

Download FieldOn mobile application from playstore or app store. Install and open it.

### Login Page

Mobile Login (For every New Version Installation on devices uninstall the older version and Install new Apk). 

<p>Enter valid field user username and password and click on "Sign in".</p>

<p align="center"> <img width="250" height="400" src="media/FU/FU1.jpg"> </p>
  
<div align="center"> 

**`Fig.1` FieldOn mobile login page**

</div>

### Forgot Password

If user Forgets Password, then user can request for a new Password which will be sent to registered Email ID. 

If user enters Invalid Username it will display an alert saying, Invalid username or Email ID.

 1. If user enters Invalid Password 5 times simultaneously the account will be blocked 

<p align="center"> <img width="250" height="400" src="media/FU/FU2.jpg"> </p>

<div align="center"> 

**`Fig.2` Forgot password screen**

</div>

### Home

<p> In this user can find the dash board, user can see the tabs for forms and assignments. By clicking on form user can see all forms available in his account. By clicking on assignment, user can find out all assigned and re-assigned assignments(tasks).</p>

<p align="center"> <img width="250" height="400" src="media/FU/FU3.jpg"> </p>

<div align="center"> 

**`Fig.3` Home screen**

</div>

### Dashboard

<p> Mobile user can see the number of assignments are assigned/ re-assigned to him/her and user can find number forms available in their account.</p>

### Forms Tab

Mobile user can see all the created Forms in the Forms Tab. User will not have a provision to see forms which are created by other admin. 

All the created forms will be shown as a list for mobile user.

<p align="center"> <img width="250" height="400" src="media/FU/FU4.jpg"> </p>

<div align="center"> 

**`Fig.4` Forms page**

</div>

#### Form Info 

Mobile User do left swipe on forms it displays two options:

 o	Info

 o	Download

Mobile user on left swipe of Form, and on clicking info, description will be displayed of that Form. 

The description shown in the info is created at the time of Form Creation. The information shown is in read-only mode which is not editable.

User click download the form to work in offline.

<p align="center"> <img width="250" height="400" src="media/FU/FU5.jpg"> </p>

<div align="center"> 

**`Fig.5` Form info**

</div>

### Form Download

Mobile User on left swipe of the Form and on click of DOWNLOAD option the Form will be downloaded. Mobile User can view the downloaded Form in offline mode in Forms page.

Mobile User can see the download forms in offline. 

Mobile User cannot submit forms when user  is  in offline.

Mobile User can see the download form with Symbology Representation. 

<p align="center"> <img width="250" height="400" src="media/FU/FU6.jpg"> </p>

<div align="center"> 

**`Fig.6` Form download**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU7.jpg"> </p>

<div align="center"> 

**`Fig.7` Downloaded form in offline**

</div>

### Form Search

Mobile User can search any Form based on the Form name. 

If Mobile User is having many Forms all the Forms can be filtered based on search criteria.

<p align="center"> <img width="250" height="400" src="media/FU/FU8.jpg"> </p>

<div align="center"> 

**`Fig.8` Form search**

</div>

### Form New Record Insertion

If Mobile User wants to submit any data, first User needs to tap on any of the form and then click on "+" button. When Mobile User clicks on + button Form with its corresponding fields will open. 

User can enter the data like Barcode Scanning, Image Capturing, fetching location details, once all the data is captured, Mobile User can save/submit the data.

<p align="center"> <img width="250" height="400" src="media/FU/FU9.jpg"> </p>

<div align="center"> 

**`Fig.9` New record insertion**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU10.jpg"> </p>

<div align="center"> 

**`Fig.10` New record of a form**

</div>

### Save Data

If a form is having 5 fields and if Mobile User has captured data for only 3 fields and remaining 2 fields are kept empty then in that case Mobile User can save the data first and again when require user can re-open the form fill the remaining fields and submit.

Mobile user can able to save the data in record when location/gps is enable. User cannot save the data in record without enabling the location/gps in mobile.

<p align="center"> <img width="250" height="400" src="media/FU/FU11.jpg"> </p>

<div align="center"> 

**`Fig.11` Saving form record**

</div>

#### Submit Button

User can view the assigned Forms work on them and can submit. User on click of SUBMIT option system will validate the form if all mandatory field values are available.

Once the complete data is captured and user on click of Submit button a message saying, Form submitted successfully will Pop-Up.

Mobile user can submit when location/gps is enable. User cannot submit record without enabling the location/gps. In case user try to submit without location it will display a alert message fetching location please enable.

<p align="center"> <img width="250" height="400" src="media/FU/FU12.jpg"> </p>

<div align="center"> 

**`Fig.12` Submit form record**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU13.jpg"> </p>

<div align="center"> 

**`Fig.13` Submit form record**

</div>

### Saved Functionality

User can see the saved data as a list view. User can submit the record by opening it and click on "submit button".

Mobile User if finds the saved record is not appropriate user can delete the saved record. By swiping the left side user able to see delete option form there he can delete saved record.
         			
### History

User on clicking History, all submitted data will be listed in history page. 

However, submitted data will open a form with all fields which are in read-only mode.

<p align="center"> <img width="250" height="400" src="media/FU/FU14.jpg"> </p>

<div align="center"> 

**`Fig.14` History for form records**

</div>

## Task Assignment Work Flow

### Tasks Assignment Tab

Mobile user can see all the created assignments in the assignments Tab. User will not have a provision to see assignments which are not assigned to him.

By clicking assignments tab, the user will see the list of assignments which are associated with user.

<p align="center"> <img width="250" height="400" src="media/FU/FU15.jpg"> </p>

<div align="center"> 

**`Fig.15` Assignments**

</div>

### Assignment Info 

Mobile User do left swipe on assignments it displays three options:

 o	Info

 o	Download

 o	sync

Mobile user on left swipe of assignments, and on clicking info, description will be displayed of that assignment. 

The description shows the info of assignments of start date, end date, created by, task name. The information shown is in read-only mode which is not editable.

<p align="center"> <img width="250" height="400" src="media/FU/FU16.jpg"> </p>

<div align="center"> 

**`Fig.16` Various options for Assignments**

</div>

### Assignment Download

Mobile User on left swipe of the assignment and on click of DOWNLOAD option the assignment will be downloaded. Mobile User can view the downloaded assignment in offline mode in assignments page.

Mobile User can see the download assignment with Symbology Representation. 

Mobile User can see the download assignment in offline. 

Mobile User cannot submit assigment  when user is  in offline.

<p align="center"> <img width="250" height="400" src="media/FU/FU17.jpg"> </p>

<div align="center"> 

**`Fig.17` Download Assignment**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU18.jpg"> </p>

<div align="center"> 

**`Fig.18`  Downloaded assignment in offline**

</div>

### Sync

<p> Once user works in offline, can go to online and can sync the data back and can sync the only valid data, cannot sync invalid data.
User cannot sync data when location is turned off, it shows alert message.</p>

<p align="center"> <img width="250" height="400" src="media/FU/FU19.jpg"> </p>

<div align="center"> 

**`Fig.19`  Sync assignments**

</div>

### Assignment Search

Mobile User can search any assignment based on the assignment name. 

If Mobile User is having many assignments all the assignment can be filtered based on search criteria.

In search box we have a filter option it will sort assignments based on the status of the assignment.

By clicking on the filter option, the pop-up menu will appear with options of assigned and reassigned.

By clicking any one of the above options and click apply then it shows the respective assignment with given status 

<p align="center"> <img width="250" height="400" src="media/FU/FU20.jpg"> </p>

<div align="center"> 

**`Fig.20`  Filter assignments**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU21.jpg"> </p>

<div align="center"> 

**`Fig.21`  Filter assignments**

</div>

### New Record Insertion

By click on assignments, user can see the associated records if any so user can fill the data.

If Mobile User wants to submit any data, first User needs to tap on any of the assignment and then click on "+" button. When Mobile User clicks on + button in assignment the new record with its corresponding fields will open. 

User can enter the data like Barcode Scanning, Image Capturing, fetching location details, once all the data is captured, Mobile User can save/submit the data.

<p align="center"> <img width="250" height="400" src="media/FU/FU22.jpg"> </p>

<div align="center"> 

**`Fig.22`  New record insertion**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU23.jpg"> </p>

<div align="center"> 

**`Fig.23`  New record insertion**

</div>

### Reassigned records

In this assignment tab, user can see reassigned records / reassigned assignments if any exists.

<p align="center"> <img width="250" height="400" src="media/FU/FU24.jpg"> </p>

<div align="center"> 

**`Fig.24`  Status of record**

</div>

<code>Note:</code> User can able to update the data in the re-assigned records, and user can submit the record with updated data. 

### History

User on clicking History, all submitted data will be listed in history page. 

However, submitted data will open a record in assignment it shows with all fields which are in read-only mode.

<p align="center"> <img width="250" height="400" src="media/FU/FU25.jpg"> </p>

<div align="center"> 

**`Fig.25`  History of assignments**

</div>

### Map view

By selecting one of the records, and click on the "map view icon".

It will display the information of record in the map with location according to record.

From map we can fill /update/submit records form it.

<p align="center"> <img width="250" height="400" src="media/FU/FU26.jpg"> </p>

<div align="center"> 

**`Fig.26`  Map view**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU27.jpg"> </p>

<div align="center"> 

**`Fig.27`  Map view**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU28.jpg"> </p>

<div align="center"> 

**`Fig.28`  Map view**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU29.jpg"> </p>

<div align="center"> 

**`Fig.29`  Map view**

</div>

### Save Data

If an assignment record is having 5 fields and if Mobile User has captured data for only 3 fields and remaining 2 fields are kept empty then in that case Mobile User can save the data first and again when require user can re-open the record to fill the remaining fields and submit.

Mobile user can able to save the data in record when location/gps is enable. User cannot save the data in record without enabling the location/gps.

Mobile user tries to save data without enable location it will show message fetching location.

<p align="center"> <img width="250" height="400" src="media/FU/FU30.jpg"> </p>

<div align="center"> 

**`Fig.30`  Save assignment**

</div>

### Submit Button

User can view the assignment record work on them and can submit. User on click of SUBMIT option system will validate the record if all mandatory field values are available.

Once the complete data is captured and user on click of Submit button a message saying, Form submitted successfully will Pop-Up.

Mobile user can do submit when only location/gps is enable. User cannot submit record without enabling the location/gps. In case user try to submit without location it will display a alert message fetching location please enable.

<p align="center"> <img width="250" height="400" src="media/FU/FU31.jpg"> </p>

<div align="center"> 

**`Fig.31`  Submit assignment**

</div>

### File Attachments

User can assign the files to record while submitting the record by clicking the attachment.

User view the uploaded files.

User can upload max 5 files and each file limit is max 5mb.

<p align="center"> <img width="250" height="400" src="media/FU/FU32.jpg"> </p>

<div align="center"> 

**`Fig.32`  File attachments**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU33.jpg"> </p>

<div align="center"> 

**`Fig.33`  File attachments**

</div>

<code>Note:</code> PDF files, .doc, .docx, .png, .jpg, xlsx, .xls, .ppt, .pptx format files will accept by the application.

### Sketching

1.	User can place the geometry point on map like point, area and line, User can add the properties on placed geometry.

2.	User can delete the placed point by clicking (x) icon. user can undo last deleted point operation by clicking the back arrow, after deleting the points if you are done click on "tick mark" icon for okay, click (x) icon to cancel the operation.

3.	User can clear all the sketching at a time, by clicking clear icon.

4.	User can edit the sketch by clicking the pencil icon as shown in figure.

<p align="center"> <img width="250" height="400" src="media/FU/FU34.jpg"> </p>

<div align="center"> 

**`Fig.34`  Steps for sketching**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU35.jpg"> </p>

<div align="center"> 

**`Fig.35`  Steps for sketching**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU36.jpg"> </p>

<div align="center"> 

**`Fig.36`  Steps for sketching**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU37.jpg"> </p>

<div align="center"> 

**`Fig.37`  Steps for sketching**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU38.jpg"> </p>

<div align="center"> 

**`Fig.38`  Steps for sketching**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU39.jpg"> </p>

<div align="center"> 

**`Fig.39`  Steps for sketching**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU40.jpg"> </p>

<div align="center"> 

**`Fig.40`  Steps for sketching**

</div>

### Settings

By clicking the settings icon, we will able to see some options according to the application.

<p align="center"> <img width="250" height="400" src="media/FU/FU41.jpg"> </p>

<div align="center"> 

**`Fig.41`  Settings tab**

</div>

### Moving to offline Mode

User clicking on network toggle switch into application level, user can switch easy to offline mode/online mode.

<p align="center"> <img width="250" height="400" src="media/FU/FU42.jpg"> </p>

<div align="center"> 

**`Fig.42`  Online mode**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU43.jpg"> </p>

<div align="center"> 

**`Fig.43`  Offline mode**

</div>

When user moves to the offline, user able to view only the downloaded tasks and forms.

User can enter the data in the records and able to save it, and when user comes to online can able to submit the saved records.  

### Select gps device

The application can connect the gps devices through Bluetooth, by clicking select gps device.

If you dont turn on Bluetooth it will shows a message Bluetooth not enable, please enable Bluetooth.

User have to select gps device from that available list.

<p align="center"> <img width="250" height="400" src="media/FU/FU44.jpg"> </p>

<div align="center"> 

**`Fig.44`  Bluetooth devices**

</div>

<p align="center"> <img width="250" height="400" src="media/FU/FU45.jpg"> </p>

<div align="center"> 

**`Fig.45`  Bluetooth devices**

</div>

### Change password

By clicking change password wed will redirect to the password change page.

By entering the old password, new password, confirm password and password is set to be according the restrictions.

After clicking on submit show the message password changed successfully and user gets logout automatically.

User has to login again with the new password.

<p align="center"> <img width="250" height="400" src="media/FU/FU46.jpg"> </p>

<div align="center"> 

**`Fig.46`  Change password**

</div>

### Version

It will display the information on which version the application is running.

Beside that refresh option checks the new version of the application, if the new version is available, it will download the apk.

<p align="center"> <img width="250" height="400" src="media/FU/FU47.jpg"> </p>

<div align="center"> 

**`Fig.47`  Version**

</div>

### About us

Here it will display the information and silent features of the application briefly.

<p align="center"> <img width="250" height="400" src="media/FU/FU48.jpg"> </p>

<div align="center"> 

**`Fig.48`  About us**

</div>

### Last Login on

It shows the last login date and time.

<p align="center"> <img width="250" height="400" src="media/FU/FU49.jpg"> </p>

<div align="center"> 

**`Fig.49`  Last login**

</div>

### Notification

User will get the notification for task assignment and reassignment. 

From the notifications he can able to proceed directly by clicking on it. 

<p align="center"> <img width="250" height="400" src="media/FU/FU50.jpg"> </p>

<div align="center"> 

**`Fig.50`  Notifications**

</div>

### Logout

User can find the logout option in setting tab. By clicking on logout button user logged out. 

<p align="center"> <img width="250" height="400" src="media/FU/FU51.jpg"> </p>

<div align="center"> 

**`Fig.51`  Logout**

</div>










