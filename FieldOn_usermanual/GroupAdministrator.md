
# Group Admnistrator
<!-- {docsify-ignore} -->
Group administrator  be created by the Super Administrator. Group administrator can mapped to "n" number of Accounts and one Account have multiple group administrators. So, group administrator can view all the projects and tasks information mapped to the respective group administrator accounts.

<p align="center"> <img width="1000" height="500" src="media/GAA.jpg"> </p>

<div align="center"> 

**`Fig` Group administrator activities**

</div>

<p>Enter valid Group Administartor username and password and click on "Sign in".</p>

Login as [Group Administartor ](https://demo.fieldon.com/#/login)

<p align="center"> <img width="1000" height="450" src="media/GA/ga1.png"> </p>

<div align="center"> 

**`Fig.1` Group Administrator login page**

</div>

- After login we can see the Group Administartor page, list of below options in the menu bar.

 - <code>Users</code>	
 - <code>Forms</code>
 - <code>Projects</code>	
 - <code>Task</code>
 - <code>Downloads</code>


<hr>

## Users

1.	On click of "Users tab" we can see the list of Users available.

<p align="center"> <img width="1000" height="450" src="media/GA/ga2.png"> </p>

<div align="center"> 

**`Fig.2` User list**

</div>

### Preview Users

1.	To preview "user" hover on the user, we can see the buttons.

<p align="center"> <img width="1000" height="450" src="media/GA/ga3.png"> </p>

<div align="center"> 

**`Fig.3` Preview User**

</div>

2.	Click on "preview button", view the existing data only in read only fields ,Group administrator cannot update user information

<p align="center"> <img width="1000" height="450" src="media/GA/ga4.png"> </p>

<div align="center"> 

**`Fig.4`  Preview User data**

</div>

### User search 

1.Enter the user’s name in the search box and click on “ →", it sort the respective user in the user list

<p align="center"> <img width="1000" height="450" src="media/GA/ga5.png"> </p>

<div align="center"> 

**`Fig.5` Search User**

</div>

## Forms
1.	When clicked on "Forms tab", we can see the list of Forms available.
2.	Account Administrator can create Forms. Group Administrator cannot create/edit the forms.

<p align="center"> <img width="1000" height="450" src="media/GA/ga6.png"> </p>
<div align="center"> 

**`Fig.6` List of Forms**

</div>

### Form Preview
1.	To Preview the Form, go to the form and hover on the form, we can see preview button,
2.	Click on "preview button" a popup  be open with all the widgets in the form.

<p align="center"> <img width="1000" height="450" src="media/GA/ga7.png"> </p>

<div align="center"> 

**`Fig.7` Preview Forms**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga8.png"> </p>

### Form Export to Excel
1.	We can Export the Form to Excel to get all the widgets of the form available in the form of excel.by clicking "excel icon".
2.	We can use fill this excel and upload in Work Assignments to user.

<p align="center"> <img width="1000" height="450" src="media/GA/ga9.png"> </p>

<div align="center"> 

**`Fig.9` Export to excel**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga10.png"> </p>

### Submitted forms
1.	Click on the "submitted icon" to view the user submitted forms
2.	Once all the records are displayed, Administrator can export the data to PDF, EXCEL, Attach as Email, Re-assign data, Go to Map view. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga11.png"> </p>

<div align="center"> 

**`Fig.11` View submitted forms**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga12.png"> </p>

### Export to Excel 
1.	To download the data to Excel format, select the records individually or bulk and click on the "Export Excel" button from tabular view. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga13.png"> </p>

<div align="center"> 

**`Fig.13` Export to excel**

</div>

2.	Once after clicking on excel option, now all the list of fields displayed select the "green color option". 
3.	The application display a message saying, ‘Please check the downloaded records in downloads tab’ 

<p align="center"> <img width="1000" height="450" src="media/GA/ga14.png"> </p>
4.	The downloaded Excel appears in this format 

<p align="center"> <img width="1000" height="450" src="media/GA/ga15.png"> </p>

### Attach Email
1.	Administrator can email the records to the Valid Email & can also send the same to another email by adding in the Alternative Email Id which is optional. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga16.png"> </p>

<div align="center"> 

**`Fig.16` Select Email option**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga17.png"> </p>
<div align="center"> 

**`Fig.17` Email generation form**

</div>

2.	Administrator can attach records and send to any email Id, Via., Excel, PDF. Successful pop-up  be shown on performing email records.

### Export to PDF 
1.	Group Administrator select single or multiple records and then click on "Pdf option" to generate the submitted data to PDF format.

<p align="center"> <img width="1000" height="450" src="media/GA/ga18.png"> </p>

<div align="center"> 

**`Fig.18` Select Export PDf Option**

</div>

2. Once after clicking on "pdf option", now all the list of fields  displayed select the green color option. The application  display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga19.png"> </p>

<p align="center"> <img width="1000" height="450" src="media/GA/ga20.png"> </p>

<div align="center"> 

**`Fig.20`PDF preview**

</div>

### See on Map
1.	Select the records individually or multiple records and then click on ‘Inserted Locations’. All the submitted data  be displayed as a marker on map.

<p align="center"> <img width="1000" height="450" src="media/GA/ga21.png"> </p>

<div align="center"> 

**`Fig.21`See on map**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga22.png"> </p>
<div align="center"> 

**`Fig.22`Map preview of inserted location**

</div>

### Filter workflow records 
1.By click on filter workflow records option, it filter out records between with workflow and without workflow.

<p align="center"> <img width="1000" height="450" src="media/GA/ga23.png"> </p>

<div align="center"> 

**`Fig.23`Filter workflow records**

</div>

### Filter records 
1.By clicking on "filter records", it display the pop menu with fields of  form date, to date, users click on "get data" it  show the respective filtered data.

<p align="center"> <img width="1000" height="450" src="media/GA/ga24.png"> </p>

<div align="center"> 

**`Fig.24`Filter records**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga25.png"> </p>

### Refresh records 
1. Click on "refresh option", it get refresh the record list and shows newly added records in the list.

<p align="center"> <img width="1000" height="450" src="media/GA/ga26.png"> </p>

<div align="center"> 

**`Fig.26`Refresh records**

</div>


### Preview of submitted records
1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.27` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.28` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done by user in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.29` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.30`  Sketching**

</div>

Click on "geo tagged images", to see the images that are geo tagged by user on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa185.jpg"></p>

<div align="center">

**`Fig.31`  Geo tagged images of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa186.jpg"></p>

<div align="center">

**`Fig.32` Geo tagged images**

</div>

Click on "attachments" to see the attached files. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa187.jpg"></p>

<div align="center">

**`Fig.33`  Attachments of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa188.jpg"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit skecthing function administrator sketch objects on the map. And edit sketching functionality is explained in the below steps.

1.Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.34` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.35` Preview of submitted record**

</div>

2. Click on "actions", shows actions options like sketching, geo tagged images.

3. Click on "sketching", to see the sketching submitted in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.36` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.37`  Sketching**

</div>

4. Edit Skecthing 

o Click on "edit sketch" option on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.38`  Edit Sketch**

</div>

o The options displayed and click on "measuring tool" to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.39`  Edit Sketch**

</div>

o By clicking on "measuring tool", options display the polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.40` Edit Sketch**

</div>

o If Administrator select the polyline sketch option, application allows administrator to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.41` Draw sketching by polyline**

</div>

o Click on the "finish" button so that it end skecth and the polyline placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.42` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.43` Line sketching**

</div>

o Click on the "tick icon " to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.44` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to save skecthing

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.45` Save sketching**

</div>

o By clicking on "yes" in pop-up menu the sketching  save successfully and message displayed"Sketching updated Successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.46` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps Administrator  sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures Administrator sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.47` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.48` Polygon sketching**

</div>

o Administrator  place a marker on the map by using "marker" option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.49` Marker sketching**

</div>

6. Delete Skecthing

o Administrator can delete the sketch, to delete the sketch administrator need to click on the "delete" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.50` Delete sketching**

</div>

o Click on the respective sketching to get delete

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.51` Delete sketching**

</div>

o Click on the "save" option, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.52` Delete sketching**

</div>

o Click on the "tick mark "option to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.53` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.54` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.55` Update sketching**

</div>

7. Edit Layers

o Administrator click on the "edit sketch" button on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.56`  Edit Sketching**

</div>

o Administrator tap on the "measuring tool" option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.57`  Measuring tool**

</div>

o Administrator tap on the "edit layers"option. Administrator can see sketching data are enabled in to edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.58`  Edit layers**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.59`  Sketching in edit mode**

</div>


o Administrator can edit by "draging handles" on the skecthing.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.60`  Drag Handles or marker**

</div>


o After changes are done administrator click on the "save" button, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.61` Save Option**

</div>

o Click on "tick mark" lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.62`  Update Button**

</div>

o Click on "yes" button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.63`  Update Button**

</div>

o Administratot get a message "sketching updated successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.64`  Update Button**

</div>

8. Sketching full screen view

o By clicking on "full screen view "option, the map opens in full screen


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.65` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.66` Full screen view sketching**

</div>


### Add/Edit sketching properties in the sketching

1. Administrator add and edit  sketching properties information for sketching objects on the map

2. Administartor click on the "edit sketch" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.67`  Edit Sketching**

</div>

3. Administrator click on the sketching object, pop-up menu open with readonly form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.68`  Sketching Properties Pop-Up menu**

</div>

4.Administrator by clicking on "edit" button in skecting properties, administartor can edit/add the fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.69`  Sketching Properties**

</div>

5 By clicking on "edit" option, form opens on the right side with all editable fields as shown in fig.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.70` Edit Sketching Properties**

</div>

6. Fill all the fields and click on "tick" mark on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.71` Edit Sketching Properties**

</div>

7. After successfully added the properties to skecthing, message displayed "Properties added successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.72` Edit Sketching Properties**

</div>

### Measuring Tool

1.Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.png"></p>

<div align="center">

**`Fig.73` submitted records table**

</div>

2.Click On submitted record.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.png"></p>

<div align="center">

**`Fig.74` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.png"></p>

<div align="center">

**`Fig.75` submitted record preview**

</div>

3.Click on "ellipse" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.png"></p>

<div align="center">

**`Fig.76` Click on ellipse button**

</div>

4.Click on "sketchings" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.png"></p>

<div align="center">

**`Fig.77` Click on sketching**

</div>

5.Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.png"></p>

<div align="center">

**`Fig.78` View Sketching on map**

</div>

6.Click on "Measuring tool" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.79` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.png"> </p>

<div align="center"> 

**`Fig.80` Draw tool**

</div>

7.Click on "draw a polyline" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.81` click on draw a polyline**

</div>

8.By click on "Polyline icon" we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.82` Drawn a poly line**

</div>

9.Click on "Finish "option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.83` Click on Finish Button**

</div>

10.To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.84` Length of distance**

</div>

11.Click on "polygon icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.85` Click on polygon icon**

</div>

12. Draw a polygon then click on "finish button".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.86` Drawn polygon on map**

</div>


13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.87` Area of polygon**

</div>

14. Click on "draw a Rectangle icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.88` Click on draw a rectangle**

</div>
15.To "draw rectangle on map".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.89` Drawn rectangle on map**

</div>

16.To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.90` Area of rectangle**

</div>

17.Click on "Marker icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.91` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.92` Marker on map**

</div>


19.By click on "edit layers" icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.93` Click on Edit layers icon**

</div>

20. We can see drawn sketchings(polyline,polygon,Rectangle,Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.94` sketchings are in editable mode**

</div>

21.After edit sketchings click on "save" option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.95` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.96` saved sketchings**

</div>

21.Click on "delete" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.97` Click on delete icon**

</div>

22.Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.98` Click on clearAll**

</div>

23.Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.99` Clear All drawed sketchings on map**

</div>

24.Click on "delete"icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.100` Click on delete icon**

</div>

25.Click on "drawn rectangle" on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.101` Click on drawn rectangle**

</div>

26. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.102` Deleted drawn rectangle**

</div>

### Search on map 

o Click on "search" icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.103` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.104` Search on map**

</div>
 
### Form Info
1.Group administrator can see the information about the form by clicking on "info" option

<p align="center"> <img width="1000" height="450" src="media/GA/ga27.png"> </p>
<div align="center"> 

**`Fig.105`Form Info**

</div>

2.It show the pop-up menu with details ,form name, created by, creation date, No of assignments, No of cases collected, last form modification, last downloaded date, last downloaded by, No of users downloaded.

<p align="center"> <img width="1000" height="450" src="media/GA/ga28.png"> </p>
<div align="center"> 

**`Fig.106`Form Information**

</div>

### Form Filter
1.	It shows the forms with different types, like public and private forms 

<p align="center"> <img width="1000" height="450" src="media/GA/ga29.png"> </p>
<div align="center"> 

**`Fig.107`Form Filter**

</div>

2.	By selecting public in list it filter out public type forms in the list, and it shows with blue colour indication.

<p align="center"> <img width="1000" height="450" src="media/GA/ga30.png"> </p>

<div align="center"> 

**`Fig.108`Form with color indication**

</div>

3.	select private option from filter,it displays forms with private type with organe color indication,if there are no private forms in list it  show no data available.

<p align="center"> <img width="1000" height="450" src="media/GA/ga31.png"> </p>

## Projects
1.When clicked on "projcets" tab,we can see  the assiocated projects in the list.

2.Account Administrator can only create project.

<p align="center"> <img width="1000" height="450" src="media/GA/ga32.png"> </p>

<div align="center"> 

**`Fig.109`Shows project**

</div>

### Project view
1. To preview ‘project’ hover on the project, we can see the options

<p align="center"> <img width="1000" height="450" src="media/GA/ga33.png"> </p>

<div align="center"> 

**`Fig.110`Project preview**

</div>

2. Click on "preview" button, view the existing data only in read only fields, Group administrator cannot update project information

<p align="center"> <img width="1000" height="450" src="media/GA/ga34.png"> </p>

<div align="center"> 

**`Fig.111`Shows the existing data of the project**

</div>

### Creation of Project Task

The project task within the project be created by Group administrator

1. To create “project task” first go to project, click on project task, click on the ellipse button.

2. Click on create project task

<p align="center"> <img width="1000" height="450" src="media/GA/ga35.png"> </p>

<div align="center"> 

**`Fig.112`Create project task**

</div>

3. After clicking on "create", the form  open with name, Description, category, workflow, set task date, end date

<p align="center"> <img width="1000" height="450" src="media/GA/ga36.png"> </p>

<div align="center"> 

**`Fig.113`Project task creation form**

</div>

4. Fill the details and click on "create button""
5. After successful creation we can see a message "Task created Successfully". 

<p align="center"> <img width="1000" height="450" src="media/GA/ga37.png"> </p>

<div align="center"> 

**`Fig.114`Message displayed after successful creation**

</div>

### Edit project task

1. To edit "project task" hover on the project task, we can see the options

<p align="center"> <img width="1000" height="450" src="media/GA/ga38.png"> </p>

<div align="center"> 

**`Fig.115`Edit Project task**

</div>

2.  Click on "edit" button, view the existing data in read only fields expect end date and category fields and if any changes there make it and click on update.., Group administrator can update project task information.

<p align="center"> <img width="1000" height="450" src="media/GA/ga39.png"> </p>

<div align="center"> 

**`Fig.116`Update task**

</div>

### Delete project task

1.If you want to delete project task, click on "delete icon" , the project delete only when it is not assign to user/ when it is in progress/when it has records.

2. Group Administrator can only delete project task.

<p align="center"> <img width="1000" height="450" src="media/GA/ga40.png"> </p>
<div align="center"> 

**`Fig.117`Delete task**

</div>

### Create work assignments

1.	To create work assignment first we have to create project task as it describe as in creation of project task ,later click on the work assignment option,

<p align="center"> <img width="1000" height="450" src="media/GA/ga41.png"> </p>

<div align="center"> 

**`Fig.118`Create work assignment**

</div>

2.	After click it opens the work assignment in project task , click on “+” to create

<p align="center"> <img width="1000" height="450" src="media/GA/ga42.png"> </p>

<div align="center"> 

**`Fig.119`Create work assignment**

</div>

3. It  display the pop-up menu asking to “Do you want to attach  prepop  excel sheet?” with “yes” or “No” options, if Administrator select “yes” the menu with assignment name, form, frequency, start date, end date, upload sheet field with  choose file button to attach excel file. Fill all the details.

4.  Click on "next""

5. If Administrator select “No” then the menu with assignment name, form, frequency, start date, end date, fill all the details

6. Click on "create""

<p align="center"> <img width="1000" height="450" src="media/GA/ga43.png"> </p>

<div align="center"> 

**`Fig.120`Work assignment creation form**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga44.png"> </p>

<div align="center"> 

**`Fig.121`Click on create-to-create work assignment**

</div>

### Edit work assignment

1. To edit "work assignment' hover on the work assignment, we can see the options

<p align="center"> <img width="1000" height="450" src="media/GA/ga45.png"> </p>

<div align="center"> 

**`Fig.122`Edit work assignment**

</div>

2. Click on "edit button", view the existing data  in read only fields expect end date,  if any changes there make it and click on "update", Group administrator can update work assignment  information. After successful update you see a message"Update successfully".

<p align="center"> <img width="1000" height="450" src="media/GA/ga46.png"> </p>

<div align="center"> 

**`Fig.123`Click on Update to update work assignment**

</div>

### Delete work assignment 
1. To delete work assignment, click on "delete icon". 

<p align="center"> <img width="1000" height="450" src="media/GA/ga47.png"> </p>

<div align="center"> 

**`Fig.124` Delete work assignment**

</div>

2. Administrator cannot delete it, when work assignment was started/when it assigns to user/when work assignment is in progress.it shows a message work assignment started  

<p align="center"> <img width="1000" height="450" src="media/GA/ga48.png"> </p>

<div align="center"> 

**`Fig.125`Pop message  appear when assignment cannot be deleted**

</div>

### Submitted Records

1.	By clicking on submitted record options once all the records are displayed, Administrator can be  export the data to PDF, EXCEL, Attach as Email, Re-assign data, Go to Map view.

<p align="center"> <img width="2000" height="450" src="media/GA/ga49.png"> </p>

<div align="center"> 

**`Fig.126`Submitted record page**

</div>


### Preview of submitted records
1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.127` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.128` Preview of submitted record**

</div>

2. Click on"actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done by user in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.129` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.130`  Sketching**

</div>

Click on "geo tagged images", to see the images that are geo tagged by user on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa185.jpg"></p>

<div align="center">

**`Fig.131`  Geo tagged images of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa186.jpg"></p>

<div align="center">

**`Fig.132` Geo tagged images**

</div>

Click on "attachments" to see the attached files. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa187.jpg"></p>

<div align="center">

**`Fig.133`  Attachments of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa188.jpg"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit skecthing function administrator can sketch objects on the map. And edit sketching functionality is explained in the below steps.

1.Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.134` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.135` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.136` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.137`  Sketching**

</div>

4. Edit Skecthing 

o Click on "edit sketch" option on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.138`  Edit Sketching**

</div>

o The options displays and click on "measuring tool" to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.139`  Edit Sketching**

</div>

o By clicking on "measuring tool", options  be display the polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.140` Edit Sketching**

</div>

o If Administrator select the polyline sketching option, application allows Administrator to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.141` Draw sketching by polyline**

</div>

o Click on the "finish button" so that it end the skecthing of polyline and placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.142` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.143` Line sketching**

</div>

o Click on the "tick icon" to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.144` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to save skecthing

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.145` Save sketching**

</div>

o By clicking on "yes"in pop-up menu the sketching  save successfully and message displayed"sketching update succesfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.146` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps Administrator  sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures Administrator can sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.147` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.148` Polygon sketching**

</div>

o Administrator place a marker on the map by using "marker "option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.149` Marker sketching**

</div>

6. Delete Skecthing

o Administrator can delete the sketching, to delete the sketching user click on the "delete" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.150` Delete sketching**

</div>

o Click on the respective sketching to get delete

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.151` Delete sketching**

</div>

o Click on the "save" option, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.152` Delete sketching**

</div>

o Click on the "tick mark" option to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.153` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.154` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.155` Update sketching**

</div>

7. Edit Layers

o Administrator click on the "edit sketch" button on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.156`  Edit Sketching**

</div>

o Administrator tap on the "measuring tool option".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.157`  Measuring tool**

</div>

o Administrator tap on the "edit layers option". Administrator can see sketching data are enabled in to edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.158`  Edit layers**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.159`  Sketching in edit mode**

</div>


o Administrator can edit by draging handles on the skecthing.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.160`  Drag Handles or marker**

</div>


o After changes are done administrator click on the "save button", to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.161` Save Option**

</div>

o Click on "tick mark" lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.162`  Update Button**

</div>

o Click on "yes" button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.163`  Update Button**

</div>

o Administratot  get a message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.164`  Update Button**

</div>

8. Sketching full screen view

o By clicking on "full screen view" option, the map opens in full screen


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.165` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.166` Full screen view sketching**

</div>


### Add/Edit sketching properties in the sketching

1. Administrator  add and edit  sketching properties information for sketching objects on the map

2. Administartor click on the "edit sketch" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.167`  Edit Sketching**

</div>

3. Administrator click on the sketching object, pop-up menu open with readonly form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.168`  Sketching Properties Pop-Up menu**

</div>

4.Administrator by clicking on "edit button" in skecting properties, administartor can edit/add the fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.169`  Sketching Properties**

</div>

5 By clicking on "edit option", form opens on the right side with all editable fields as shown in fig.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.170` Edit Sketching Properties**

</div>

6. Fill all the fields and click on "tick mark" on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.171` Edit Sketching Properties**

</div>

7. After successfully added the properties to skecthing, message displayed"Properties added succesfully."

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.172` Edit Sketching Properties**

</div>

### Measuring Tool

1.Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.png"></p>

<div align="center">

**`Fig.173` submitted records table**

</div>

2. submitted record.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.png"></p>

<div align="center">

**`Fig.174` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.png"></p>

<div align="center">

**`Fig.175` submitted record preview**

</div>

3. "ellipse button".

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.png"></p>

<div align="center">

**`Fig.176` Click on ellipse button**

</div>

4.click on "sketching" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.png"></p>

<div align="center">

**`Fig.177` Click on sketching**

</div>

5.Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.png"></p>

<div align="center">

**`Fig.178` View Sketching on map**

</div>

6.Click on "draw tool icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.179` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.png"> </p>

<div align="center"> 

**`Fig.180` Draw tool**

</div>

7.Click on Draw a polyline icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.181` click on draw a polyline**

</div>

8.By click on "Polyline icon" we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.182` Drawn a poly line**

</div>

9.Click on "Finish" option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.183` Click on Finish Button**

</div>

10.To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.184` Length of distance**

</div>

11.Click on "polygon" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.185` Click on polygon icon**

</div>

12.  Draw a polygon then click on "finish button".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.186` Drawn polygon on map**

</div>


13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.187` Area of polygon**

</div>

14. Click on "draw a Rectangle icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.188` Click on draw a rectangle**

</div>
15.To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.187` Drawn rectangle on map**

</div>

16.To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.188` Area of rectangle**

</div>

17.Click on "Marker icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.189` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.190` Marker on map**

</div>


19.By click on "edit layers "icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.191` Click on Edit layers icon**

</div>

20. We can see drawn sketchings(polyline,polygon,Rectangle,Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.192` sketchings are in editable mode**

</div>

21.After edit sketchings click on "save option".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.193` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.194` saved sketchings**

</div>

21.Click on "delete icon".
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.195` Click on delete icon**

</div>

22.Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.196` Click on clearAll**

</div>

23.Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.197` Clear All drawed sketchings on map**

</div>

24.Click on "Delete icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.198` Click on delete icon**

</div>

25.Click on "drawn rectangle" on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.199` Click on drawn rectangle**

</div>

26. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.200` Deleted drawn rectangle**

</div>

### Search on map 

o Click on "search"icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.201` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.202` Search on map**

</div>

### Export to PDF 

1.	Administrator  be  select single or multiple records and then click on Pdf option to generate the submitted data to PDF format.

<p align="center"> <img width="1000" height="450" src="media/GA/ga50.png"> </p>

<div align="center"> 

**`Fig.203`Export to PDF**

</div>

2.	Once after clicking on "pdf" option, now all the list of fields be displayed select the green color option. The application  display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga51.png"> </p>

<div align="center"> 

**`Fig.204`Select required columns**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga52.png"> </p>

<div align="center"> 

**`Fig.205`Toasted messages appear**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga53.png"> </p>

<div align="center"> 

**`Fig.206`Record data shown in PDF Format**

</div>

### Download to Excel

1.	To download the data to Excel format, select the records individually or bulk and click on the "Export Excel" button from tabular view.

<p align="center"> <img width="1000" height="450" src="media/GA/ga54.png"> </p>

<div align="center"> 

**`Fig.207`Steps to generate excel file**

</div>

2.	Once after clicking on "excel option", now all the list of fields  be displayed select the green color option. The application display a message saying, ‘Please check the downloaded records in downloads tab’

<p align="center"> <img width="1000" height="450" src="media/GA/ga55.png"> </p>

<div align="center"> 

**`Fig.208`Message displays**

</div>

3.	The downloaded Excel appears in this format

<p align="center"> <img width="1000" height="450" src="media/GA/ga56.png"> </p>

<div align="center"> 

**`Fig.209`Message displays**

</div>

### Attach Email

1.	Administrator can email the records to the Valid Email & can also send the same to another email by adding in the Alternative Email Id which is optional. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga57.png"> </p>

<div align="center"> 

**`Fig.210`Steps to attach mail**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga58.png"> </p>

<div align="center"> 

**`Fig.211`Email generation form**

</div>

2.	Administrator can attach records and send to any email Id, Via., Excel, PDF. Successful pop-up  be shown on performing email records.

<p align="center"> <img width="1000" height="450" src="media/GA/ga59.png"> </p>

<div align="center"> 

**`Fig.212`Messaged displayed after successful sent**

</div>

### Re-assign the data

1.	Re-assign function defines that, if any record contains invalid or incomplete data then Administrator can re-assign the data to any user (if required).

2. To re-assign the record, first select the required record, and click on "Re-assign" button.

<p align="center"> <img width="3000" height="450" src="media/GA/ga60.png"> </p>

<div align="center"> 

**`Fig.213`Steps to re-assign record**

</div>

3.	 Enter the comments and select to which user the record as to reassign and click on "submit".

<p align="center"> <img width="1000" height="450" src="media/GA/ga61.png"> </p>

<div align="center"> 

**`Fig.214`Steps to re-assign record**

</div>

4.	Once the record is re-assigned, then the record status be changed to Reassigned record,

### See on Map

First, select the records individually or multiple records and then click on ‘Inserted Locations’. All the submitted data be displayed as a marker on map.

<p align="center"> <img width="1000" height="450" src="media/GA/ga62.png"> </p>

<div align="center"> 

**`Fig.215`Steps to see insert location on map**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga63.png"> </p>

<div align="center"> 

**`Fig.216`Map shows inserted records**

</div>

### Pending Records

For pending records hover on the task, by clicking on pending records options it displays pending records for respective task.

<p align="center"> <img width="1000" height="450" src="media/GA/ga64.png"> </p>

<div align="center"> 

**`Fig.217`Steps for pending records**

</div>

### Export to excel

1.	Administrator be export the pending data to excel

First, select the data and then click on "Excel option""

<p align="center"> <img width="1000" height="450" src="media/GA/ga65.png"> </p>

<div align="center"> 

**`Fig.218`Steps for records to export into excel**

</div>

2.Once after clicking on excel option, the fields displayed now click on "green color icon".

<p align="center"> <img width="1000" height="450" src="media/GA/ga66.png"> </p>

<div align="center"> 

**`Fig.219`Message  appear**

</div>

3.The exported data in excel be downloaded in downloads tab.

### Export to PDF
1.	Administrator can export the pending data to PDF.

2.	 First, select the data and then click on "PDF" option.

<p align="center"> <img width="1000" height="450" src="media/GA/ga67.png"> </p>

<div align="center"> 

**`Fig.220`Steps to export records into PDF**

</div>

3.	Once after clicking on "excel option", the fields  displayed now click on "green color icon".

<p align="center"> <img width="1000" height="450" src="media/GA/ga68.png"> </p>

<div align="center"> 

**`Fig.221`Message  appear**

</div>

4.	The exported data in PDF downloaded in downloads tab.

### Email Pending Records

1.	Administrator  be  export the pending data Via Email option.

2.	 First, select the data and then click on "Email option".

<p align="center"> <img width="1000" height="450" src="media/GA/ga69.png"> </p>

<div align="center"> 

**`Fig.222`Steps to export records to attach mail**

</div>

3.	Once after clicking on Email option, enter the to & CC valid email Id’s. Now select the required format ‘Pdf, Excel’ and then click on "submit button".

<p align="center"> <img width="1000" height="450" src="media/GA/ga70.png"> </p>

<div align="center"> 

**`Fig.223`Steps to export records to attach mail**

</div>

4.	Now select the list of fields and then click on "green tick color option", the application display a message saying, ‘Email sent successfully’

### Upload New Pending Records
1.	Administrator can upload new records using +Button.

2.	First, click on "Add button" and now select the "choose file option".

3.	From the choose file option select the required excel to upload the new data to the task.

<p align="center"> <img width="1000" height="450" src="media/GA/ga71.png"> </p>

<div align="center"> 

**`Fig.224`Steps to add new records**

</div>

4.	Now select the "submit option" to upload the attached file and the data be inserted in the system.

<p align="center"> <img width="1000" height="450" src="media/GA/ga72.png"> </p>

<div align="center"> 

**`Fig.225`Steps to add new records**

</div>

### Assign Users
Administrator  be  assign the unassigned data to the users.

1.	First, select the unassigned users and then click on "users".

<p align="center"> <img width="1000" height="450" src="media/GA/ga73.png"> </p>

<div align="center"> 

**`Fig.226`Steps to assign to users**

</div>

Now select the required user and then click on "add user" icon and the status be changed from Unassigned to username.

Administrator can assign the pending records to another user.

1.	First select the record and then click on "users".

2.	Select the required user, and then click on "user" icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga74.png"> </p>

<div align="center"> 

**`Fig.227`Steps to assign records to another user**

</div>

3.	The first assigned username be changed to another new user.

### Delete Pending Record

1.	First select the records individually or multiple and then click on "delete" option.

2.	The selected records be deleted from the list.

<p align="center"> <img width="1000" height="450" src="media/GA/ga75.png"> </p>

<div align="center"> 

**`Fig.228`Steps to Delete record**

</div>

### Edit  Record
1.	Click on "submitted records" button in work assignment, it display the list of records which are submitted by mobile user

2.	Tap on “original record”, it opens the form with user entered data.

3.	Tap on the "toggle button" to edit the user entered details in the record

<p align="center"> <img width="1000" height="450" src="media/GA/ga76.png"> </p>

<div align="center"> 

**`Fig.229`Steps to edit record**

</div>

4.Group Administrator can edit the details enter by user if necessary,after necessary changes done click on "update" .

<p align="center"> <img width="1000" height="450" src="media/GA/ga77.png"> </p>

<div align="center"> 

**`Fig.230`Steps to edit record**

</div>

### View sketching in Record

1.	To view sketching, first we have to open record, click on"ellipse", the sketching, and images buttons  appear.

<p align="center"> <img width="1000" height="450" src="media/GA/ga78.png"> </p>

<div align="center"> 

**`Fig.231`Steps to view sketching in record**
</div>

2.	Click on "sketching button", it show sketch  attach by user, Administrator  cannot   edit, if there is no sketch attach to record it shows and message “This  record as no sketching”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga79.png"> </p>

<div align="center"> 

**`Fig.232`Message displays “this record has no sketching**

</div>
 
<p align="center"> <img width="1000" height="450" src="media/GA/ga80.png"> </p>

<div align="center"> 

**`Fig.233`Sketching in record**

</div>
 
### View Attachment in Record
1.To view attachments, first we have to open record, tap on attachments, it shows the user attach files in record.

<p align="center"> <img width="1000" height="450" src="media/GA/ga81.png"> </p>

<div align="center"> 

**`Fig.234`Steps to view attachments**

</div>

2.	If there is no file attach to the record it shows “No data available”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga82.png"> </p>

<div align="center"> 

**`Fig.235`No data available message  appear when no attachments found**

</div>

## Workflow Approval Cycles

We have two types of approval cycles for workflow in FieldOn.

•	Task level workflow

•	Record level workflow

### Task Level Workflow

The “Task Level Workflow” is the one assigned on the “Tasks”, the administrators/moderators in the workflow are responsible to approve the whole task by checking the submissions on it.

<p align="center"> <img width="1000" height="450" src="media/GA/ga83.png"> </p>

<div align="center"> 

**`Fig.236`Selection of workflow**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga84.png"> </p>

<div align="center"> 

**`Fig.237`Selection of workflow level**

</div>

1.	While creating a task, we assign workflow to it as follows ,

2.	Select the type of workflow.

3.  After you have created the task, and assigned the workflow to it, we can now create assignments for this task.

4.	For creation of assignment please check the topic “Work Assignments”.

5.	The data is submitted on these assignments by the field user.

6.	After the data is submitted, the Account Administrator, needs to change the status of the task, to “Workflow Cycle Started”, then the workflow would be triggered and the task would be pending for approval of the first person in the workflow.

7.	Example: The following is the workflow used for depicting the task level workflow.

<p align="center"> <img width="1000" height="450" src="media/GA/ga85.png"> </p>

<div align="center"> 

**`Fig.238`Workflow cycle**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga86.png"> </p>

<div align="center"> 

**`Fig.239`Status of project**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga87.png"> </p>

<div align="center"> 

**`Fig.240`Displays  the  status of project**

</div>

### Workflow Cycle Started

•	As in the above workflow, “Chanda GA” is the first person, find this task in the tasks pending for his approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga88.png"> </p>

<div align="center"> 

**`Fig.241`Workflow Notificationt**

</div>

•	On clicking on the above highlighted icon, you can find the recent task “Ballarpur Area” pending for his approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga89.png"> </p>

<div align="center"> 

**`Fig.242`Displays  the approvals projects**

</div>

•	Now if the “Chanda GA” rejects the task, all the work assignments under it and corresponding records in it are reassigned to the users.

•	When users, submit all the reassigned records for all work assignment under the particular task, the workflow starts again, and the task is again pending for approval of “Chanda GA”

•	Now the “Chanda GA” approves and the task goes to next level in the workflow and as you can see in the below screenshot, on approval it would go to “Moderator Chandrapur” (who is a moderator).


<p align="center"> <img width="1000" height="450" src="media/GA/ga90.png"> </p>

<div align="center"> 

**`Fig.243`Shows the next approver person in cycle**

</div>

•	Now the task comes to the next level, which is at “Moderator Chandrapur”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga91.png"> </p>

<div align="center"> 

**`Fig.244`Workflow cycle in moderator**

</div>

•	“Moderator Chandrapur” can accept or reject the task, if he accepts, it goes to the next level, else back to the earlier level.

•	Now on accepting, it goes to “Chanda GA”(Note: Please see the from and to for all levels in the workflow.

<p align="center"> <img width="1000" height="450" src="media/GA/ga92.png"> </p>

<div align="center"> 

**`Fig.245`Approval in moderator**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga93.png"> </p>

<div align="center"> 

**`Fig.246`Approval in Group administrator**

</div>

•	Again, the “Chanda GA”  be responsible to approve or reject the task.

•	On approving the workflow cycle completes.

<p align="center"> <img width="1000" height="450" src="media/GA/ga94.png"> </p>

<div align="center"> 

**`Fig.247`Shows next Approval**

</div>

### Record Level Workflow

The “Record Level Workflow” is the one assigned on the “Tasks/Forms”, the administrators/moderators in the workflow are responsible to approve the record by checking the submissions on it.

Here we take an example of Record Level Workflow on a form. (Note: We have used same workflow as used in task level workflow)

1.	We assign workflow to a form by editing the form.

<p align="center"> <img width="1000" height="450" src="media/GA/ga95.png"> </p>

<div align="center"> 

**`Fig.248`Assign work flow**

</div>

2.	Whenever any record is submitted for this form, the workflow gets triggered and the record goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on “Submitted Record” icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga96.png"> </p>

<div align="center"> 

**`Fig.249`Select the record to be approved**

</div>

3.	Now login as “Chanda GA” and go to the forms records,  find the record status in RED color. The RED color indicates that the record is pending for the logged in user’s approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga97.png"> </p>

<div align="center"> 

**`Fig.250`Here red in colour shows record is pending to approve**

</div>

4.	On clicking on the status, the record details open in the side panel. There on clicking on the ellipse,  find accept or reject icons.

5.	On rejecting the record, it goes back to the field user for resubmission.

<p align="center"> <img width="1000" height="450" src="media/GA/ga98.png"> </p>

<div align="center"> 

**`Fig.251`Click on update to approve/reject  record**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga99.png"> </p>

6.	When field user submits the data, again the record comes to the “Chanda GA” for approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga100.png"> </p>

<div align="center"> 

**`Fig.252`When user submits data  again records  come to  groupadmin**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga101.png"> </p>

<div align="center"> 

**`Fig.253`Message displayed after successful approval**

</div>

7.	Now when “Moderator Chandrapur” logs in, he can find the record in RED color in forms records, waiting for his approval

<p align="center"> <img width="1000" height="450" src="media/GA/ga102.png"> </p>

<div align="center"> 

**`Fig.254`Record in pending for review in moderator as workflow**

</div>

8.	The next level person, “Chanda GA”, now has to accept or reject the record.

<p align="center"> <img width="1000" height="450" src="media/GA/ga103.png"> </p>

<div align="center"> 

**`Fig.255`Here the status of the record change to approved after moderator approval**

</div>


## Workflow History

### Task Level Workflow History

For any task having assigned a workflow, we can see the workflow history, that is, the series of approval or rejection done throughout the workflow cycle.

We need to click on the “Workflow history” icon to fetch these details.

<p align="center"> <img width="1000" height="450" src="media/GA/ga104.png"> </p>
<div align="center"> 

**`Fig.256`Hover on the task to view  task workflow history**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga105.png"> </p>

<div align="center"> 

**`Fig.257`Displays the task workflow history**

</div>

### Record Level Workflow History

We can see the workflow history for a record by opening the record and clicking on the "ellipse icon", we get icon to view workflow history.

<p align="center"> <img width="1000" height="450" src="media/GA/ga106.png"> </p>

<div align="center"> 

**`Fig.258`Record level workflow history**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga107.png"> </p>

<div align="center"> 

**`Fig.259`Displays record level workflow history**

</div>

## Tasks

Administrator can  see the task which are created by the account Administrator, by click on "task tab", it  show the associated task in the list

<p align="center"> <img width="1000" height="450" src="media/GA/ga108.png"> </p>

<div align="center"> 

**`Fig.260`Task tab screen**

</div>

### Preview Task

1.	Administrator can preview task (if required) by clicking on ‘preview’ option. Administrator can see all the  data in fields and those fields are  read and only fields.

<p align="center"> <img width="1000" height="450" src="media/GA/ga109.png"> </p>

<div align="center"> 

**`Fig.261`Preview task**

</div>

### Submitted Records

1.	Submitted records define to the Administrator, that all the user submitted records displayed. Administrator can export the data to PDF, EXCEL.

2.	To get the submitted data click on the required task and then ‘Work assignment’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga110.png"> </p>

<div align="center"> 

**`Fig.261`Click on work assignment**

</div>

3.	Now within the work assignment click on "Submitted records".

<p align="center"> <img width="1000" height="450" src="media/GA/ga111.png"> </p>

<div align="center"> 

**`Fig.262`Click on submitted records button to view submitted records**

</div>

4.	All the submitted data by the users for the work assignment  be displayed as a list.

<p align="center"> <img width="1000" height="450" src="media/GA/ga112.png"> </p>

<div align="center"> 

**`Fig.263`Displays the submitted records as list**

</div>

5.	Once all the records are displayed, Administrator can be  export the data to PDF, EXCEL, Attach as Email, Re-assign data, Go to Map view.

<p align="center"> <img width="1000" height="450" src="media/GA/ga113.png"> </p>

<div align="center"> 

**`Fig.264`Various options in submitted record tab**

</div>


### Preview of submitted records
1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.265` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.266` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done by user in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.267` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.268`  Sketching**

</div>

Click on "geo tagged images", to see the images that are geo tagged by user on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa185.jpg"></p>

<div align="center">

**`Fig.269`  Geo tagged images of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa186.jpg"></p>

<div align="center">

**`Fig.270` Geo tagged images**

</div>

Click on "attachments" to see the attached files. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa187.jpg"></p>

<div align="center">

**`Fig.271`  Attachments of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa188.jpg"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit skecthing function administrator can sketch objects on the map. And edit sketching functionality is explained in the below steps.

1.Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.272` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.273` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.274` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.275`  Sketching**

</div>

4. Edit Skecthing 

o Click on "edit sketching option" on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.276`  Edit Sketching**

</div>

o The options displayed and click on "measuring tool" to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.277`  Edit Sketching**

</div>

o By clicking on "measuring tool", options  display the polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.278` Edit Sketching**

</div>

o If Administrator select the polyline sketching option, application allows Administrator to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.279` Draw sketching by polyline**

</div>

o Click on the finish button so that it  end skecthing and the polyline placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.280` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.281` Line sketching**

</div>

o Click on the "tick icon" to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.282` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to save skecthing

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.283` Save sketching**

</div>

o By clicking on "yes" in pop-up menu the sketching  save successfully and message displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.284` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps Administrator  sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures Administrator can sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.285` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.286` Polygon sketching**

</div>

o Administrator place a marker on the map by using marker option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.287` Marker sketching**

</div>

6. Delete Skecthing

o Administrator can delete the sketching, to delete the sketching user click on the "delete icon".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.288` Delete sketching**

</div>

o Click on the respective sketching to get delete

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.289` Delete sketching**

</div>

o Click on the "save option", to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.290` Delete sketching**

</div>

o Click on the "tick mark option" to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.291` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.292` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.293` Update sketching**

</div>

7. Edit Layers

o Administrator click on the "edit sketch" button on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.294`  Edit Sketching**

</div>

o Administrator tap on the "measuring tool option".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.295`  Measuring tool**

</div>

o Administrator tap on the "edit layers option". Administrator can see sketching data are enabled in to edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.296`  Edit layers**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.297`  Sketching in edit mode**

</div>


o Administrator can edit by draging handles on the skecthing.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.298`  Drag Handles or marker**

</div>


o After changes are done administrator click on the "save button", to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.299` Save Option**

</div>

o Click on "tick mark" lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.300`  Update Button**

</div>

o Click on "yes" button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.301`  Update Button**

</div>

o Administratot  get a message "sketching Updated succeesfully"

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.302`  Update Button**

</div>

8. Sketching full screen view

o By clicking on "full screen view option", the map opens in full screen


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.303` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.304` Full screen view sketching**

</div>


### Add/Edit sketching properties in the sketching

1. Administrator can add and edit  sketching properties information for sketching objects on the map

2. Administartor click on the "edit sketch" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.305`  Edit Sketching**

</div>

3. Administrator click on the sketching object, pop-up menu open with readonly form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.306`  Sketching Properties Pop-Up menu**

</div>

4.Administrator by clicking on "edit button" in skecting properties, administartor  edit/add the fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.307`  Sketching Properties**

</div>

5 By clicking on "edit option", form opens on the right side with all editable fields as shown in fig.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.308` Edit Sketching Properties**

</div>

6. Fill all the fields and click on ?"tick mark" on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.309` Edit Sketching Properties**

</div>

7. After successfully added the properties to skecthing, message displayed "Add properites successfully"

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.310` Edit Sketching Properties**

</div>

### Measuring Tool

1.Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.png"></p>

<div align="center">

**`Fig.311` submitted records table**

</div>

2.Click On submitted record.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.png"></p>

<div align="center">

**`Fig.312` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.png"></p>

<div align="center">

**`Fig.313` submitted record preview**

</div>

3.Click on "ellipse button".

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.png"></p>

<div align="center">

**`Fig.314` Click on ellipse button**

</div>

4.click on "sketchings button".

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.png"></p>

<div align="center">

**`Fig.315` Click on sketching**

</div>

5.Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.png"></p>

<div align="center">

**`Fig.316` View Sketching on map**

</div>

6.Click on draw tool icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.png"> </p>

<div align="center"> 

**`Fig.317` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.
 - Draw a Ploygon.
 - Draw a rectangle.
 - Draw a marker.
 - Edit Layers.
 - Delete layers.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.png"> </p>

<div align="center"> 

**`Fig.318` Draw tool**

</div>

7.Click on "Draw a polyline icon".
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.png"> </p>

<div align="center"> 

**`Fig.319` click on draw a polyline**

</div>

8.By click on Polyline icon we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.png"> </p>

<div align="center"> 

**`Fig.320` Drawn a poly line**

</div>

9.Click on "Finish option"

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.png"> </p>

<div align="center"> 

**`Fig.321` Click on Finish Button**

</div>

10.To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.png"> </p>

<div align="center"> 

**`Fig.322` Length of distance**

</div>

11.Click on "polygon icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.png"> </p>

<div align="center"> 

**`Fig.323` Click on polygon icon**

</div>

12. To draw a polygon then click on "finish button".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.png"> </p>

<div align="center"> 

**`Fig.324` Drawn polygon on map**

</div>


13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.png"> </p>

<div align="center"> 

**`Fig.325` Area of polygon**

</div>

14. Click on draw a Rectangle icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.png"> </p>

<div align="center"> 

**`Fig.326` Click on draw a rectangle**

</div>
15.To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.png"> </p>

<div align="center"> 

**`Fig.327` Drawn rectangle on map**

</div>

16.To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.png"> </p>

<div align="center"> 

**`Fig.328` Area of rectangle**

</div>

17.Click on "Marker icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.png"> </p>

<div align="center"> 

**`Fig.329` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.png"> </p>

<div align="center"> 

**`Fig.330` Marker on map**

</div>


19.By click on "edit layers icon". 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.png"> </p>

<div align="center"> 

**`Fig.331` Click on Edit layers icon **

</div>

20. We can see drawn sketchings(polyline,polygon,Rectangle,Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.png"> </p>

<div align="center"> 

**`Fig.332` sketchings are in editable mode**

</div>

21.After edit sketchings click on "save option".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.png"> </p>

<div align="center"> 

**`Fig.333` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.png"> </p>

<div align="center"> 

**`Fig.334` saved sketchings**

</div>

21.Click on "delete icon".
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.png"> </p>

<div align="center"> 

**`Fig.335` Click on delete icon**

</div>

22.Click on "clearAll button".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.png"> </p>

<div align="center"> 

**`Fig.336` Click on clearAll**

</div>

23.Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.png"> </p>

<div align="center"> 

**`Fig.337` Clear All drawed sketchings on map**

</div>

24.Click on "Delete icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.png"> </p>

<div align="center"> 

**`Fig.338` Click on delete icon**

</div>

25.Click on "drawn rectangle on map".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.png"> </p>

<div align="center"> 

**`Fig.339` Click on drawn rectangle**

</div>

26. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.png"> </p>

<div align="center"> 

**`Fig.340` Deleted drawn rectangle**

</div>

### Search on map 

o Click on "search icon" on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.341` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.342` Search on map**

</div>

### Export to PDF 

1.	Administrator can  select single or multiple records and then click on "Pdf option" to generate the submitted data to PDF format.

<p align="center"> <img width="1000" height="450" src="media/GA/ga114.png"> </p>

<div align="center"> 

**`Fig.343`Various options in submitted record tab**

</div>

2.	Once after clicking on "pdf option", now all the list of fields displayed select the green color option. The application display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga115.png"> </p>

<div align="center"> 

**`Fig.344`Message appear after records successful export**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga116.png"> </p>

<div align="center"> 

**`Fig.345`Records data in PDF format**

</div>

### Download to Excel

1.	To download the data to Excel format, select the records individually or bulk and click on the ‘Export Excel’ button from tabular view.

<p align="center"> <img width="1000" height="450" src="media/GA/ga117.png"> </p>

<div align="center"> 

**`Fig.346`Steps to export records to excel**

</div>

2.	Once after clicking on "excel option", now all the list of fields  be displayed select the green color option. The application display a message saying, ‘Please check the downloaded records in downloads tab’

<p align="center"> <img width="1000" height="450" src="media/GA/ga118.png"> </p>

<div align="center"> 

**`Fig.347` Message appears after records successful export**

</div>

3.	The downloaded Excel appears in this format

<p align="center"> <img width="1000" height="450" src="media/GA/ga119.png"> </p>

<div align="center"> 

**`Fig.348` Records data in the excel sheet**

</div>

### Attach Email


1.	Administrator can email the records to the Valid Email & can also send the same to another email by adding in the Alternative Email Id which is optional. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga120.png"> </p>

<div align="center"> 

**`Fig.349` Steps to export records to mail**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga121.png"> </p>

<div align="center"> 

**`Fig.350` Fill all the details in form**

</div>

2.	Administrator can attach records and send to any email Id, Via., Excel, PDF. Successful pop-up  be shown on performing email records

<p align="center"> <img width="1000" height="450" src="media/GA/ga122.png"> </p>

<div align="center"> 

**`Fig.351`Message appear after records successful send to mail**

</div>

### See on Map

Select the records individually or multiple records and then click on "Inserted Locations". All the submitted data displayed as a marker on map.

<p align="center"> <img width="1000" height="450" src="media/GA/ga123.png"> </p>

<div align="center"> 

**`Fig.352`Steps to see inserted location records on map**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga124.png"> </p>

<div align="center"> 

**`Fig.353`Map show records with inserted location**

</div>

### Pending Records in Tasks

The assigned records for a task  be  take to excel, pdf.

### Export to excel

1. Administrator export the pending data to excel.

2. First, select the data and then click on "Excel option".

<p align="center"> <img width="1000" height="450" src="media/GA/ga125.png"> </p>

<div align="center"> 

**`Fig.354`Steps to export records to excel**

</div>

3.	Once after clicking on "excel option", the fields  be displayed now click on "green color icon".

<p align="center"> <img width="1000" height="450" src="media/GA/ga126.png"> </p>

<div align="center"> 

**`Fig.355`Message appears after successful export**

</div>

4.	The exported data in excel downloaded in downloads tab.

### Export to PDF

1.	Administrator export the pending data to PDF.

2.	First, select the data and then click on "PDF option".

<p align="center"> <img width="1000" height="450" src="media/GA/ga127.png"> </p>

<div align="center"> 

**`Fig.356`Steps to export records to PDF**

</div>

3.	Once after clicking on "excel option", the fields  be displayed now click on "green color icon".

<p align="center"> <img width="1000" height="450" src="media/GA/ga128.png"> </p>

<div align="center"> 

**`Fig.357`Message appears after successful export to pdf**

</div>

4.	The exported data in PDF downloaded in downloads tab.

### Email Pending Records

1.	Administrator export the pending data Via Email option.

2.	 First, select the data and then click on "Email option".

<p align="center"> <img width="1000" height="450" src="media/GA/ga129.png"> </p>

<div align="center"> 

**`Fig.358`Steps to export records to mail**

</div>

3.	Once after clicking on "Email option", enter the to & CC valid email Id’s. Now select the required format ‘Pdf, Excel’ and then click on "submit button".
 
<p align="center"> <img width="1000" height="450" src="media/GA/ga130.png"> </p>

<div align="center"> 

**`Fig.359`Fill all the details in the menu**

</div>

4.	Now select the list of fields and then click on "green color tick option", the application  be displaying a message saying, ‘Email sent successfully’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga131.png"> </p>

<div align="center"> 

**`Fig.360`Message appears on successfully export records to mail**

</div>

### Edit Record 

1.	Click on "submitted  records" button in work assignment, it  display the list of  records which are submitted by mobile user

2.	Tap on “original record”, it opens the form with user entered data.

3.	Tap on the "toggle button" to edit the user entered details in the record

<p align="center"> <img width="1000" height="450" src="media/GA/ga132.png"> </p>

<div align="center"> 

**`Fig.361`Steps to edit record**

</div>

4.Group Administrator can edit the details enter by user if necessary,after necessary changes done click on "update" .

<p align="center"> <img width="1000" height="450" src="media/GA/ga133.png"> </p>

<div align="center"> 

**`Fig.362`Click on update to saved changes in record**

</div>

### View sketching in Record

1.	To view sketching, first we have to open record, click on "ellipse", the sketching, and images buttons  appear.

<p align="center"> <img width="1000" height="450" src="media/GA/ga134.png"> </p>

<div align="center"> 

**`Fig.363`Steps to view sketching in record**

</div>

2.	Click on "sketching button", it show sketch  attach by user, Administrator  cannot  edit, if there is no sketch attach to record it shows and message “This  record as no sketching”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga135.png"> </p>

<div align="center"> 

**`Fig.364`Displays a message when on sketching availabled**

</div>

### View Attachment in  Record

1.	To view attachments, first we have to open record, tap on attachments, it shows the user attach files in record.

<p align="center"> <img width="1000" height="450" src="media/GA/ga136.png"> </p>

<div align="center"> 

**`Fig.365`Steps to view attachments in records**

</div>

2.There is no file attach to the record it shows “No data available”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga137.png"> </p>

<div align="center"> 

**`Fig.366`No data available message displays when no attachments available**

</div>

## Downloads

The “Downloads” tab provides the feasibility to download the record reports, that is both excel and pdf reports.

It has the other details like place of download, time of download etc.

The files under “Downloads” tab gets deleted automatically after 24 hours of download.

<p align="center"> <img width="1000" height="450" src="media/GA/ga138.png"> </p>

<div align="center"> 

**`Fig.367`Shows exported records in PDF and excel file formats**

</div>

## Dashboards

The Dashboards provide the statistical information present under the particular login. 

Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.

Under we have three type of entities:

•	Projects

•	Tasks

•	Users

1. Under the “Projects” entity the following are the statistics captured.

<p align="center"> <img width="1000" height="450" src="media/GA/ga139.png"> </p>

<div align="center"> 

**`Fig.368`Dashboard**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga140.png"> </p>

<div align="center"> 

**`Fig.369`Project entity dashboard**

</div>

•	Here Completed project block displays the No of project completed.

•	Pending Project block displays the No of projects in pending state.

•	Projects functionality block displays the total No of the projects under Group administrator.

•	Work assignments block displays the total No of work assignments under Group administrator.

•	In completed Project list we  the projects which are completed in the form of the list.

2.	Under the “Tasks” entity the following are the statistics captured.

<p align="center"> <img width="1000" height="450" src="media/GA/ga141.png"> </p>

<div align="center"> 

**`Fig.370`Task entity**

</div>

•	Here Completed project block displays the No of project completed.

•	Pending task block displays the No of the task are in pending state.

•	Task block displays the total No of task created.

3. Under the “Users” entity the following are the statistics captured.

<p align="center"> <img width="1000" height="450" src="media/GA/ga142.png"> </p>

<div align="center"> 

**`Fig.371` User’s entity**

</div>

•	Active users block  display the total No of active users.

•	Users block displays the total No of users present.

•	In active users block which is in right side, by clicking on ellipse button Administrator  see the active users with Username, Logged Time and Device details in the list.






































































































































